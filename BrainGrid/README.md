# BrainGrid

To build and run the BrainGrid application, first run `make develop`. Once the development environment is setup, run `make build` and then `make test` to ensure all tests are passing properly. Once built, a `BrainGrid` executable will be placed within the `./bin` directory, and can be used to run the simulation.

Running the application with the `--help` flag will provide a description of the application and options that can be used to define simulation parameters.

Specifying an output interval, by using the `--interval` flag will tell the program to output the simulation to a `.viz` file every `<interval>` steps. This simulation file can be played using the MASS SimViz application for a graphical visualization of the simulation space.

In the examples below, neuron somas are colored navy blue. Excitatory cells are purple, inhibitory cells are dark gray, and neutral cells are blue. Axons are shaded slightly darker than dendrites. Messages are green and are shaded based on their modulation, lighter when inhibited, darker when excited.

_Example output with a simulation space of 100x100_


![Braingrid (100x100)](./img/braingrid_100x100.gif)


```
## Command to reproduce the above simulation
./bin/BrainGrid --size=100 --interval=1 --p_excitatory=0.001 --p_inhibitory=0.001 --p_neutral=0.001 --seed=1663480003
```

_Example output with a simulation space of 1000x1000_

![Braingrid (1000x1000)](./img/braingrid_1000x1000.gif)

```
## Command to reproduce the above simulation
./bin/BrainGrid --size=1000 --interval=1 --p_excitatory=0.001 --p_inhibitory=0.001 --p_neutral=0.001 --seed=1663481128
```