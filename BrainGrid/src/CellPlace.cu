#include "CellPlace.h"

#include <mass/Mass.h>

MASS_FUNCTION CellPlace::CellPlace(mass::PlaceState *placeState, void *argument) 
    : mass::Place(placeState, argument) {

    state = (CellState*)placeState;

    // Configure Place. It'd be great if this could be set once
    // and referenced by all places.
    CellConfig* config = (CellConfig*)argument;
    
    float E = config->p_excitatory * 100.f;
    float I = config->p_inhibitory * 100.f;
    float N = config->p_neutral * 100.f;
    float G = config->growing_mode * 100.f;
    float B = config->p_branch_possibility * 100.f;
    float S = config->p_stop_branch_growth * 100.f;
    float A = config->p_signal_activation * 100.f;

    // Type percentages are stacked.
    state->conf.p_excitatory = E;
    state->conf.p_inhibitory = I + E;
    state->conf.p_neutral = N + I + E;
    state->conf.growing_mode = G;
    state->conf.p_branch_possibility = B;
    state->conf.p_stop_branch_growth = S;
    state->conf.p_signal_activation = A;
    state->conf.p_signal_modulation = config->p_signal_modulation;

    // Rest of the config
    state->conf.repetitive_branches = config->repetitive_branches;
    state->conf.branch_growth = config->branch_growth;
    
}

MASS_FUNCTION CellPlace::~CellPlace() {}

MASS_FUNCTION void CellPlace::callMethod(int functionId, void *argument) {
    switch (functionId) {
        case CellFunctions::INIT_CELL:
            initCell(((int*)argument)[getIndex()]);
            break;
        case CellFunctions::UPDATE_RANDOM_STATE:
            updateRandomState();
            break;
        case CellFunctions::UPDATE_NEURON:
            updateNeuron((bool*)argument);
            break;
        case CellFunctions::UPDATE_AXON:
            updateAxon();
            break;
        case CellFunctions::UPDATE_SYNAPTIC_TERMINAL:
            updateSynapticTerminal();
            break;
        case CellFunctions::UPDATE_DENDRITE:
            updateDendrite();
            break;
        case CellFunctions::PROPAGATE_SIGNAL:
            propagateSignal();
            break;
        case CellFunctions::PRINT_STATE:
            printState();
            break;
        case CellFunctions::PRINT_CELL_BOUNDARY:
            if (argument == NULL) {
                printCellBoundary(0);
            } else {
                printCellBoundary(*(int*)argument);
            }
            
            break;
    }

    return;
}

/**** MASS functions ****/

MASS_FUNCTION void CellPlace::initCell(int randVal) {
    // set random state
    state->randState = randVal;
    state->signal = 0.f;
    state->neighborIdx = -1;
    state->axonIdx = -1;

    // determine cell type and sub-type.
    float decider = (randVal % 100000) / 1000.f;
    if (decider < 0) { decider = -decider; }
    if (decider < state->conf.p_excitatory) {
        state->type = CellType::NEURON;
        state->subType = CellSubType::EXCITATORY;
    } else if (decider < state->conf.p_inhibitory) {
        state->type = CellType::NEURON;
        state->subType = CellSubType::INHIBITORY;
    } else if (decider < state->conf.p_neutral) {
        state->type = CellType::NEURON;
        state->subType = CellSubType::NEUTRAL;
    }

    // determine neighbor locations
    setHasNeighbors();
    setNeuronId(getIndex());
    setCanGrow(true);
}

MASS_FUNCTION void CellPlace::updateNeuron(bool *shouldSpawn) {
    if (state->type != CellType::NEURON) { return; }

    if (shouldSpawn[0]) {
        updateRandomState();
        createAxon();
    }

    if (shouldSpawn[1]) {
        updateRandomState();
        createDendrites();
    }

    int randVal = state->randState % 100;
    if (randVal < 0) { randVal = -randVal; }
    
    // if we're excitatory and we should activate a signal, do it.
    if (getCellSubType() == CellSubType::EXCITATORY && randVal < state->conf.p_signal_activation) {
        activateSignal();
    }

    // Check if we've received a signal
    if (receivedSignal()) {
        // add signal modulation
        switch (getCellSubType()) {
            case CellSubType::EXCITATORY:
                setSignal(getSignal() * (1.f + state->conf.p_signal_modulation));
                break;
            case CellSubType::INHIBITORY:
                setSignal(getSignal() - (getSignal() * state->conf.p_signal_modulation));
                break;
        }

        if (getAxonIdx() != -1 && state->hasNeighbor[getAxonIdx()]) {
            CellPlace* neighbor = ((CellPlace*)state->neighbors[getAxonIdx()]);
            neighbor->setSignal(getSignal());
            neighbor->setReceivedSignal(true);
        }

        setReceivedSignal(false);
        setSignal(0.f);
    }
}

MASS_FUNCTION void CellPlace::updateAxon() {
    if (getCellType() != CellType::AXON) { return; }
    
    updateRandomState();
    int randVal = state->randState % 100;
    if (randVal < 0) {
        randVal = -randVal;
    }

    // Attempt to grow, or transform to synaptic terminal.
    if (randVal < state->conf.growing_mode) {
        growAxon();
    } else if (!isLinked() && isTail()) {
        // If the axon isn't linked with a neighboring dendrite,
        // transform into a synaptic terminal.
        setCellType(CellType::SYNAPTIC_TERMINAL);
        setNumCellsGrown(0);
        setCanGrow(true);
    }
}

MASS_FUNCTION void CellPlace::updateDendrite() {
    if (getCellType() != CellType::DENDRITE) { return; }

    growDendrite();

    if (getNumBranched() >= state->conf.repetitive_branches) {
        return;
    }

    updateRandomState();
    int randVal = state->randState % 100;
    if (randVal < 0) {
        randVal = -randVal;
    }
    if (randVal < state->conf.p_branch_possibility && isTail()) {
        bool cellCanGrow = canGrow();
        int numGrown = getNumCellsGrown();
        setNumCellsGrown(-1);
        setCanGrow(true);
        bool success = growDendrite();
        if (success) {
            setNumBranched(getNumBranched() + 1);
        }
        setNumCellsGrown(numGrown);
        setCanGrow(cellCanGrow);
    }
}

MASS_FUNCTION void CellPlace::updateSynapticTerminal() {
    if (getCellType() != CellType::SYNAPTIC_TERMINAL) { return; }

    if (getNumBranched() >= state->conf.repetitive_branches) {
        return;
    }

    updateRandomState();
    int randVal = state->randState % 100;
    if (randVal < 0) {
        randVal = -randVal;
    }
    if (randVal < state->conf.p_branch_possibility) {
        bool cellCanGrow = canGrow();
        setCanGrow(true);
        bool success = growAxon();
        if (success) {
            setNumBranched(getNumBranched() + 1);
        }

        setCanGrow(cellCanGrow);
    }
}

MASS_FUNCTION void CellPlace::propagateSignal() {
    if (!receivedSignal()) { return; }
    
    // signal propagation for neurons is handled in updateNeuron
    // function.
    if (getCellType() == CellType::SPACE ||
        getCellType() == CellType::NEURON) { return; }

    // Synaptic terminals have multiple bridges,
    // propagate down each path.
    if (getCellType() == CellType::SYNAPTIC_TERMINAL) {
        propagateSignalST();

        return;
    }

    // If we don't have a neighbor, the signal goes away
    if (getNeighborIdx() == -1) {
        setSignal(0.f);
        setReceivedSignal(false);

        return;
    }

    CellPlace* neighbor = (CellPlace*)state->neighbors[getNeighborIdx()];
    neighbor->setSignal(getSignal());
    neighbor->setReceivedSignal(true);
    
    setSignal(0.f);
    setReceivedSignal(false);
}

/**** MASS DEBUG functions ****/

MASS_FUNCTION void CellPlace::printState() {
    printf("%d: type=%d, randState=%d, E=%.2f, I=%.2f, N=%.2f\n", 
        getIndex(), 
        state->type,
        state->randState,
        state->conf.p_excitatory,
        state->conf.p_inhibitory,
        state->conf.p_neutral
    );
}

MASS_FUNCTION void CellPlace::printCellBoundary(int distanceFromWall) {
    int idx = getIndex();
    int size = state->size[0]; // assume a square simulation space
    int row = idx / size;
    int col = idx % size;

    bool top = row == distanceFromWall;
    bool bottom = row == (size - (1 + distanceFromWall));
    bool right = col == (size - (1 + distanceFromWall));
    bool left = col == distanceFromWall;

    if (top) {
        printf("idx=%d [%d,%d]: DFW=%d, top neighbors: [%d, %d, %d]\n",
            idx, row, col, distanceFromWall,
            state->hasNeighbor[7],
            state->hasNeighbor[0],
            state->hasNeighbor[1]
        );
    }

    if (bottom) {
        printf("idx=%d [%d,%d]: DFW=%d, bottom neighbors: [%d, %d, %d]\n",
            idx, row, col, distanceFromWall,
            state->hasNeighbor[5],
            state->hasNeighbor[4],
            state->hasNeighbor[3]
        );
    }

    if (right) {
        printf("idx=%d [%d,%d]: DFW=%d, right neighbors: [%d, %d, %d]\n",
            idx, row, col, distanceFromWall,
            state->hasNeighbor[1],
            state->hasNeighbor[2],
            state->hasNeighbor[3]
        );
    }

    if (left) {
        printf("idx=%d [%d,%d]: DFW=%d, left neighbors: [%d, %d, %d]\n",
            idx, row, col, distanceFromWall,
            state->hasNeighbor[7],
            state->hasNeighbor[6],
            state->hasNeighbor[5]
        );
    }
}

/**** Utility functions ****/

MASS_FUNCTION void CellPlace::setHasNeighbors() {
    int idx = getIndex();
    int size = state->size[0]; // assume a square simulation space
    int row = idx / size;
    int col = idx % size;

    state->hasNeighbor[0] = (row - 1) >= 0;                         // N
    state->hasNeighbor[1] = (row - 1) >= 0 && (col + 1) < size;     // NE
    state->hasNeighbor[2] = (col + 1) < size;                       // E
    state->hasNeighbor[3] = (row + 1) < size && (col + 1) < size;   // SE 
    state->hasNeighbor[4] = (row + 1) < size;                       // S
    state->hasNeighbor[5] = (row + 1) < size && (col - 1) >= 0;     // SW
    state->hasNeighbor[6] = (col - 1) >= 0;                         // W
    state->hasNeighbor[7] = (row - 1) >= 0 && (col - 1) >= 0;       // NW
}

MASS_FUNCTION int CellPlace::getRelativeIdx(Direction neighbor) {
    switch (neighbor) {
        case NORTH:
            return Direction::SOUTH;
        case NORTH_EAST:
            return Direction::SOUTH_WEST;
        case EAST:
            return Direction::WEST;
        case SOUTH_EAST:
            return Direction::NORTH_WEST;
        case SOUTH:
            return Direction::NORTH;
        case SOUTH_WEST:
            return Direction::NORTH_EAST;
        case WEST:
            return Direction::EAST;
        case NORTH_WEST:
            return Direction::SOUTH_EAST;
    }

    return -1;
}

MASS_FUNCTION void CellPlace::propagateSignalST() {
    int val = -2;
    int growDir = getGrowthDirection();
    CellPlace* neighbor = NULL;
    for (int i = 0; i < 3; i++) {
        val++;
        int idx = growDir + val;
        if (idx < 0 || !state->hasNeighbor[idx]) { continue; }

        neighbor = (CellPlace*)state->neighbors[idx];
        CellType nType = neighbor->getCellType();
        
        if (nType == CellType::AXON && getNeuronId() == neighbor->getNeuronId() && neighbor->getNumCellsGrown() == 1) {
            neighbor->setSignal(getSignal());
            neighbor->setReceivedSignal(true);

            continue;
        }

        if (nType == CellType::DENDRITE && getNeuronId() != neighbor->getNeuronId()) {
            neighbor->setSignal(getSignal());
            neighbor->setReceivedSignal(true);
        }
    }

    setSignal(0.f);
    setReceivedSignal(false);
}

MASS_FUNCTION void CellPlace::activateSignal() {
    // Axon may not have been created yet...
    if (getAxonIdx() == -1 || !state->hasNeighbor[state->axonIdx]) { return; }

    CellPlace* neighbor = ((CellPlace*)state->neighbors[getAxonIdx()]);
    neighbor->setSignal(1.0000000000000f);
    neighbor->setReceivedSignal(true);
}

MASS_FUNCTION void CellPlace::createAxon() {
    if (state->axonCreated) { return; }

    int start = state->randState % MAX_NEIGHBORS;
    int idx;
    CellPlace* neighbor = NULL;
    for (int i = 0; i < MAX_NEIGHBORS; i++) {
        idx = (start + i) % MAX_NEIGHBORS;
        if (idx < 0) { // ABS of idx
            idx = -idx;
        }
        
        // if we don't have a neighbor there, move on.
        if (!state->hasNeighbor[idx]) { continue; }

        neighbor = ((CellPlace*)state->neighbors[idx]);

        // Only create Axons in empty spaces.
        if (neighbor->getCellType() == CellType::SPACE) {
            break;
        }
    }

    // If neighbor is still null or is not an empty space than there 
    // are no open spaces to create axons.
    if (neighbor == NULL || neighbor->getCellType() != CellType::SPACE) {
        return;
    }

    // "spawn" an axon but telling our neighbor to change its state.
    neighbor->setCellType(CellType::AXON);
    neighbor->setGrowthDirection((Direction)idx);
    neighbor->setCellSubType(getCellSubType());
    neighbor->setNeuronId(getNeuronId());
    neighbor->setIsTail(true);
    state->axonCreated = true;
    setAxonIdx(idx);
}

MASS_FUNCTION void CellPlace::createDendrites() {
    int maxDendrites = 7;
    if (state->dendritesCreated >= maxDendrites) {
        return;
    }
    int start = state->randState % MAX_NEIGHBORS;
    int idx;
    CellPlace* neighbor = NULL;
    for (int i = 0; i < MAX_NEIGHBORS; i++) {
        idx = (start + i) % MAX_NEIGHBORS;
        if (idx < 0) { // ABS of idx
            idx = -idx;
        }

        // if we don't have a neighbor there, move on.
        if (!state->hasNeighbor[idx]) { continue; }

        neighbor = ((CellPlace*)state->neighbors[idx]);
        if (neighbor->getCellType() != CellType::SPACE) {
            continue;
        }

        // Add dendrite
        neighbor->setCellType(CellType::DENDRITE);
        neighbor->setGrowthDirection((Direction)idx);
        neighbor->setCellSubType(getCellSubType());
        neighbor->setNeuronId(getNeuronId());
        neighbor->setNeighborIdx(getRelativeIdx((Direction)idx));
        neighbor->setNeighborType(CellType::NEURON);
        state->dendritesCreated++;
        
        // If we hit the max number of dendrites
        // then return.
        if (state->dendritesCreated >= maxDendrites) {
            return;
        }
    }
}

MASS_FUNCTION bool CellPlace::growAxon() {
    if(!canGrow()) {
        return false;
    }

    // If we're at our growth limit, disable growing
    if (getNumCellsGrown() == state->conf.branch_growth) {
        setCanGrow(false);

        return false;
    }

    // determine if axon should stop growing...
    updateRandomState();
    int randVal = state->randState % 100;
    if (randVal < 0) { randVal = -randVal; }
    if (randVal < state->conf.p_stop_branch_growth) {
        // If Axon should randomly stop growing, stop it.
        setCanGrow(false);

        return false;
    }

    randVal = state->randState % 3;
    if (randVal < 0) {
        randVal = -randVal;
    }

    int val = 0;
    if (randVal == 0) { val = -1; }
    if (randVal == 2) { val = 1; }
    
    // Grow in neutral, or +/- 45 degree offset from current 
    // growth direction. 
    int newDir = getGrowthDirection();
    newDir = (newDir + val) % MAX_NEIGHBORS;
    if (newDir < 0) {
        newDir = -newDir; // Why is this going negative?
    }

    // If no neighbor in target direction, return...
    if (!state->hasNeighbor[newDir]) {
        return false;
    }
    

    CellPlace *neighbor = (CellPlace*)state->neighbors[newDir];
    CellType neighborType = neighbor->getCellType();
    // if neighbor belongs to a different cell, and is a dendrite,
    // then we've connected with another cell and can stop growing.
    if (neighbor->getNeuronId() != getNeuronId() && neighborType == CellType::DENDRITE) {
        setCanGrow(false); // no more growing...
        setIsLinked(true);
        setNeighborIdx(newDir);
        setNeighborType(CellType::DENDRITE);

        return false;
    }

    if (neighborType != CellType::SPACE) {
        return false;
    }

    // Grow an axon
    setGrowthDirection((Direction)(newDir));
    neighbor->setCellType(CellType::AXON);
    neighbor->setGrowthDirection((Direction)newDir);
    neighbor->setCellSubType(getCellSubType());
    neighbor->setNeuronId(getNeuronId());
    neighbor->setNumBranched(getNumBranched());
    neighbor->setNumCellsGrown(getNumCellsGrown() + 1);
    neighbor->setIsTail(true);

    // Now that we've grown, we have not reason to grow further
    setCanGrow(false);
    setIsTail(false);
    setNeighborIdx(newDir);
    setNeighborType(CellType::AXON);

    return true;
}

MASS_FUNCTION bool CellPlace::growDendrite() {
    if (!canGrow()) { return false; }

    // Grow K times from last branch
    if (getNumCellsGrown() >= state->conf.branch_growth) {
        setCanGrow(false);
        
        return false;
    }

    // determine if dendrite should stop growing...
    updateRandomState();
    int randVal = state->randState % 100;
    if (randVal < 0) { randVal = -randVal; }
    if (randVal < state->conf.p_stop_branch_growth) {
        setCanGrow(false);

        return false;
    }

    updateRandomState();
    
    randVal = state->randState % 3;
    if (randVal < 0) {
        randVal = -randVal;
    }

    int val = 0;
    if (randVal == 0) { val = -1; }
    if (randVal == 2) { val = 1; }
    
    // Grow in neutral, or +/- 45 degree offset from current 
    // growth direction. 
    int newDir = getGrowthDirection();
    newDir = (newDir + val) % MAX_NEIGHBORS;
    if (newDir < 0) {
        newDir = -newDir;
    }

    if (!state->hasNeighbor[newDir]) {
        return false;
    }
    
    CellPlace *neighbor = (CellPlace*)state->neighbors[newDir];
    // If we encounter a neighboring synaptic terminal or axon,
    // stop growing.
    if (neighbor->getNeuronId() != getNeuronId() && 
        (neighbor->getCellType() == CellType::SYNAPTIC_TERMINAL || 
         neighbor->getCellType() == CellType::AXON)) {
        setCanGrow(false);
        setIsLinked(true);
        neighbor->setNeighborIdx(getRelativeIdx((Direction)newDir));
        neighbor->setNeighborType(CellType::DENDRITE);

        return false;
    }

    if (neighbor->getCellType() != CellType::SPACE) {
        return false;
    }

    setGrowthDirection((Direction)(newDir));
    neighbor->setCellType(CellType::DENDRITE);
    neighbor->setGrowthDirection((Direction)newDir);
    neighbor->setCellSubType(getCellSubType());
    neighbor->setNumCellsGrown(getNumCellsGrown() + 1);
    neighbor->setNumBranched(getNumBranched());
    neighbor->setNeuronId(getNeuronId());
    neighbor->setIsTail(true);
    neighbor->setNeighborIdx(getRelativeIdx((Direction)newDir));
    neighbor->setNeighborType(CellType::DENDRITE);
    setCanGrow(false);
    setIsTail(false);

    return true;
}

MASS_FUNCTION void CellPlace::updateRandomState() {
    state->randState += (state->randState+1) * (getIndex() + 42) * 1662917368;
}

/**** Getters ****/

MASS_FUNCTION int CellPlace::getNumBranched() {
    return state->numBranched;
}

MASS_FUNCTION int CellPlace::getNumCellsGrown() {
    return state->numCellsGrown;
}

MASS_FUNCTION float CellPlace::getSignal() {
    return state->signal;
}

MASS_FUNCTION CellType CellPlace::getCellType() {
    return state->type;
}

MASS_FUNCTION CellSubType CellPlace::getCellSubType() {
    return state->subType;
}

MASS_FUNCTION Direction CellPlace::getGrowthDirection() {
    return state->growthDirection;
}

MASS_FUNCTION bool CellPlace::isLinked() {
    return state->isLinked;
}

MASS_FUNCTION bool CellPlace::isTail() {
    return state->isTail;
}

MASS_FUNCTION CellType CellPlace::getNeighborType() {
    return state->neighborType;
}

MASS_FUNCTION int CellPlace::getNeighborIdx() {
    return state->neighborIdx;
}

MASS_FUNCTION bool CellPlace::receivedSignal() {
    return state->receivedSignal;
}

MASS_FUNCTION int CellPlace::getNeuronId() {
    return state->neuronId;
}

MASS_FUNCTION bool CellPlace::canGrow() {
    return state->canGrow;
}

MASS_FUNCTION int CellPlace::getAxonIdx() {
    return state->axonIdx;
}

/**** Setters ****/

MASS_FUNCTION void CellPlace::setNumBranched(int n) {
    state->numBranched = n;
}

MASS_FUNCTION void CellPlace::setNumCellsGrown(int n) {
    state->numCellsGrown = n;
}

MASS_FUNCTION void CellPlace::setSignal(float s) {
    state->signal = s;
}

MASS_FUNCTION void CellPlace::setCellType(CellType cellType) {
    if (state->type== CellType::SYNAPTIC_TERMINAL ||
        cellType == CellType::SYNAPTIC_TERMINAL) {
    }
    state->type = cellType;
}

MASS_FUNCTION void CellPlace::setCellSubType(CellSubType subType) {
    state->subType = subType;
}

MASS_FUNCTION void CellPlace::setGrowthDirection(Direction dir) {
    state->growthDirection = dir;
}

MASS_FUNCTION void CellPlace::setIsLinked(bool isLinked) {
    state->isLinked = isLinked;
}

MASS_FUNCTION void CellPlace::setIsTail(bool isTail) {
    state->isTail = isTail;
}

MASS_FUNCTION void CellPlace::setNeighborType(CellType type) {
    state->neighborType = type;
}

MASS_FUNCTION void CellPlace::setNeighborIdx(int idx) {
    state->neighborIdx = idx;
}

MASS_FUNCTION void CellPlace::setReceivedSignal(bool s) {
    state->receivedSignal = s;
}

MASS_FUNCTION void CellPlace::setNeuronId(int id) {
    state->neuronId = id;
}

MASS_FUNCTION void CellPlace::setCanGrow(bool canGrow) {
    state->canGrow = canGrow;
}

MASS_FUNCTION void CellPlace::setAxonIdx(int idx) {
    state->axonIdx = idx;
}
