#include <iostream>

// Boost APIs
#include <boost/program_options.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include <simviz.h>

#include <mass/Mass.h>
#include <mass/Logger.h>

#include "BrainGrid.h"
#include "Timer.h"

namespace po = boost::program_options;
namespace logging = boost::log;

int main(int argc, char* argv[]) {
	// Setup and parse program options.
	po::options_description desc("general options");
	desc.add_options()
		("help", "print help message")
		("verbose", po::bool_switch()->default_value(false), "set verbose output")
		("interval", po::value<int>()->default_value(0), "output interval")
		("out_file", po::value<std::string>()->default_value("./braingrid.viz"), "SimViz file output")
	;

	po::options_description sim_opts("simulation options");
	sim_opts.add_options()
		("seed", po::value<int>()->default_value(-1), "seed used for RNG. if left unset, current time is used")
		("size", po::value<int>()->default_value(100), "size of simulation space")
		("max_time", po::value<int>()->default_value(100), "max simulation time steps")
		("p_excitatory", po::value<float>()->default_value(0.1f), "percent excitatory [0,100]")
		("p_inhibitory", po::value<float>()->default_value(0.1f), "percent inhibitory [0,100]")
		("p_neutral", po::value<float>()->default_value(0.1f), "percent neutral [0,100]")
		("p_signal_activation", po::value<float>()->default_value(0.1f), "percent chance of signal activation [0,100]")
		("p_signal_modulation", po::value<float>()->default_value(0.1f), "percent chance of signal modulation [0,100]")
		("growing_mode", po::value<float>()->default_value(0.9f), "percent chance of signal modulation [0,100]")
		("repetitive_branches", po::value<int>()->default_value(3), "number of repetitive branches")
		("branch_growth", po::value<int>()->default_value(5), "number of times a branch can grow")
		("p_branch_possibility", po::value<float>()->default_value(0.33f), "branch_possibility percentage [0,100]")
		("p_stop_branch_growth", po::value<float>()->default_value(0.1f), "percent chance of stopping branch growth [0,100]")
	;
	desc.add(sim_opts);

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		std::cout << desc << std::endl;

		return 0;
	}

	// Setup logger.
	logging::trivial::severity_level log_level = logging::trivial::info;
	if (vm["verbose"].as<bool>()) {
		log_level = logging::trivial::debug;
	}

	mass::logger::setLogLevel(log_level);

	// Parse simulation options and get a config struct.
	braingrid::ConfigOpts opts = braingrid::parseSimConfig(vm);

	int interval = vm["interval"].as<int>();
	mass::logger::info(
		"Running BrainGrid with params: size=%d, max_time=%d, interval=%d",
		opts.size, opts.max_time, interval
	);

	// Create viz file if interval is non-zero.
	simviz::RGBFile vizFile(opts.size, opts.size);
	if (interval > 0) {
		vizFile.open(vm["out_file"].as<std::string>().c_str());
	}

	Timer timer;
	timer.start();

    // Run application
	braingrid::runSimulation(opts, interval, vizFile);

	mass::logger::info("Total execution time %dus\n", timer.lap());
    
	vizFile.close();

	return 0;
}
