#include "BrainGrid.h"

#include <vector>       // vector
#include <cstdlib>      // rand
#include <ctime>       // time

#include <mass/Mass.h>

#include "CellPlace.h"
#include "CellState.h"
#include "Timer.h"

namespace braingrid {
    void runSimulation(ConfigOpts opts, int interval, simviz::RGBFile& vizFile) {
        mass::logger::info("Starting MASS CUDA BrainGrid simulation");
        mass::logger::info("MASS Config: MAX_AGENTS=%d, MAX_NEIGHBORS=%d, MAX_DIMS=%d, N_DESTINATIONS=%d",
            MAX_AGENTS,
            MAX_NEIGHBORS,
            MAX_DIMS,
            N_DESTINATIONS
        );

        Timer initTimer;
        long initTime;
        Timer simTimer;
        long simTime;

        initTimer.start();

        std::srand(opts.seed);
        mass::logger::info("seed used for simulation: %d", opts.seed);

        // initialize MASS
        mass::Mass::init();

        int dims = 2;
        int simSpace[] = {opts.size, opts.size};
        int numCells = opts.size * opts.size;

        // Generate random values for each cell.
        int* randVals = generateRandVals(numCells);

        CellConfig* cellConf = new CellConfig{
            opts.p_excitatory,
            opts.p_inhibitory,
            opts.p_neutral,
            opts.p_signal_activation,
            opts.p_signal_modulation,
            opts.growing_mode,
            opts.repetitive_branches,
            opts.branch_growth,
            opts.p_branch_possibility,
            opts.p_stop_branch_growth
        };

        // create cell places to represent our simulation space.
        mass::Places* cellPlaces = mass::Mass::createPlaces<CellPlace, CellState>(
            0,                  // handle
            cellConf,           // args
            sizeof(CellConfig), // arg size
            dims,               // dimensions
            simSpace            // size of sim space
        );

        cellPlaces->callAll(CellFunctions::INIT_CELL, randVals, sizeof(int) * numCells);
        cellPlaces->callAll(CellFunctions::UPDATE_RANDOM_STATE);

        // Create neighborhood for each place
        std::vector<int*> neighbors;
        neighbors.push_back(new int[2]{0, 1}); // North
        neighbors.push_back(new int[2]{1, 1}); // North-East
        neighbors.push_back(new int[2]{1, 0}); // East
        neighbors.push_back(new int[2]{1, -1}); // South-East
        neighbors.push_back(new int[2]{0, -1}); // South
        neighbors.push_back(new int[2]{-1, -1}); // South-West
        neighbors.push_back(new int[2]{-1, 0}); // West
        neighbors.push_back(new int[2]{-1, 1}); // North-West

        // Output initial state
        if (interval != 0) {
            outputSimSpace(vizFile, cellPlaces, opts.size);
        }

        initTime = initTimer.lap();
        simTimer.start();
        
        // Start simulation loop
        bool *shouldSpawn = new bool[2]{0, 0};
        for (int i = 0; i < opts.max_time; i++) {
            shouldSpawn[0] = (rand() % 2) == 0; // Axon
            shouldSpawn[1] = (rand() % 2) == 0; // Dendrite

            cellPlaces->exchangeAll(&neighbors, CellFunctions::PROPAGATE_SIGNAL, NULL, 0);

            cellPlaces->exchangeAll(&neighbors, CellFunctions::UPDATE_SYNAPTIC_TERMINAL, NULL, 0);

            cellPlaces->exchangeAll(&neighbors, CellFunctions::UPDATE_AXON, NULL, 0);
            cellPlaces->exchangeAll(&neighbors, CellFunctions::UPDATE_DENDRITE, NULL, 0);

            cellPlaces->exchangeAll(&neighbors, CellFunctions::UPDATE_NEURON, shouldSpawn, sizeof(bool)*2);
            

            // Output viz frame each 'interval' tick.
            if (interval != 0 && (i % interval == 0 || i == opts.max_time - 1)) {
                outputSimSpace(vizFile, cellPlaces, opts.size);
            }
        }

        simTime = simTimer.lap();
        long totalTime = initTime + simTime;

        // DEBUG Calls
        // cellPlaces->callAll(CellFunctions::PRINT_CELL_BOUNDARY, new int(1), sizeof(int)); // DEBUG
        // cellPlaces->callAll(CellFunctions::PRINT_STATE);

        // implement simulation...
        mass::logger::info("simulation completed in %ldus "
            "(initTime=%ldus, simTime=%ldus, seed=%d) - shutting down...",
            totalTime,
            initTime,
            simTime,
            opts.seed
        );

        mass::Mass::finish();
    }

    ConfigOpts parseSimConfig(po::variables_map vm) {
        ConfigOpts opts = ConfigOpts{
            vm["seed"].as<int>(),
            vm["size"].as<int>(),
            vm["max_time"].as<int>(),
            vm["p_excitatory"].as<float>(),
            vm["p_inhibitory"].as<float>(),
            vm["p_neutral"].as<float>(),
            vm["p_signal_activation"].as<float>(),
            vm["p_signal_modulation"].as<float>(),
            vm["growing_mode"].as<float>(),
            vm["repetitive_branches"].as<int>(),
            vm["branch_growth"].as<int>(),
            vm["p_branch_possibility"].as<float>(),
            vm["p_stop_branch_growth"].as<float>()
        };

        // If seed is unset, set to current time in milli-seconds since
        // epoch
        if (opts.seed == -1) {
            std::time_t _time = std::time(NULL);
            opts.seed = _time;
        }
        
        mass::logger::debug("Simulation Config: {seed=%d, size=%d, max_time=%d, "
            "p_excitatory=%.2f, p_inhibitory=%.2f, p_neutral=%.2f, "
            "p_signal_activation=%.2f, p_signal_modulation=%.2f, "
            "growing_mode=%.2f, repetitive_branches=%d, branch_growth=%d, "
            "p_branch_possibility=%.2f, p_stop_branch_growth=%.2f}",
            opts.seed,
            opts.size,
            opts.max_time,
            opts.p_excitatory,
            opts.p_inhibitory,
            opts.p_neutral,
            opts.p_signal_activation,
            opts.p_signal_modulation,
            opts.growing_mode,
            opts.repetitive_branches,
            opts.branch_growth,
            opts.p_branch_possibility,
            opts.p_stop_branch_growth
        );

        return opts;
    }

    void outputSimSpace(simviz::RGBFile& vizFile, mass::Places *places, int size) {
        unsigned char* spaceColor = new unsigned char[3]{192, 192, 192};    // gray
        unsigned char* neuronColor = new unsigned char[3]{0, 0, 128};	    // navy blue
        unsigned char* excitatoryCell = new unsigned char[3]{145, 30, 180}; // purple
        unsigned char* inhibitoryCell = new unsigned char[3]{90, 90, 90};	// some gray
        unsigned char* neutralCell = new unsigned char[3]{0, 130, 200};     // blue
        unsigned char* signalColor = new unsigned char[3]{60, 180, 75};     // green
        unsigned char* signalModifier = new unsigned char[3]{45, 45, 45};
        unsigned char* dendriteModifier = new unsigned char[3]{45, 45, 45};

        mass::Place** cells = places->getElements();
        int indices[2];
        int ecount = 0;
        int icount = 0;
        int ncount = 0;
        int acount = 0;
        int dcount = 0;
        int stcount = 0;
        int signalCount = 0;
        for (int row = 0; row < size; row++) {
            indices[0] = row;
            for (int col = 0; col < size; col++) {
                indices[1] = col;
                int rmi = places->getRowMajorIdx(indices);
                if (rmi != (row % size) * size + col) {
                    mass::logger::error("Row Major Index is incorrect: [%d][%d] != %d",
					row, col, rmi);

					continue;
                }

                CellType cellType = ((CellPlace*)cells[rmi])->getCellType();
                CellSubType cellSubType = ((CellPlace*)cells[rmi])->getCellSubType();
                bool receivedSignal = ((CellPlace*)cells[rmi])->receivedSignal();
                float signal = ((CellPlace*)cells[rmi])->getSignal();
                unsigned char* color = spaceColor;

                switch (cellSubType) {
                    case NONE:  // no color changes
                        break;
                    case CellSubType::EXCITATORY:
                        color = excitatoryCell;
                        ecount++;
                        break;
                    case CellSubType::INHIBITORY:
                        color = inhibitoryCell;
                        icount++;
                        break;
                    case CellSubType::NEUTRAL:
                        color = neutralCell;
                        ncount++;
                        break;
                }

                switch (cellType) {
                    case CellType::NEURON:
                        color = neuronColor;
                        break;
                    case CellType::SYNAPTIC_TERMINAL:
                        stcount++;
                        break;
                    case CellType::AXON:
                        acount++;
                        break;
                    case CellType::DENDRITE:
                        dcount++;
                        // make dendrites a little lighter in color
                        color = new unsigned char[3]{
                            static_cast<unsigned char>(color[0] + dendriteModifier[0]),
                            static_cast<unsigned char>(color[1] + dendriteModifier[1]),
                            static_cast<unsigned char>(color[2] + dendriteModifier[2])
                        };
                        break;
                }

                if (receivedSignal) {
                    signalCount++;
                    color = signalColor;
                    if (signal <= 0.99) {
                        color = new unsigned char[3]{
                            static_cast<unsigned char>(color[0] + signalModifier[0]),
                            static_cast<unsigned char>(color[1] + signalModifier[1]),
                            static_cast<unsigned char>(color[2] + signalModifier[2])
                        };
                    } else if (signal >= 1.01) {
                        color = new unsigned char[3]{
                            static_cast<unsigned char>(color[0] - signalModifier[0]),
                            static_cast<unsigned char>(color[1] - signalModifier[1]),
                            static_cast<unsigned char>(color[2] - signalModifier[2])
                        };
                    }
                }

                vizFile.write((char*)color, simviz::NumRGBBytes);
            }
        }

        mass::logger::debug(
            "E: %d, I: %d, N: %d, axon: %d, dendrite: %d, ST: %d, signals: %d", 
            ecount,
            icount,
            ncount,
            acount,
            dcount,
            stcount,
            signalCount
        );
    }

    int* generateRandVals(int n) {
        int* randVals = new int[n];
        for (int i = 0; i < n; i++) {
            randVals[i] = rand();
        }

        return randVals;
    }
}
