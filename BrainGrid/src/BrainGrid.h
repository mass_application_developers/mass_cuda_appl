#pragma once

#include <boost/program_options.hpp>

#include <mass/Places.h>

#include <simviz.h>

namespace po = boost::program_options;

namespace braingrid {
    // Simulation configuration options. These are defined in this document:
    // https://docs.google.com/document/d/0B-DdRv6zRAzAS1k1SkJ0aEJ1SDFyTEJWMGlnVDFWVlA3eHhz
    struct ConfigOpts {
        int seed;
        int size;                   // S
        int max_time;
        float p_excitatory;         // E
        float p_inhibitory;         // I
        float p_neutral;            // N
        float p_signal_activation;  // A
        float p_signal_modulation;  // M
        float growing_mode;         // G
        int repetitive_branches;    // R
        int branch_growth;          // K
        float p_branch_possibility; // B
        float p_stop_branch_growth; // S
    };

    // runSimulation runs the BrainGrid simulation, configured with the
    // provided options.
    void runSimulation(ConfigOpts opts, int interval, simviz::RGBFile& vizFile);

    // outputSimSpace outputs a graphical visualiztion of the simulation space
    // to the provided simviz file.
    void outputSimSpace(simviz::RGBFile& vizFile, mass::Places *places, int size);

    // parseSimConfig parses the provided variables_map and
    // returns configs options for which to run the simulation
    // with.
    ConfigOpts parseSimConfig(po::variables_map vm);

    // generateRandVals generates 'n' random values, using cstdlib
    // and the provided seed.
    int* generateRandVals(int n);
}
