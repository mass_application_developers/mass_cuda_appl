#pragma once

#include <mass/PlaceState.h>

// forward declarations
class CellPlace;

enum Direction : int {
    NORTH = 0,  // 0
    NORTH_EAST, // 1
    EAST,       // 2
    SOUTH_EAST, // 3
    SOUTH,      // 4
    SOUTH_WEST, // 5
    WEST,       // 6
    NORTH_WEST  // 7
};

enum CellType : int {
    SPACE = 0,
    NEURON,
    AXON,
    DENDRITE,
    SYNAPTIC_TERMINAL
};

enum CellSubType : int {
    NONE = 0,
    EXCITATORY,
    INHIBITORY,
    NEUTRAL
};

struct CellConfig {
    float p_excitatory;
    float p_inhibitory;
    float p_neutral;
    float p_signal_activation;
    float p_signal_modulation;
    float growing_mode;
    int repetitive_branches;
    int branch_growth;
    float p_branch_possibility;
    float p_stop_branch_growth;
};

class CellState : public mass::PlaceState {
    public:
        int randState = 0;
        bool hasNeighbor[MAX_NEIGHBORS];
        int numCellsGrown = 0;
        bool canGrow;
        bool isLinked;
        bool isTail;
        int numBranched;
        int neuronId;
        int dendritesCreated;
        bool axonCreated;
        int axonIdx;
        float signal;
        bool receivedSignal;
        int neighborIdx;
        CellType neighborType;
        Direction growthDirection;
        CellType type = CellType::SPACE;
        CellSubType subType = CellSubType::NONE;
        CellConfig conf;
};
