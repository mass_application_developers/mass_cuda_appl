#pragma once

#include <mass/Place.h>

#include "CellState.h"

enum CellFunctions : int {
    INIT_CELL = 0,
    UPDATE_RANDOM_STATE,
    UPDATE_NEURON,
    UPDATE_AXON,
    UPDATE_SYNAPTIC_TERMINAL,
    UPDATE_DENDRITE,
    PROPAGATE_SIGNAL,
    
    // Debug functions
    PRINT_STATE,
    PRINT_CELL_BOUNDARY
};

class CellPlace : public mass::Place {
    public:
        CellState* state;
        
        MASS_FUNCTION CellPlace(mass::PlaceState *placeState, void *argument);
        MASS_FUNCTION ~CellPlace();
        MASS_FUNCTION void callMethod(int functionId, void *argument);

        // MASS functions
        MASS_FUNCTION void initCell(int randVal);
        MASS_FUNCTION void updateRandomState();
        MASS_FUNCTION void updateNeuron(bool *shouldSpawn);
        MASS_FUNCTION void updateAxon();
        MASS_FUNCTION void updateSynapticTerminal();
        MASS_FUNCTION void updateDendrite();
        MASS_FUNCTION void propagateSignal();
        
        // DEBUG functions
        MASS_FUNCTION void printState();
        MASS_FUNCTION void printCellBoundary(int distanceFromWall);

        // Utility functions
        MASS_FUNCTION void setHasNeighbors();
        MASS_FUNCTION void propagateSignalST();
        MASS_FUNCTION int getRelativeIdx(Direction neighbor);
        MASS_FUNCTION void activateSignal();
        MASS_FUNCTION void createAxon();
        MASS_FUNCTION bool growAxon();
        MASS_FUNCTION void createDendrites();
        MASS_FUNCTION bool growDendrite();

        // Getters
        MASS_FUNCTION CellType getCellType();
        MASS_FUNCTION CellSubType getCellSubType();
        MASS_FUNCTION Direction getGrowthDirection();
        MASS_FUNCTION int getNumCellsGrown();
        MASS_FUNCTION bool isLinked();
        MASS_FUNCTION bool isTail();
        MASS_FUNCTION bool canGrow();
        MASS_FUNCTION int getNumBranched();
        MASS_FUNCTION float getSignal();
        MASS_FUNCTION bool receivedSignal();
        MASS_FUNCTION int getNeuronId();
        MASS_FUNCTION int getNeighborIdx();
        MASS_FUNCTION CellType getNeighborType();
        MASS_FUNCTION int getAxonIdx();

        // Setters
        MASS_FUNCTION void setCellType(CellType cellType);
        MASS_FUNCTION void setCellSubType(CellSubType subType);
        MASS_FUNCTION void setGrowthDirection(Direction dir);
        MASS_FUNCTION void setNumCellsGrown(int n);
        MASS_FUNCTION void setIsLinked(bool isLinked);
        MASS_FUNCTION void setIsTail(bool isTail);
        MASS_FUNCTION void setCanGrow(bool canGrow);
        MASS_FUNCTION void setNumBranched(int n);
        MASS_FUNCTION void setSignal(float s);
        MASS_FUNCTION void setReceivedSignal(bool s);
        MASS_FUNCTION void setNeuronId(int id);
        MASS_FUNCTION void setNeighborIdx(int idx);
        MASS_FUNCTION void setNeighborType(CellType type);
        MASS_FUNCTION void setAxonIdx(int idx);
};
