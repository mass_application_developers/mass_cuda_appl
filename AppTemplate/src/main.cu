#include <iostream>

// Boost APIs
#include <boost/program_options.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include <mass/Mass.h>
#include <mass/Logger.h>

namespace po = boost::program_options;
namespace logging = boost::log;

int main(int argc, char* argv[]) {
	// Setup and parse program options
	po::options_description desc("options");
	desc.add_options()
		("help", "print help message")
		("verbose", po::bool_switch()->default_value(false), "set verbose output")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		std::cout << desc << std::endl;

		return 0;
	}

	// Setup logger
	logging::trivial::severity_level log_level = logging::trivial::info;
	if (vm["verbose"].as<bool>()) {
		log_level = logging::trivial::debug;
	}

	mass::logger::setLogLevel(log_level);

    // Run application
	mass::logger::info("MASS CUDA App Template: Hello, World!");
	mass::Mass::init();

	mass::logger::info("MASS Initialized... Now shutting down.");

	mass::Mass::finish();
    mass::logger::info("MASS shut down.");

	return 0;
}
