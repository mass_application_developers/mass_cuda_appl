#ifndef MACROPHAGE_STATE_H
#define MACROPHAGE_STATE_H

#include "mass/AgentState.h"

// A MacrophageState is the state for a Macrophage in the Tuberculosis simulation.
class MacrophageState : public mass::AgentState {
public:
  // The current state, whether it is a spawner, number of internal bacteria, 
  // the number of days since the macrophage was first infeceted.
  int state, isSpawner, internalBacteria, infectedTime;
};

#endif
