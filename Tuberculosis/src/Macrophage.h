#ifndef MACROPHAGE_H
#define MACROPHAGE_H

#include <mass/Agents.h>
#include "mass/AgentState.h"
#include "mass/Logger.h"

#include "MacrophageState.h"
#include "EnvironmentPlace.h"

enum MacrophageFunctions : int {
  INITIALIZE_MACRO = 0,
  MIGRATE_MACRO,
  UPDATE_STATE,
  SPAWN_MACRO,
  UPDATE_MACRO_PLACE_STATE,
};

enum State {
  RESTING,
  INFECTED,
  ACTIVATED,
  CHRONICALLY_INFECTED
};
// The Macrophage is an agent in the Tuberculosis simulation that detects foreign
// bacteria in the immune system. Macrophages can be various states depending on
// the history of interactions in the simulation space.
class Macrophage : public mass::Agent {
public:
  MASS_FUNCTION Macrophage(mass::AgentState* state, void* argument);
  MASS_FUNCTION ~Macrophage();

  // Calls the method with the given arguments specified by the given function ID.
  MASS_FUNCTION virtual void callMethod(int functionId, void* arg = NULL);

  // Returns the Macrophage's current state.
  MASS_FUNCTION virtual MacrophageState* getState();

private:
  // The macrophage's current state.
  MacrophageState* myState;

  // The bacteria capacity that a macrophage can hold.
  const int bacteriaCapacity = 100;

  // The threshold of held bacteria which turns an infected macrophage to a
  // chronically infected macrophage.
  const int chronicInfectionLimit = 75;

  // Number of bloodvessels for macrophage spawn points
  const int spawnPtNum = 4;

  // Initializes start state
  MASS_FUNCTION void initialize();

  // Migrates the current macrophage to another EnvironmentPlace if possible.
  MASS_FUNCTION void migrate();

  // Applies the rules for a resting macrophage.
  MASS_FUNCTION void restingRules();

  // Applies the rules for a infected macrophage.
  MASS_FUNCTION void infectedRules();

  // Applies the rules for an activated macrophage.
  MASS_FUNCTION void activatedRules();

  // Applies the rules for a chronically infected macrophage.
  MASS_FUNCTION void chronicallyInfectedRules();

  // Destroys the infected macrophage and spreads chemokine to the surrounding
  // EnvironmentPlaces.
  MASS_FUNCTION void burst();

  // Updates the state of the macrophage based on its' current state.
  MASS_FUNCTION void updateState();

  // Invisible macrophage spawner cells spawn new macrophages if needed
  MASS_FUNCTION void spawnMacro();

  // Updates the state of the macrophage's current place to the 
  // macrophage's current state.
  MASS_FUNCTION void updatePlaceState();
};

#endif
