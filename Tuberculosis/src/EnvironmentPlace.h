#ifndef ENVIRONMENT_PLACE_H
#define ENVIRONMENT_PLACE_H

#include <mass/Place.h>

#include "EnvironmentPlaceState.h"
#include "Macrophage.h"
#include "TCell.h"

class Macrophage;
class TCell;

enum EnvironmentFunctions : int {
    INITIALIZE_ENVIRONMENT = 0,
    INIT_RAND,
    FIX_NEIGHBORS,
    CHEMOKINE_DECAY,
    BACTERIA_GROWTH,
    CELL_RECRUITMENT,
    UPDATE_RANDOM_STATE,
    
    // Debug functions
    PRINT_STATE,
};

// An EnvironmentPlace is a cell in the Tuberculosis simulation space.
class EnvironmentPlace : public mass::Place {
public:
  MASS_FUNCTION EnvironmentPlace(mass::PlaceState* state, void* arg);
  MASS_FUNCTION ~EnvironmentPlace();

  // Calls the method with the given arguments specified by the given function ID.
  MASS_FUNCTION virtual void callMethod(int functionId, void* arg = NULL);

  // Returns whether the bacterium exists.
  MASS_FUNCTION bool getBacteria();

  // Sets the existence of bacterium.
	MASS_FUNCTION void setBacteria(bool bacteria);

  // Returns the chemokine level.
  MASS_FUNCTION int getChemokine();

  // Sets the chemokine level.
	MASS_FUNCTION void setChemokine(int chemokine);

  // Returns whether the current cell has an agent specified by the given bool.
	MASS_FUNCTION bool isGoodForMigration(bool isMacrophage);

  // Returns whether there is a macrophage occupying the EnvironmentPlace.
  MASS_FUNCTION bool getMacrophage();

  // Sets the occupancy of macrophage at the EnvironmentPlace.
  MASS_FUNCTION void setMacrophage(bool macrophage);

  // Sets the macrophage state.
  MASS_FUNCTION void setMacrophageState(int state);

  // Returns the macrophage state.
  MASS_FUNCTION int getMacrophageState();

  // Returns whether there is a tcell occupying the EnvironmentPlace.
  MASS_FUNCTION bool getTCell();

  // Sets the occupancy of tcell at the EnvironmentPlace.
  MASS_FUNCTION void setTCell(bool tcell);

  // Updates the surrounding EnvironmentPlaces when the occupying Macrophage bursts.
  MASS_FUNCTION void burst();

  // Returns the relative index of the EnvironmentPlace with the highest chemokine
  // levels.
  MASS_FUNCTION int getHighestChemokine();

  /*    Getters    */
  MASS_FUNCTION bool getShouldSpawnMacro();
  MASS_FUNCTION bool getShouldSpawnTCell();

  /*    Setters    */
  MASS_FUNCTION void stopSpawningMacro();
  MASS_FUNCTION void stopSpawningTCell();

private:
  // The state of the EnvironmentPlace.
  EnvironmentPlaceState* myState;

  // The maximum valid chemokine level.
  const int maxChemokine = 2;

  // Day when tcells can enter blood vessels
  const int tCellEntrance = 10;

  // Initializes an EnvironmentPlace using the given simulation space index and
  // simulation size.
  MASS_FUNCTION void initialize();

  // Pass in generated random values from Host to Device
  MASS_FUNCTION void initRand(int randVal);

  // Assigns NULL to edge neighbors so agents can't move through "walls"
  MASS_FUNCTION void fixNeighbors();

  // Updates the chemokine level to decay with each simulation step
  MASS_FUNCTION void chemokineDecay();

  // Updates the growth of bacterium in the simulation space.
  MASS_FUNCTION void bacteriaGrowth();

  // Reserves the place with a 50% chance to spawn the cells
  MASS_FUNCTION void cellRecruitment(int day);
  
  // Updates random state
  MASS_FUNCTION void updateRandomState();
};

#endif
