#include "Macrophage.h"

MASS_FUNCTION Macrophage::Macrophage(mass::AgentState *state, void *argument) : Agent(state, argument) {
  myState = (MacrophageState*) state;
  myState->state = RESTING;
  myState->internalBacteria = 0;
}

MASS_FUNCTION Macrophage::~Macrophage() {}

MASS_FUNCTION void Macrophage::callMethod(int functionId, void* argument) {
  switch (functionId) {
    case MacrophageFunctions::INITIALIZE_MACRO:
      initialize();
      break;
    case MacrophageFunctions::MIGRATE_MACRO:
      migrate();
      break;
    case MacrophageFunctions::UPDATE_STATE:
      updateState();
      break;
    case MacrophageFunctions::SPAWN_MACRO:
      spawnMacro();
      break;
    case MacrophageFunctions::UPDATE_MACRO_PLACE_STATE:
      updatePlaceState();
      break;
    default:
      break;
  }
}

MASS_FUNCTION MacrophageState* Macrophage::getState() {
    return myState;
}

MASS_FUNCTION void Macrophage::initialize() {
    // set up agent variables
    myState->isSpawner = getIndex() < spawnPtNum;
    updatePlaceState();
}

MASS_FUNCTION void Macrophage::migrate() {
  if (myState->isSpawner) return;

  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace(); // current place

  if (myPlace != NULL) {  // place exists
    int highestChemokineIdx = myPlace->getHighestChemokine();
    if (highestChemokineIdx != -1) {
      myPlace->setMacrophage(false);

      // Debug
      // printf("Agent: %d From: %d Migrating to: %d Direction:%d\n", getIndex(), myPlace->getIndex(), myPlace->state->neighbors[highestChemokineIdx]->getIndex(), highestChemokineIdx);
      migrateAgent(myPlace->state->neighbors[highestChemokineIdx], highestChemokineIdx);
    }
  }
}

MASS_FUNCTION void Macrophage::updateState() {
  if (myState->isSpawner) return;

  switch (myState->state) {
    case RESTING:
      restingRules();
      break;
    case INFECTED:
      infectedRules();
      break;
    case ACTIVATED:
      activatedRules();
      break;
    case CHRONICALLY_INFECTED:
      chronicallyInfectedRules();
      break;
    default:
      break;
  }
}

MASS_FUNCTION void Macrophage::restingRules() {
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  if (myPlace == NULL || !myPlace->getBacteria()) return;
  myPlace->setBacteria(false);
  myState->internalBacteria = 1;
  myState->state = INFECTED;
  myState->infectedTime = 0;
}

MASS_FUNCTION void Macrophage::infectedRules() {
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  if (myPlace == NULL) return;
  myState->infectedTime++;
  myState->internalBacteria = 2 * myState->infectedTime + 1;
  if (myState->internalBacteria > chronicInfectionLimit) {
    myState->state = CHRONICALLY_INFECTED;
  } else if (myPlace != NULL && myPlace->getTCell()) {
    myState->state = ACTIVATED;
    myState->internalBacteria = myState->infectedTime = 0;
  }
}

MASS_FUNCTION void Macrophage::activatedRules() {
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  if (myPlace == NULL || !myPlace->getBacteria()) return;
  myPlace->setBacteria(false);
}

MASS_FUNCTION void Macrophage::chronicallyInfectedRules() {
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  if (myPlace == NULL) return;
  myState->internalBacteria += 2;
  if ((myPlace != NULL && myPlace->getTCell()) || myState->internalBacteria >= bacteriaCapacity)
    burst();
}

MASS_FUNCTION void Macrophage::burst() {
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  if (myPlace == NULL) return;
  myPlace->burst();
  myPlace->setMacrophage(false);
  terminateAgent();
}

MASS_FUNCTION void Macrophage::spawnMacro() {
  if (myState->isSpawner) {
    EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
    bool shouldSpawn = myPlace->getShouldSpawnMacro();
    if (shouldSpawn) {
      spawn(1, myPlace);
      myPlace->stopSpawningMacro();
    }
  }
}

MASS_FUNCTION void Macrophage::updatePlaceState() {
  if (myState->isSpawner) return;
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  myPlace->setMacrophage(true);
  myPlace->setMacrophageState(myState->state);
}