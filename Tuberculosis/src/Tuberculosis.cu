#include "Tuberculosis.h"

#include <iostream>
#include <sstream>     // ostringstream
#include <vector>

#include <mass/Mass.h>

#include "EnvironmentPlace.h"
#include "EnvironmentPlaceState.h"
#include "Macrophage.h"
#include "MacrophageState.h"
#include "TCell.h"
#include "TCellState.h"
  
namespace tuberculosis {
    void runSimulation(ConfigOpts opts, int interval, simviz::RGBFile& vizFile) {
        mass::logger::info("Starting MASS CUDA Tuberculosis simulation");
        mass::logger::info("MASS Config: MAX_AGENTS=%d, MAX_NEIGHBORS=%d, MAX_DIMS=%d, N_DESTINATIONS=%d",
            MAX_AGENTS,
            MAX_NEIGHBORS,
            MAX_DIMS,
            N_DESTINATIONS
        );

        std::srand(opts.seed);
        mass::logger::info("seed used for simulation: %d", opts.seed);

        int dims = 2;
        int simSpace[] = {opts.size, opts.size};
        int numCells = opts.size * opts.size;

        int spawnPtNum = 4;
        int totalMacro = opts.init_macro_num + spawnPtNum;
        
        // initialize MASS
        mass::Mass::init();

        // create cell places to represent our simulation space.
        mass::Places* environmentPlaces = mass::Mass::createPlaces<EnvironmentPlace, EnvironmentPlaceState>(
            0,                  // handle
            NULL,               // args
            0,                  // arg size
            dims,               // dimensions
            simSpace            // size of sim space
        );

        environmentPlaces->callAll(EnvironmentFunctions::INITIALIZE_ENVIRONMENT);

        int* macroSpawn = new int[totalMacro];
        spawnInitialMacrophages(opts.size, macroSpawn, totalMacro);

        // create Macrophage Agents
        mass::Agents* macrophageAgents = mass::Mass::createAgents<Macrophage, MacrophageState>(
            1,                                   // handle
            NULL,                                // args
            0,                                   // arg size
            totalMacro,                          // number of agents
            0,                                   // places handle
            numCells,                            // max agents
            macroSpawn                           // initial starting places
        );

        macrophageAgents->callAll(MacrophageFunctions::INITIALIZE_MACRO);


        int* tcellSpawn = new int[4]; // Only need to initialize spawners
        spawnInitialTCells(opts.size, tcellSpawn);

        // create TCell Agents
        mass::Agents* tcellAgents = mass::Mass::createAgents<TCell, TCellState>(
            2,                                   // handle
            NULL,                                // args
            0,                                   // arg size
            4,                                   // number of agents
            0,                                   // places handle
            numCells,                            // max agents
            tcellSpawn                           // initial starting places
        );

        tcellAgents->callAll(TCellFunctions::INITIALIZE_TCELL);

        // Generate random values for each cell.
        int* randVals = generateRandVals(numCells);
        
        environmentPlaces->callAll(EnvironmentFunctions::INIT_RAND, randVals, sizeof(int) * numCells);
        delete[] randVals;

        // Create neighborhood for each place
        std::vector<int*> neighbors;
        neighbors.push_back(new int[2]{0, 1}); // North
        neighbors.push_back(new int[2]{1, 1}); // North-East
        neighbors.push_back(new int[2]{1, 0}); // East
        neighbors.push_back(new int[2]{1, -1}); // South-East
        neighbors.push_back(new int[2]{0, -1}); // South
        neighbors.push_back(new int[2]{-1, -1}); // South-West
        neighbors.push_back(new int[2]{-1, 0}); // West
        neighbors.push_back(new int[2]{-1, 1}); // North-West

        environmentPlaces->exchangeAll(&neighbors, EnvironmentFunctions::FIX_NEIGHBORS, NULL, 0);

        // Output initial state
        if (interval != 0) {
            outputSimSpace(vizFile, environmentPlaces, opts.size);
        }

        // Start simulation loop
        for (int i = 0; i < opts.total_days; i++) {

            environmentPlaces->callAll(EnvironmentFunctions::CHEMOKINE_DECAY);

            if (i > 0 && i % 10 == 0)   // grows every 10 days
                environmentPlaces->callAll(EnvironmentFunctions::BACTERIA_GROWTH);

            environmentPlaces->callAll(EnvironmentFunctions::CELL_RECRUITMENT, new int(i), sizeof(int));

            macrophageAgents->callAll(MacrophageFunctions::MIGRATE_MACRO);
            tcellAgents->callAll(TCellFunctions::MIGRATE_TCELL);

            macrophageAgents->manageAll();
            tcellAgents->manageAll();

            macrophageAgents->callAll(MacrophageFunctions::UPDATE_MACRO_PLACE_STATE);
            tcellAgents->callAll(TCellFunctions::UPDATE_TCELL_PLACE_STATE);

            macrophageAgents->callAll(MacrophageFunctions::UPDATE_STATE);

            macrophageAgents->callAll(MacrophageFunctions::SPAWN_MACRO);
            tcellAgents->callAll(TCellFunctions::SPAWN_TCELL);

            macrophageAgents->manageAll();
            tcellAgents->manageAll();

            macrophageAgents->callAll(MacrophageFunctions::UPDATE_MACRO_PLACE_STATE);
            tcellAgents->callAll(TCellFunctions::UPDATE_TCELL_PLACE_STATE);
            
            // Output viz frame each 'interval' tick.
            if (interval != 0 && (i % interval == 0 || i == opts.total_days - 1)) {
                outputSimSpace(vizFile, environmentPlaces, opts.size);
            }
        }

        printf("\nTOTAL MACRO: %d\n", macrophageAgents->getNumAgents());

        // implement simulation...
        mass::logger::info("simulation complete, shutting down...");

        mass::Mass::finish();
    }

    ConfigOpts parseSimConfig(po::variables_map vm) {
        ConfigOpts opts = ConfigOpts{
            vm["seed"].as<int>(),
            vm["size"].as<int>(),
            vm["total_days"].as<int>(),
            vm["init_macro_num"].as<int>(),
        };

        // If seed is unset, set to current time in milli-seconds since
        // epoch
        if (opts.seed == -1) {
            std::time_t _time = std::time(NULL);
            opts.seed = _time;
        }
        
        mass::logger::debug("Simulation Config: {seed=%d, size=%d, total_days=%d, "
            "init_macro_num=%d}",
            opts.seed,
            opts.size,
            opts.total_days,
            opts.init_macro_num
        );

        return opts;
    }

    void outputSimSpace(simviz::RGBFile& vizFile, mass::Places *places, int size) {
        unsigned char* spaceColor = new unsigned char[3]{255, 255, 255};                    // white
        unsigned char* bacteria = new unsigned char[3]{0, 0, 128};	                        // black
        unsigned char* macrophageResting = new unsigned char[3]{0, 255, 0};                 // green
        unsigned char* macrophageInfected = new unsigned char[3]{255, 255, 0};	            // yellow
        unsigned char* macrophageActivated = new unsigned char[3]{0, 255, 255};             // light blue
        unsigned char* macrophageChronicallyInfected = new unsigned char[3]{128, 0, 128};   // purple
        unsigned char* tCell = new unsigned char[3]{0, 0, 255};                             // blue
        unsigned char* chemokineLevelOne = new unsigned char[3]{255, 140, 0};               // orange
        unsigned char* chemokineLevelTwo = new unsigned char[3]{255, 0, 0};                 // red

        mass::Place** cells = places->getElements();

        int indices[2];
        for (int row = 0; row < size * 2; row++) {
            indices[0] = row / 2;
            for (int col = 0; col < size * 2; col++) {
                indices[1] = col / 2;
                int rmi = places->getRowMajorIdx(indices);
                if (rmi != (indices[0] % size) * size + indices[1]) {
                    mass::logger::error("Row Major Index is incorrect: [%d][%d] != %d",
					row, col, rmi);

					continue;
                }

                EnvironmentPlace* place = (EnvironmentPlace*)cells[rmi];

                unsigned char* color = spaceColor;

                if (row % 2 == 0) {
                    if (col % 2 == 0) {         // top left
                        if (place->getBacteria()) {
                            color = bacteria;
                        }
                    } else {                    // top right
                        if (place->getMacrophage()) {
                            switch (place->getMacrophageState()) {
                                case State::RESTING:
                                    color = macrophageResting;
                                    break;
                                case State::INFECTED:
                                    color = macrophageInfected;
                                    break;
                                case State::ACTIVATED:
                                    color = macrophageActivated;
                                    break;
                                case State::CHRONICALLY_INFECTED:
                                    color = macrophageChronicallyInfected;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                } else {
                    if (col % 2 == 0) {        // bottom left
                        if(place->getTCell()) {
                            color = tCell;
                        }
                    } else {                   // bottom right
                        if (place->getChemokine() == 1) {
                            color = chemokineLevelOne;
                        } else if (place->getChemokine() == 2) {
                            color = chemokineLevelTwo;
                        }
                    }
                }

                vizFile.write((char*)color, simviz::NumRGBBytes);
            }
        }
    }

    void spawnInitialMacrophages(int size, int* macroSpawn, int totalMacro) {
        int quadrant = size / 4;

        // Set spawner locations on centre of each quadrant
        macroSpawn[0] = quadrant - 1 + size * (quadrant - 1); // Top Left
        macroSpawn[1] = size - quadrant + size * (quadrant - 1); // Top Right
        macroSpawn[2] = quadrant - 1 + size * (size - quadrant);     // Bottom Left
        macroSpawn[3] = size - quadrant + size * (size - quadrant);     // Bottom Right

        std::vector<int> shufflePlaces;
        for (int i = 0; i < size * size; i++) {
            shufflePlaces.push_back(i);
        }

        std::random_shuffle(shufflePlaces.begin(), shufflePlaces.end());

        std::vector<int> retVec(&shufflePlaces[0], &shufflePlaces[totalMacro]);

        // copy to static array macroSpawn
        for (int i = 4; i < totalMacro; i++) {
            macroSpawn[i] = retVec[i];
        }
    }

    void spawnInitialTCells(int size, int* tcellSpawn) {
        int quadrant = size / 4;

        // Set spawner locations on centre of each quadrant
        tcellSpawn[0] = quadrant - 1 + size * (quadrant - 1); // Top Left
        tcellSpawn[1] = size - quadrant + size * (quadrant - 1); // Top Right
        tcellSpawn[2] = quadrant - 1 + size * (size - quadrant);     // Bottom Left
        tcellSpawn[3] = size - quadrant + size * (size - quadrant);     // Bottom Right
    }

    int* generateRandVals(int n) {
        int* randVals = new int[n];
        for (int i = 0; i < n; i++) {
            randVals[i] = rand();
        }

        return randVals;
    }
}