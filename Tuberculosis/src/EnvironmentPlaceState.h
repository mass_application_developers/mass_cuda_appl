#ifndef ENVIRONMENT_PLACE_STATE_H
#define ENVIRONMENT_PLACE_STATE_H

#include "../src/PlaceState.h"
#include "Macrophage.h"
#include "TCell.h"

class EnvironmentPlace;
class Macrophage;
class TCell;

// The state for an EnvironmentPlace in the Tuberculosis simulation space.
class EnvironmentPlaceState : public mass::PlaceState {
public:
  // Whether the bacterium exists.
  bool bacteria;

  // Whether the current EnvironmentPlace is an entry point for simulation agents.
  bool bloodVessel;

  // The chemokine level.
  int chemokine;

  // Whether the macrophage exists.
  bool macrophage;

  // Whether a macro spawner is trying to spawn on the place
  bool shouldSpawnMacro;

  // The macrophage state.
  int macrophageState;

  // The current T-Cell occupying the EnvironmentPlace.
  bool tcell;

  // Whether a tcell spawner is trying to spawn on the place
  bool shouldSpawnTCell;

  // Random value (randomized on init)
  int randState;
};

#endif
