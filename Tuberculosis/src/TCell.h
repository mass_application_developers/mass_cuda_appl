#ifndef TCELL_H
#define TCELL_H

#include <mass/Agents.h>
#include "mass/AgentState.h"
#include "mass/Logger.h"

#include "TCellState.h"
#include "EnvironmentPlace.h"

enum TCellFunctions : int {
  INITIALIZE_TCELL = 0,
  MIGRATE_TCELL,
  SPAWN_TCELL,
  UPDATE_TCELL_PLACE_STATE,
};

// The T-Cell is an agent in Tuberculosis simulation that activates infected
// macrophages and kills chronically infected macrophages
class TCell : public mass::Agent {
public:
  MASS_FUNCTION TCell(mass::AgentState* state, void* arg);
  MASS_FUNCTION ~TCell();

  // Calls the method with the given arguments specified by the given function ID.
  MASS_FUNCTION virtual void callMethod(int functionId, void* arg = NULL);

  // Returns the T-Cell's current state.
  MASS_FUNCTION virtual TCellState* getState();
private:
  // The T-Cell's current state.
  TCellState* myState;

  // Only called on spawners - initializes start state
  MASS_FUNCTION void initialize();

  // Migrates the current T-Cell to another EnvironmentPlace if possible.
  MASS_FUNCTION void migrate();

  // Invisible TCell spawner cells spawn new TCells if needed
  MASS_FUNCTION void spawnTCell();

  // Sets TCell of the TCell's current place to true
  MASS_FUNCTION void updatePlaceState();
};

#endif