#include "TCell.h"

MASS_FUNCTION TCell::TCell(mass::AgentState* state, void* arg) : Agent(state, arg) {
  myState = (TCellState*) state;
  myState->isSpawner = false;
}

MASS_FUNCTION TCell::~TCell() {}

MASS_FUNCTION void TCell::callMethod(int functionId, void* arg) {
  switch (functionId) {
    case TCellFunctions::INITIALIZE_TCELL:
      initialize();
      break;
    case TCellFunctions::MIGRATE_TCELL:
      migrate();
      break;
    case TCellFunctions::SPAWN_TCELL:
      spawnTCell();
      break;
    case TCellFunctions::UPDATE_TCELL_PLACE_STATE:
      updatePlaceState();
      break;
    default:
      break;
  }
}

MASS_FUNCTION TCellState* TCell::getState() {
  return myState;
}

MASS_FUNCTION void TCell::initialize() {
  myState->isSpawner = true;
}


MASS_FUNCTION void TCell::migrate() {
  if (myState->isSpawner) return;

  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace(); // current place

  if (myPlace != NULL) {  // place exists
    int highestChemokineIdx = myPlace->getHighestChemokine();
    if (highestChemokineIdx != -1) {
      myPlace->setTCell(false);

      // Debug
      // printf("Agent: %d From: %d Migrating to: %d Direction:%d\n", getIndex(), myPlace->getIndex(), myPlace->state->neighbors[highestChemokineIdx]->getIndex(), highestChemokineIdx);
      migrateAgent(myPlace->state->neighbors[highestChemokineIdx], highestChemokineIdx);
    }
  }
}

MASS_FUNCTION void TCell::spawnTCell() {
  if (myState->isSpawner) {
    EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
    bool shouldSpawn = myPlace->getShouldSpawnTCell();
    if (shouldSpawn) {
      spawn(1, myPlace);
      myPlace->stopSpawningTCell();
    }
  }
}

MASS_FUNCTION void TCell::updatePlaceState() {
  if (myState->isSpawner) return;
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  myPlace->setTCell(true);
}