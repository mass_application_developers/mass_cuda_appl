#include "EnvironmentPlace.h"

using namespace std;
using namespace mass;

MASS_FUNCTION EnvironmentPlace::EnvironmentPlace(PlaceState* state, void* argument) : Place(state, argument) {
  myState = (EnvironmentPlaceState*) state;
  myState->bacteria = myState->bloodVessel = myState->macrophage = myState->shouldSpawnMacro = myState->tcell = false;
  myState->chemokine = 0;
  myState->macrophageState = -1;
  myState->randState = 0;
}

MASS_FUNCTION EnvironmentPlace::~EnvironmentPlace() {}

MASS_FUNCTION void EnvironmentPlace::callMethod(int functionId, void* arg) {
  switch (functionId) {
    case EnvironmentFunctions::INITIALIZE_ENVIRONMENT:
      initialize();
      break;
    case EnvironmentFunctions::INIT_RAND:
      initRand(((int *)arg)[getIndex()]);
      break;
    case EnvironmentFunctions::FIX_NEIGHBORS:
      fixNeighbors();
      break;
    case EnvironmentFunctions::CHEMOKINE_DECAY:
      chemokineDecay();
      break;
    case EnvironmentFunctions::BACTERIA_GROWTH:
      bacteriaGrowth();
      break;
    case EnvironmentFunctions::CELL_RECRUITMENT:
      cellRecruitment(*(int*)arg);
      break;
    case EnvironmentFunctions::UPDATE_RANDOM_STATE:
      updateRandomState();
      break;
    default:
      break;
  }
}

MASS_FUNCTION bool EnvironmentPlace::getBacteria() {
  return myState->bacteria;
}

MASS_FUNCTION void EnvironmentPlace::setBacteria(bool newBacteria) {
  if (myState->bacteria != newBacteria) {
    myState->bacteria = newBacteria;
  }
}

MASS_FUNCTION int EnvironmentPlace::getChemokine() {
  return myState->chemokine;
}

MASS_FUNCTION void EnvironmentPlace::setChemokine(int newChemokine) {
  int chemokineLevel = newChemokine;
  if (chemokineLevel < 0)
    chemokineLevel = 0;
  else if (chemokineLevel > maxChemokine)
    chemokineLevel = maxChemokine;

  if (myState->chemokine != chemokineLevel)
    myState->chemokine = chemokineLevel;
}

MASS_FUNCTION bool EnvironmentPlace::isGoodForMigration(bool isMacrophage) {
  return !getMacrophage();
}

MASS_FUNCTION bool EnvironmentPlace::getMacrophage() {
  return myState->macrophage;
}

MASS_FUNCTION void EnvironmentPlace::setMacrophage(bool newMacrophage) {
  if (myState->macrophage != newMacrophage) {
    myState->macrophage = newMacrophage;
  }
}

MASS_FUNCTION void EnvironmentPlace::setMacrophageState(int state) {
  myState->macrophageState = state;
}

MASS_FUNCTION int EnvironmentPlace::getMacrophageState() {
  return myState->macrophageState;
}

MASS_FUNCTION bool EnvironmentPlace::getTCell() {
  return myState->tcell;
}

MASS_FUNCTION void EnvironmentPlace::setTCell(bool newTCell) {
  if (myState->tcell != newTCell) {
    myState->tcell = newTCell;
  }
}

MASS_FUNCTION void EnvironmentPlace::burst() {
  setBacteria(true);
  for (int i = 0; i < MAX_NEIGHBORS; i++) {
    EnvironmentPlace* neighbor = (EnvironmentPlace*) myState->neighbors[i];
    if (neighbor != NULL)
      neighbor->setBacteria(true);
  }
}

MASS_FUNCTION int EnvironmentPlace::getHighestChemokine() {
  int highestVal = 0;
  int count = 0;

  for (int i = 0; i < MAX_NEIGHBORS; i++) {
    EnvironmentPlace* neighbor = (EnvironmentPlace*) myState->neighbors[i];
    if (neighbor != NULL) {
      int neighborChemokine = neighbor->getChemokine();
      if (neighborChemokine > highestVal) {
        highestVal = neighborChemokine;
        count = 0;
      }
      if (neighborChemokine == highestVal && !myState->shouldSpawnMacro) {
        count++;
      }
    }
  }

  int highestValIndex = 0;
  updateRandomState();
  int randVal = myState->randState;
  highestValIndex = myState->randState % count;
  if (highestValIndex < 0) { highestValIndex = - highestValIndex; }
  highestValIndex++;

  for (int i = 0; i < MAX_NEIGHBORS; i++) {
    EnvironmentPlace* neighbor = (EnvironmentPlace*) myState->neighbors[i];
    if (neighbor != NULL) {
      if (neighbor->getChemokine() == highestVal && !myState->shouldSpawnMacro) highestValIndex--;
      if (highestValIndex == 0) {
        return i;
      }
    }
  }
  return -1;
}

MASS_FUNCTION void EnvironmentPlace::initialize() {
  int index = getIndex();
  int size = myState->size[0];
  int j = index % size, i = index / size;
  int half = size / 2, quadrant = size / 4;
  if ((i == half-1 || i == half) && (j == half-1 || j == half)) {
    setBacteria(true);
  } else {
    setBacteria(false);
  }
  if ((i == quadrant-1 || i == size-quadrant) && (j == quadrant-1 || j == size-quadrant)) {
    myState->bloodVessel = true;
  }
  myState->randState = 0;
}

MASS_FUNCTION void EnvironmentPlace::initRand(int randVal) {
  myState->randState = randVal;
}

MASS_FUNCTION void EnvironmentPlace::fixNeighbors() {
  int index = getIndex();
  int size = myState->size[0];
  if (index < size) {
    myState->neighbors[0] = myState->neighbors[1] = myState->neighbors[7] = nullptr;
  }
  if (index >= size * size - size) {
    myState->neighbors[3] = myState->neighbors[4] = myState->neighbors[5] = nullptr;
  }
  if (index % size == 0) {
    myState->neighbors[5] = myState->neighbors[6] = myState->neighbors[7] = nullptr;
  }
  if (index % size == size - 1) {
    myState->neighbors[1] = myState->neighbors[2] = myState->neighbors[3] = nullptr;
  }
}

MASS_FUNCTION void EnvironmentPlace::chemokineDecay() {
  setChemokine(getChemokine() - 1);
  if (!getMacrophage()) return;
  if (myState->macrophageState == State::RESTING || myState->macrophageState == State::INFECTED || myState->macrophageState == State::CHRONICALLY_INFECTED) {
    setChemokine(maxChemokine);
    for (int i = 0; i < MAX_NEIGHBORS; i++) {
      EnvironmentPlace* neighbor = (EnvironmentPlace*) myState->neighbors[i];
      if (neighbor != NULL)
        neighbor->setChemokine(maxChemokine);
    }
  }
}

MASS_FUNCTION void EnvironmentPlace::bacteriaGrowth() {
  if (!getBacteria()) return;
  for (int i = 0; i < MAX_NEIGHBORS; i++) {
    EnvironmentPlace* neighbor = (EnvironmentPlace*) myState->neighbors[i];
    if (neighbor != NULL)
      neighbor->setBacteria(true);
  }
}

MASS_FUNCTION void EnvironmentPlace::cellRecruitment(int day) {
  if (!myState->bloodVessel) return;

  if (!myState->macrophage) {
    updateRandomState();
    if (myState->randState % 100 < 50) {
      myState->macrophage = true;
      myState->shouldSpawnMacro = true;
    }
  }

  if (day > tCellEntrance && !myState->tcell) {
    updateRandomState();
    if (myState->randState % 100 < 50) {
      myState->tcell = true;
      myState->shouldSpawnTCell = true;
    }
  }
}

MASS_FUNCTION void EnvironmentPlace::updateRandomState() {
  myState->randState += (myState->randState+1) * (getIndex() + 42) * 1662169368;
}

MASS_FUNCTION bool EnvironmentPlace::getShouldSpawnMacro() { return myState->shouldSpawnMacro; }
MASS_FUNCTION bool EnvironmentPlace::getShouldSpawnTCell() { return myState->shouldSpawnTCell; }

MASS_FUNCTION void EnvironmentPlace::stopSpawningMacro() { myState->shouldSpawnMacro = false; }
MASS_FUNCTION void EnvironmentPlace::stopSpawningTCell() { myState->shouldSpawnTCell = false; }