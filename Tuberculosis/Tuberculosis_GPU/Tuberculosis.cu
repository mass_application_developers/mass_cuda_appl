#include <iostream>
#include <fstream>	// file IO
#include <sstream>  // ostringstream
#include <stdlib.h>	// rand
#include "Timer.h"	// timer
using namespace std;

#define H2D cudaMemcpyHostToDevice
#define D2H cudaMemcpyDeviceToHost

// Constants
const static int numBloodVessels = 4;
const static int numNeighbors = 8;

// Structs
// Places hold information on the current state of the place including whether
// or not it had bacteria, which agent, and the chemokine levels if any
struct Place {
	bool bloodVessel, bacteria, macrophage, tcell, temp;
	int chemokine;	// Valid Levels: 0, 1, 2
};

// Macrophages hold information on the current state of the agent including
// how many bacteria it is holding and where it is located
struct Macrophage {
	enum State{RESTING = 0, INFECTED = 1, ACTIVATED = 2, CHRONICALLY_INFECTED = 3, DEAD = 4};
	const static int bacteriaCapacity = 100;
	const static int chronicInfectionLimit = 75;

	State currentState;
	int internalBacteria, row, col, infectedTime;
};

// T-Cells hold information on the current location of the agent
struct TCell {
	int row, col;
};

// Device (GPU) Methods
__device__ void restingRules(Macrophage* macrophages, int mIdx, Place** grid) {
	int row = macrophages[mIdx].row, col = macrophages[mIdx].col;
	if (!grid[row][col].bacteria) return;
	macrophages[mIdx].currentState = Macrophage::INFECTED;
	macrophages[mIdx].internalBacteria = 1;
	macrophages[mIdx].infectedTime = 0;
	grid[row][col].bacteria = false;
}

__device__ void infectedRules(Macrophage* macrophages, int mIdx, Place** grid) {
	int row = macrophages[mIdx].row, col = macrophages[mIdx].col;
	macrophages[mIdx].infectedTime++;
	macrophages[mIdx].internalBacteria = 2 * macrophages[mIdx].infectedTime + 1;
	if (macrophages[mIdx].internalBacteria > Macrophage::chronicInfectionLimit) {
		macrophages[mIdx].currentState = Macrophage::CHRONICALLY_INFECTED;
	} else if (grid[row][col].tcell) {
		macrophages[mIdx].currentState = Macrophage::ACTIVATED;
		macrophages[mIdx].internalBacteria = 0;
		macrophages[mIdx].infectedTime = -1;
	}
}

__device__ void activatedRules(Macrophage* macrophages, int mIdx, Place** grid) {
	grid[macrophages[mIdx].row][macrophages[mIdx].col].bacteria = false;
}

__device__ void burst(Macrophage* macrophages, int mIdx, Place** grid, int size) {
	int col = macrophages[mIdx].col, row = macrophages[mIdx].row;
	for (int vert = -1; vert <= 1; vert++) {
		for (int hori = -1; hori <= 1; hori++) {
			if (row + vert != -1 && row + vert != size && col + hori != -1 && col + hori != size) {
				grid[row][col].bacteria = true;
			}
		}
	}
	macrophages[mIdx].currentState = Macrophage::DEAD;
	macrophages[mIdx].row = macrophages[mIdx].col = macrophages[mIdx].infectedTime = macrophages[mIdx].internalBacteria = -1;
	grid[row][col].macrophage = false;
}

__device__ bool chronicallyInfectedRules(Macrophage* macrophages, int mIdx, Place** grid, int size) {
	int row = macrophages[mIdx].row, col = macrophages[mIdx].col;
	macrophages[mIdx].internalBacteria += 2;
	return macrophages[mIdx].internalBacteria >= Macrophage::bacteriaCapacity || grid[row][col].tcell;
}

// Global (Shared) Methods
__global__ void computeChemokineDecay(Place **grid, int gridSize, Macrophage* macrophages) {
	int i = threadIdx.x, j = threadIdx.y;
	if (grid[i][j].chemokine > 0) {
		grid[i][j].chemokine--;
	}
	if (!grid[i][j].macrophage) return;
	int rmi = i * gridSize + j;	// agentIndex
	if (macrophages[rmi].currentState == Macrophage::DEAD || macrophages[rmi].currentState == Macrophage::RESTING) return;
	for (int vert = -1; vert <= 1; vert++) {
		for (int hori = -1; hori <= 1; hori++) {
			if (i + vert != -1 && i + vert != gridSize && j + hori != -1 && j + hori != gridSize) {
				grid[i + vert][j + hori].chemokine = 2;
			}
		}
	}
}

__global__ void bacteriaGrowth(Place** grid, int size) {
	int row = threadIdx.x, col = threadIdx.y;
	if (grid[row][col].bacteria) {
		for (int vert = -1; vert <= 1; vert++) {
			for (int hori = -1; hori <= 1; hori++) {
				if (row + vert != -1 && row + vert != size && col + hori != -1 && col + hori != size) {
					grid[row + vert][col + hori].temp = true;
				}
			}
		}
	}
	__syncthreads();		// synchronize warps in a block
	grid[row][col].bacteria = grid[row][col].temp;
	grid[row][col].temp = false;
}

__global__ void moveMacrophage(Place** grid, int gridSize, int* randMovement, Macrophage* mAgents) {
	const int agentId = threadIdx.x, row = mAgents[agentId].row, col = mAgents[agentId].col;
	int highestChemokine = 0, nextPosition = 0, positionCounter = 0;
	// update position (find highest chemokine in moor's neighbor, array of random positions)
	for (int vert = -1; vert <= 1; vert++) {
		for (int hori = -1; hori <= 1; hori++, positionCounter++) {
			if (row + vert != -1 && row + vert != gridSize && col + hori != -1 && col + hori != gridSize) {
				if (grid[row + vert][col + hori].chemokine > highestChemokine) {
					highestChemokine = grid[row + vert][col + hori].chemokine;
					nextPosition = positionCounter;
				}
			}
		}
	}
	if (highestChemokine > 0) {
		switch(nextPosition) {
			case 0:
				if (!grid[row - 1][col - 1].macrophage) {
					mAgents[agentId].row = row - 1;
					mAgents[agentId].row = col - 1;
					grid[row - 1][col - 1].macrophage = true;
				}
				break;
			case 1:
				if (!grid[row - 1][col].macrophage) {
					mAgents[agentId].row = row - 1;
					grid[row - 1][col].macrophage = true;
				}
				break;
			case 2:
				if (!grid[row - 1][col + 1].macrophage) {
					mAgents[agentId].col = col + 1;
					mAgents[agentId].row = row - 1;
					grid[row - 1][col + 1].macrophage = true;
				}
				break;
			case 3:
				if (!grid[row][col - 1].macrophage) {
					mAgents[agentId].col = col - 1;
					grid[row][col - 1].macrophage = true;
				}
				break;
			case 4:
				if (!grid[row][col + 1].macrophage) {
					mAgents[agentId].col = col + 1;
					grid[row][col + 1].macrophage = true;
				}
				break;
			case 5:
				if (!grid[row + 1][col - 1].macrophage) {
					mAgents[agentId].col = col - 1;
					mAgents[agentId].row = row + 1;
					grid[row + 1][col - 1].macrophage = true;
				}
				break;
			case 6:
				if (!grid[row + 1][col].macrophage) {
					mAgents[agentId].row = row + 1;
					grid[row + 1][col].macrophage = true;
				}
				break;
			case 7:
				if (!grid[row + 1][col + 1].macrophage) {
					mAgents[agentId].col = col + 1;
					mAgents[agentId].row = row + 1;
					grid[row + 1][col + 1].macrophage = true;
				}
				break;
		}
	} else {
		switch(randMovement[agentId]) {
			case 0:
				if (!grid[row - 1][col - 1].macrophage) {
					mAgents[agentId].col = col - 1;
					mAgents[agentId].row = row - 1;
					grid[row - 1][col - 1].macrophage = true;
				}
				break;
			case 1:
				if (!grid[row - 1][col].macrophage) {
					mAgents[agentId].row = row - 1;
					grid[row - 1][col].macrophage = true;
				}
				break;
			case 2:
				if (!grid[row - 1][col + 1].macrophage) {
					mAgents[agentId].col = col + 1;
					mAgents[agentId].row = row - 1;
					grid[row - 1][col + 1].macrophage = true;
				}
				break;
			case 3:
				if (!grid[row][col - 1].macrophage) {
					mAgents[agentId].col = col - 1;
					grid[row][col - 1].macrophage = true;
				}
				break;
			case 4:
				if (!grid[row][col + 1].macrophage) {
					mAgents[agentId].col = col + 1;
					grid[row][col + 1].macrophage = true;
				}
				break;
			case 5:
				if (!grid[row + 1][col - 1].macrophage) {
					mAgents[agentId].col = col - 1;
					mAgents[agentId].row = row + 1;
					grid[row + 1][col - 1].macrophage = true;
				}
				break;
			case 6:
				if (!grid[row + 1][col].macrophage) {
					mAgents[agentId].row = row + 1;
					grid[row + 1][col].macrophage = true;
				}
				break;
			case 7:
				if (!grid[row + 1][col + 1].macrophage) {
					mAgents[agentId].col = col + 1;
					mAgents[agentId].row = row + 1;
					grid[row + 1][col + 1].macrophage = true;
				}
				break;
		}
	}
}

__global__ void updateMacrophageState(Place** grid, int size, Macrophage* macrophages) {
	const int index = threadIdx.x;
	switch (macrophages[index].currentState) {
		case Macrophage::RESTING:
			restingRules(macrophages, index, grid);
			break;
		case Macrophage::INFECTED:
			infectedRules(macrophages, index, grid);
			break;
		case Macrophage::ACTIVATED:
			activatedRules(macrophages, index, grid);
			break;
		case Macrophage::CHRONICALLY_INFECTED:
			if (chronicallyInfectedRules(macrophages, index, grid, size))
				burst(macrophages, index, grid, size);
			break;
		default: // DEAD
			break;
	}
}

__global__ void moveTCell(Place** grid, int size, int* randMovement, TCell* tAgents) {
	const int agentId = threadIdx.x, row = tAgents[agentId].row, col = tAgents[agentId].col;
	int highestChemokine = 0, nextPosition = 0, positionCounter = 0;
	// update position (find highest chemokine in moor's neighbor, array of random positions)
	for (int vert = -1; vert <= 1; vert++) {
		for (int hori = -1; hori <= 1; hori++, positionCounter++) {
			if (row + vert != -1 && row + vert != size && col + hori != -1 && col + hori != size) {
				if (grid[row + vert][col + hori].chemokine > highestChemokine) {
					highestChemokine = grid[row + vert][col + hori].chemokine;
					nextPosition = positionCounter;
				}
			}
		}
	}
	if (highestChemokine > 0) {
		switch(nextPosition) {
			case 0:
				if (!grid[row - 1][col - 1].tcell) {
					tAgents[agentId].col = col - 1;
					tAgents[agentId].row = row - 1;
					grid[row - 1][col - 1].tcell = true;
				}
				break;
			case 1:
				if (!grid[row - 1][col].tcell) {
					tAgents[agentId].row = row - 1;
					grid[row - 1][col].tcell = true;
				}
				break;
			case 2:
				if (!grid[row - 1][col + 1].tcell) {
					tAgents[agentId].col = col + 1;
					tAgents[agentId].row = row - 1;
					grid[row - 1][col + 1].tcell = true;
				}
				break;
			case 3:
				if (!grid[row][col - 1].tcell) {
					tAgents[agentId].col = col - 1;
					grid[row][col - 1].tcell = true;
				}
				break;
			case 4:
				if (!grid[row][col + 1].tcell) {
					tAgents[agentId].col = col + 1;
					grid[row][col + 1].tcell = true;
				}
				break;
			case 5:
				if (!grid[row + 1][col - 1].tcell) {
					tAgents[agentId].col = col - 1;
					tAgents[agentId].row = row + 1;
					grid[row + 1][col - 1].tcell = true;
				}
				break;
			case 6:
				if (!grid[row + 1][col].tcell) {
					tAgents[agentId].row = row + 1;
					grid[row + 1][col].tcell = true;
				}
				break;
			case 7:
				if (!grid[row + 1][col + 1].tcell) {
					tAgents[agentId].col = col + 1;
					tAgents[agentId].row = row + 1;
					grid[row + 1][col + 1].tcell = true;
				}
				break;
		}
	} else {
		switch(randMovement[agentId]) {
			case 0:
				if (!grid[row - 1][col - 1].tcell) {
					tAgents[agentId].col = col - 1;
					tAgents[agentId].row = row - 1;
					grid[row - 1][col - 1].tcell = true;
				}
				break;
			case 1:
				if (!grid[row - 1][col].tcell) {
					tAgents[agentId].row = row - 1;
					grid[row - 1][col].tcell = true;
				}
				break;
			case 2:
				if (!grid[row - 1][col + 1].tcell) {
					tAgents[agentId].col = col + 1;
					tAgents[agentId].row = row - 1;
					grid[row - 1][col + 1].tcell = true;
				}
				break;
			case 3:
				if (!grid[row][col - 1].tcell) {
					tAgents[agentId].col = col - 1;
					grid[row][col - 1].tcell = true;
				}
				break;
			case 4:
				if (!grid[row][col + 1].tcell) {
					tAgents[agentId].col = col + 1;
					grid[row][col + 1].tcell = true;
				}
				break;
			case 5:
				if (!grid[row + 1][col - 1].tcell) {
					tAgents[agentId].col = col - 1;
					tAgents[agentId].row = row + 1;
					grid[row + 1][col - 1].tcell = true;
				}
				break;
			case 6:
				if (!grid[row + 1][col].tcell) {
					tAgents[agentId].row = row + 1;
					grid[row + 1][col].tcell = true;
				}
				break;
			case 7:
				if (!grid[row + 1][col + 1].tcell) {
					tAgents[agentId].col = col + 1;
					tAgents[agentId].row = row + 1;
					grid[row + 1][col + 1].tcell = true;
				}
				break;
		}
	}
}

// Host (CPU) Methods
void performCellRecruitment(Place** d_grid, int size, int day, Macrophage* d_macrophages, TCell* d_tcells) {
	int totalCells = size * size;

	// copy the current simulation state back into CPU
	Place** h_grid = new Place*[size];
	for (int i = 0; i < size; i++) {
		h_grid[i] = new Place[size];
		cudaMemcpy(h_grid[i], d_grid[i], size*sizeof(Place), D2H);
	}
	Macrophage* h_macrophages = new Macrophage[totalCells];
	TCell* h_tcells = new TCell[totalCells];
	cudaMemcpy(h_macrophages, d_macrophages, totalCells*sizeof(Macrophage), D2H);
	cudaMemcpy(h_tcells, d_tcells, totalCells*sizeof(TCell), D2H);

	int quadrant = size / 4;
	for (int i = 0; i < numBloodVessels; i++) {
		int agentRecruitment = day < 10 ? rand() % 2 : rand() % 3;
		if (agentRecruitment == 0) continue;

		int row = 0, col = 0;
		switch (i) {
			case 0:
				row = col = quadrant - 1;
				break;
			case 1:
				row = quadrant - 1;
				col = size - quadrant;
				break;
			case 2:
				row = size - quadrant;
				col = quadrant - 1;
				break;
			case 3:
				row = col = size - quadrant;
				break;
			default:
				break;
		}

		// attempt to recruit macrophage
		int agentIndex = 0;
		if (agentRecruitment == 1) {
			// current cell is occupied with a macrophage, cannot recruit
			if (h_grid[row][col].macrophage) continue;

			// find the first available macrophage spot in the macrophage array
			while(agentIndex < totalCells && h_macrophages != NULL && h_macrophages[agentIndex].currentState != Macrophage::DEAD) {
				agentIndex++;
			}
			if (agentIndex >= totalCells || h_macrophages == NULL) continue;

			// initialize that macrophage
			h_macrophages[agentIndex].currentState = Macrophage::RESTING;
			h_macrophages[agentIndex].internalBacteria = 0;
			h_macrophages[agentIndex].infectedTime = -1;
			h_macrophages[agentIndex].row = row;
			h_macrophages[agentIndex].col = col;
			h_grid[row][col].macrophage = true;

		} else {	// attempt to recruit tcell
			// current cell is occupied with a tcell, cannot recruit
			if (h_grid[row][col].tcell) continue;

			// find the first available tcell spot in the tcell array
			while (agentIndex < totalCells
				&& h_tcells != NULL
				&& !(h_tcells[agentIndex].row == -1 && h_tcells[agentIndex].col == -1)) {
					agentIndex++;
			}
			if (agentIndex >= totalCells || h_tcells == NULL) continue;

			// initalize that cell
			h_tcells[agentIndex].row = row;
			h_tcells[agentIndex].col = col;
			h_grid[row][col].tcell = true;
		}
	}

	// copy updated simulation state back into the device (GPU)
	for (int i = 0; i < size; i++) {
		cudaMemcpy(d_grid[i], h_grid[i], size*sizeof(Place), H2D);
	}
	cudaMemcpy(d_macrophages, h_macrophages, totalCells*sizeof(Macrophage), H2D);
	cudaMemcpy(d_tcells, h_tcells, totalCells*sizeof(TCell), H2D);

	for (int i = 0; i < size; i++) {
		delete[] h_grid[i];
	}
	delete[] h_grid;
	delete[] h_macrophages;
	delete[] h_tcells;
}

void updateMacrophages(Place** grid, int size, Macrophage* mAgents) {
	const int maxAgents = size * size;
	int* randMovement = new int[maxAgents];
	for (int i = 0; i < maxAgents; i++) {
		randMovement[i] = rand() % numNeighbors;
	}
	int* device_randMovement;
	cudaMalloc((void**) &device_randMovement, maxAgents * sizeof(int));
	cudaMemcpy(device_randMovement, randMovement, maxAgents * sizeof(int), H2D);
	moveMacrophage<<<1, maxAgents>>>(grid, size, device_randMovement, mAgents);
	updateMacrophageState<<<1, maxAgents>>>(grid, size, mAgents);
	cudaFree(device_randMovement);
	delete[] randMovement;
}

void updateTCells(Place** grid, int size, TCell* tAgents) {
	const int maxAgents = size * size;
	int* randMovement = new int[maxAgents];
	for (int i = 0; i < maxAgents; i++) {
		randMovement[i] = rand() % numNeighbors;
	}
	int* device_randMovement;
	cudaMalloc((void**) &device_randMovement, maxAgents * sizeof(int));
	cudaMemcpy(device_randMovement, randMovement, maxAgents * sizeof(int), H2D);
	moveTCell<<<1, maxAgents>>>(grid, size, device_randMovement, tAgents);
	cudaFree(device_randMovement);
	delete[] randMovement;
}

void displayResults(Place** d_grid, int size, int totalDays, Macrophage* d_macrophages, ofstream& outputFile) {
	ostringstream ss, mAgents, tAgents, macrophageInfo;
	Place** h_grid = new Place*[size];
	for (int i = 0; i < size; i++) {
		h_grid[i] = new Place[size];
		cudaMemcpy(h_grid[i], d_grid[i], size*sizeof(Place), D2H);
	}
	outputFile << "Tuberculosis Environment at Size " << size << " after " << totalDays << " days" << endl;
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			ss << h_grid[i][j].bacteria << " ";
			mAgents << h_grid[i][j].macrophage << " ";
			tAgents << h_grid[i][j].tcell << " ";
		}
		ss << "\n";
		mAgents << "\n";
		tAgents << "\n";
	}
	ss << "\n";
	mAgents << "\n";
	tAgents << "\n";

	outputFile << "Bacteria" << endl << ss.str() << endl;
	outputFile << "Macrophages" << endl << mAgents.str() << endl;
	outputFile << "TCells" << endl << tAgents.str() << endl;

	int totalCells = size * size;
	Macrophage* h_macrophages = new Macrophage[totalCells];
	cudaMemcpy(h_macrophages, d_macrophages, totalCells*sizeof(Macrophage), D2H);
	outputFile << endl << "Macrophages:" << endl;
	for (int i = 0; i < totalCells; i++) {
		if (h_macrophages[i].currentState >= 0 && h_macrophages[i].currentState < 4
			&& h_macrophages[i].row != 0 && h_macrophages[i].col != 0) {
			macrophageInfo << "Macrophage: {" << h_macrophages[i].row << ", " << h_macrophages[i].col << "},  ";
			macrophageInfo << "currentState: " << h_macrophages[i].currentState << ", ";
			macrophageInfo << "internalBacteria: " << h_macrophages[i].internalBacteria << ", ";
			macrophageInfo << "infectedTime: " << h_macrophages[i].infectedTime << "\n";
		}
	}
	outputFile << macrophageInfo.str() << endl;
}

// Main Simulation
void runDeviceSim(int size, int totalDays, ofstream& outputFile) {
	int totalCells = size * size, macrophageDistribution = 10, i, j, day;
	dim3 block(size, size);

	// Allocate and initalize T-Cells
	TCell* host_tcells = new TCell[totalCells];
	for (i = 0; i < totalCells; i++) {
		host_tcells[i].row = host_tcells[i].col = -1;
	}
	TCell* device_tcells;
	cudaMalloc((void**) &device_tcells, totalCells*sizeof(TCell));
	cudaMemcpy(device_tcells, host_tcells, totalCells*sizeof(TCell), H2D);

	// Allocate and initialize Macrophages
	Macrophage *host_macrophages = new Macrophage[totalCells];
	for (i = 0; i < totalCells; i++) {
		host_macrophages[i].currentState = Macrophage::DEAD;
		host_macrophages[i].row = host_macrophages[i].col = host_macrophages[i].infectedTime = host_macrophages[i].internalBacteria = -1;
	}
	Macrophage* device_macrophages;
	cudaMalloc((void**) &device_macrophages, totalCells*sizeof(Macrophage));

	// Allocate and initialize Places
	Place **host_grid = new Place*[size];
	Place **device_grid;
	cudaMalloc((void**) &device_grid, size*sizeof(Place*));
	Place *device_grid_rows[size];

	int half = size / 2, quadrant = size / 4, host_macrophageCount = 0;
	for (i = 0; i < size; i++) {
		// allocate memory for vector of places
		host_grid[i] = new Place[size];
		cudaMalloc((void**) &device_grid_rows[i], size*sizeof(Place));

		for (j = 0; j < size; j++) {
			host_grid[i][j].bloodVessel = (i == quadrant-1 || i == size-quadrant) && (j == quadrant-1 || j == size-quadrant);
			host_grid[i][j].bacteria = (i == half-1 || i == half) && (j == half-1 || j == half);
			host_grid[i][j].tcell = host_grid[i][j].temp = false;
			host_grid[i][j].chemokine = 0;

			// initialize starting locations of the macrophages
			if (host_grid[i][j].macrophage = rand() % macrophageDistribution == 0) {
				host_macrophages[host_macrophageCount].row = i;
				host_macrophages[host_macrophageCount].col = j;
				host_macrophages[host_macrophageCount].currentState = Macrophage::RESTING;
				host_macrophages[host_macrophageCount].infectedTime = host_macrophages[host_macrophageCount].internalBacteria = 0;
				host_macrophageCount++;
			}
		}

		// copy vector of places to device (GPU)
		cudaMemcpy(device_grid_rows[i], host_grid[i], size*sizeof(Place), H2D);
	}

	// copy vectors of places and macrophages to device (GPU)
	cudaMemcpy(device_grid, device_grid_rows, size*sizeof(Place*), H2D);
	cudaMemcpy(device_macrophages, host_macrophages, totalCells*sizeof(Macrophage), H2D);

	// RUNNING SIMULATION: Iterate through Tuberculosis days
	for (day = 0; day < totalDays; day++) {
		// chemokine decays on place cell as infected macrophages leaves
		computeChemokineDecay<<<1, block>>>(device_grid, size, device_macrophages);

		// bacteria grows every 10 days
		if (day > 0 && day % 10 == 0) {
			bacteriaGrowth<<<1, block>>>(device_grid, size);
		}

		// if cell recruitment can be done, does so.
		performCellRecruitment(device_grid_rows, size, day, device_macrophages, device_tcells);

		// update the state of macrophages
		updateMacrophages(device_grid, size, device_macrophages);

		// update the state of t-cells
		updateTCells(device_grid, size, device_tcells);
	}

	outputFile << endl << "Simulation complete, printing results" << endl;
	displayResults(device_grid_rows, size, totalDays, device_macrophages, outputFile);

	// Clear Device Memory and Heap Memory
	for (i = 0; i < size; i++) {
		cudaFree(device_grid_rows[i]);
		delete[] host_grid[i];
	}
	cudaFree(device_grid);
	cudaFree(device_macrophages);
	cudaFree(device_tcells);
	delete[] host_grid;
	delete[] host_macrophages;
	delete[] host_tcells;
}

int main() {
	int numSimSizes = 5, totalDays = 50;
	int simSizes[] = {32, 64, 128, 256, 512};

	Timer timer;
	ofstream outputFile;
	outputFile.open("Tuberculosis.txt");
	outputFile << "Tuberculosis (GPU implementation)" << endl;

	outputFile << "Starting Simulation" << endl;
	for (int i = 0; i < numSimSizes; i++) {
		timer.start();
	 	runDeviceSim(simSizes[i], totalDays, outputFile);
	 	outputFile << "Time: " << timer.lap() << "ms" << endl << endl;
	}
	outputFile << "Ending Simulation" << endl;
	outputFile.close();

	return 0;
}
