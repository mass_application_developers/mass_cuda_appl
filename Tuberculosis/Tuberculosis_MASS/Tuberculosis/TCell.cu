#include "TCell.h"

MASS_FUNCTION TCell::TCell(mass::AgentState* state, void* arg) : Agent(state, arg) {
  myState = (TCellState*) state;
}

MASS_FUNCTION TCell::~TCell() {}

MASS_FUNCTION void TCell::callMethod(int functionId, void* arg) {
  switch (functionId) {
    case MIGRATE:
      migrate();
      break;
    default:
      break;
  }
}

MASS_FUNCTION TCellState* TCell::getState() {
  return myState;
}

MASS_FUNCTION void TCell::migrate() {
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  if (myPlace == NULL) return;
  int neighborIdx = myPlace->getHighestChemokine();
  if (neighborIdx == -1) { // random
    // migrateAgent();
    return;
  }
  switch (neighborIdx) {
    case 0:
      break;
    case 1:
      break;
    case 2:
      break;
    case 3:
      break;
    case 4:
      break;
    case 5:
      break;
    case 6:
      break;
    case 7:
      break;
    default:
      break;
  }
}
