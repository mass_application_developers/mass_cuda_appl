#ifndef MACROPHAGE_STATE_H
#define MACROPHAGE_STATE_H

#include "../src/AgentState.h"

// A MacrophageState is the state for a Macrophage in the Tuberculosis simulation.
class MacrophageState : public mass::AgentState {
public:
  // The current state, number of internal bacteria, the number of days since
  // the macrophage was first infeceted, and destination index.
  int state, internalBacteria, infectedTime, destinationIdx;
};

#endif
