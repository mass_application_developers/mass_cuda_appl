#include "Macrophage.h"

MASS_FUNCTION Macrophage::Macrophage(mass::AgentState *state, void *argument) : Agent(state, argument) {
  myState = (MacrophageState*) state;
  myState->state = RESTING;
  myState->destinationIdx = myState->internalBacteria = 0;
}

MASS_FUNCTION Macrophage::~Macrophage() {}

MASS_FUNCTION void Macrophage::callMethod(int functionId, void* argument) {
  switch (functionId) {
    case MIGRATE:
      migrate();
      break;
    case RESTING_RULES:
      restingRules();
      break;
    case INFECTED_RULES:
      infectedRules();
      break;
    case ACTIVATED_RULES:
      activatedRules();
      break;
    case CHRONICALLY_INFECTED_RULES:
      chronicallyInfectedRules();
      break;
    case INIT_LOCATIONS:
      // initializeLocations((bool*) argument);
      break;
    case UPDATE_STATE:
      updateState();
      break;
    default:
      break;
  }
}

MASS_FUNCTION MacrophageState* Macrophage::getState() {
    return myState;
}

MASS_FUNCTION bool Macrophage::isProducingChemokine() {
  return myState->state == INFECTED || myState->state == CHRONICALLY_INFECTED;
}

MASS_FUNCTION void Macrophage::migrate() {
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace(); // current place
  if (myPlace != NULL && myPlace->getMigrationDest() != NULL) {  // place exists
    int highestChemokineIdx = myPlace->getHighestChemokine();
    if (highestChemokineIdx == -1) {  // random direction
      // migrateAgent();
    }
    switch (highestChemokineIdx) {  // follow highestChemokineIdx
      case 0: // NW
        break;
      case 1: // N
        break;
      case 2: // NE
        break;
      case 3: // W
        break;
      case 4: // E
        break;
      case 5: // SW
        break;
      case 6: // S
        break;
      case 7: // SE
        break;
      default:
        break;
    }
  }
}

MASS_FUNCTION void Macrophage::updateState() {
  switch (myState->state) {
    case RESTING:
      restingRules();
      break;
    case INFECTED:
      infectedRules();
      break;
    case ACTIVATED:
      activatedRules();
      break;
    case CHRONICALLY_INFECTED:
      chronicallyInfectedRules();
      break;
    default:
      break;
  }
}

MASS_FUNCTION void Macrophage::restingRules() {
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  if (myPlace == NULL || !myPlace->getBacteria()) return;
  myPlace->setBacteria(false);
  myState->internalBacteria = 1;
  myState->state = INFECTED;
  myState->infectedTime = 0;
}

MASS_FUNCTION void Macrophage::infectedRules() {
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  if (myPlace == NULL) return;
  myState->infectedTime++;
  myState->internalBacteria = 2 * myState->infectedTime + 1;
  if (myState->internalBacteria > chronicInfectionLimit) {
    myState->state = CHRONICALLY_INFECTED;
  } else if (myPlace != NULL && myPlace->getAgentExists(false)) {
    myState->state = ACTIVATED;
    myState->internalBacteria = myState->infectedTime = 0;
  }
}

MASS_FUNCTION void Macrophage::activatedRules() {
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  if (myPlace == NULL || !myPlace->getBacteria()) return;
  myPlace->setBacteria(false);
}

MASS_FUNCTION void Macrophage::chronicallyInfectedRules() {
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  if (myPlace == NULL) return;
  myState->internalBacteria += 2;
  if ((myPlace != NULL && myPlace->getAgentExists(false)) || myState->internalBacteria >= bacteriaCapacity)
    burst();
}

MASS_FUNCTION void Macrophage::burst() {
  EnvironmentPlace* myPlace = (EnvironmentPlace*) getPlace();
  if (myPlace == NULL) return;
  myPlace->burst();
  myPlace->setAgentExists(NULL, false);
  terminateAgent();
}

// MASS_FUNCTION void initializeLocations(bool* locations) {
//   // int idx = getIndex();
//   // get EnvironmentPlace
//
// }
