#ifndef TCELL_H
#define TCELL_H

#include "../src/Agent.h"
#include "../src/AgentState.h"
#include "../src/Logger.h"
#include "TCellState.h"
#include "EnvironmentPlace.h"

// The T-Cell is an agent in Tuberculosis simulation that activates infected
// macrophages and kills chronically infected macrophages
class TCell : public mass::Agent {
public:
  // The function ID to migrate a TCell
  const static int MIGRATE = 0;

  MASS_FUNCTION TCell(mass::AgentState* state, void* arg);
  MASS_FUNCTION ~TCell();

  // Calls the method with the given arguments specified by the given function ID.
  MASS_FUNCTION virtual void callMethod(int functionId, void* arg = NULL);

  // Returns the T-Cell's current state.
  MASS_FUNCTION virtual TCellState* getState();
private:
  // The T-Cell's current state.
  TCellState* myState;

  // Migrates the current T-Cell to another EnvironmentPlace if possible.
  MASS_FUNCTION void migrate();
};

#endif
