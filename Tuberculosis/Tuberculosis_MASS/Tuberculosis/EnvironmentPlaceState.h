#ifndef ENVIRONMENT_PLACE_STATE_H
#define ENVIRONMENT_PLACE_STATE_H

#include "../src/PlaceState.h"
#include "Macrophage.h"
#include "TCell.h"

class EnvironmentPlace;
class Macrophage;
class TCell;

// The state for an EnvironmentPlace in the Tuberculosis simulation space.
class EnvironmentPlaceState : public mass::PlaceState {
public:
  // Whether the bacterium exists.
  bool bacteria;

  // Whether the current EnvironmentPlace is an entry point for simulation agents.
  bool bloodVessel;

  // The chemokine level.
  int chemokine;

  // The current Macrophage occupying the EnvironmentPlace.
  Macrophage* currentMacrophage;

  // The current T-Cell occupying the EnvironmentPlace.
  TCell* currentTCell;

  // The EnvironmentPlace of a migratation destination.
  EnvironmentPlace* migrationDest;

  // The index of the relative migratation destination.
  int migrationDestRelativeIdx;
};

#endif
