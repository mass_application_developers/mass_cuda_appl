#ifndef TCELL_STATE_H
#define TCELL_STATE_H

// The TCellState is the state for a TCell agent in the Tuberculosis simulation.
class TCellState : public mass::AgentState {
public:
  // The destination index.
  int destinationIdx;
};

#endif
