#include "Tuberculosis.h"
#include "EnvironmentPlace.h"
#include "Macrophage.h"
#include "TCell.h"

#include <iostream>
#include <sstream>     // ostringstream
#include <vector>
#include <stdlib.h>   // random

#include "../src/Mass.h"
#include "../src/Logger.h"
#include "Timer.h"

using namespace std;
using namespace mass;

Tuberculosis::Tuberculosis() {}
Tuberculosis::~Tuberculosis() {}

void Tuberculosis::runMassSim(int size, int totalDays) {
  Mass::init();

  const int nDims = 2, totalCells = size * size, agentDistribution = 10;
  // const int bloodVessels = 4;
  int placesSize[nDims] = {size, size};

  Places* places = Mass::createPlaces<EnvironmentPlace, EnvironmentPlaceState>(0, NULL, sizeof(double), nDims, placesSize);
  places->callAll(EnvironmentPlace::INIT_BACTERIA);

  Agents* macrophages = Mass::createAgents<Macrophage, MacrophageState> (1, NULL, sizeof(double), 0, 0);
  bool locations[totalCells];
  for (int i = 0; i < totalCells; i++) {
    locations[i] = rand() % agentDistribution == 0;
  }
  macrophages->callAll(Macrophage::INIT_LOCATIONS, locations, sizeof(bool)*totalCells);
  macrophages->manageAll();

  Agents* tcells = Mass::createAgents<TCell, TCellState> (2, NULL, sizeof(double), 0, 0);

  vector<int*> neighbors;   // {x, y}
  int northwest[2] = {-1, 1};
  neighbors.push_back(northwest);
  int north[2] = {0, 1};
  neighbors.push_back(north);
  int northeast[2] = {1, 1};
  neighbors.push_back(northeast);
  int west[2] = {-1, 0};
  neighbors.push_back(west);
  int east[2] = {1, 0};
  neighbors.push_back(east);
  int southwest[2] = {-1, -1};
  neighbors.push_back(southwest);
  int south[2] = {0, -1};
  neighbors.push_back(south);
  int southeast[2] = {1, -1};
  neighbors.push_back(southeast);

  for (int i = 0; i < totalDays; i++) {
    places->exchangeAll(&neighbors, EnvironmentPlace::CHEMOKINE_DECAY, NULL, 0);

    if (i > 0 && i % 10 == 0)   // grows every 10 days
      places->exchangeAll(&neighbors, EnvironmentPlace::BACTERIA_GROWTH, NULL, 0);

    // int recruitment[bloodVessels];
    // for (int j = 0; j < bloodVessels; j++) {
    //   recruitment[i] = i < 10 ? rand() % 2 : rand() % 3;
    // }
    // places? agents?

    macrophages->callAll(Macrophage::MIGRATE);
    macrophages->manageAll();
    macrophages->callAll(Macrophage::UPDATE_STATE);

    tcells->callAll(TCell::MIGRATE);
    tcells->manageAll();
  }

  displayPlaces(places, totalDays, size);
  displayAgents(macrophages, totalDays);

  Mass::finish();
}

void Tuberculosis::displayPlaces(Places* places, int totalDays, int size) {
  ostringstream ss, macrophages, tcells;

  ss << "Tuberculosis Environment at size " << size << " after " << totalDays << " days\n";
  Place** retVals = places->getElements();
  int indices[2];
  for (int row = 0; row < size; row++) {
    indices[0] = row;
    for (int col = 0; col < size; col++) {
      indices[1] = col;
      int rmi = places->getRowMajorIdx(indices);
      if (rmi != (row % size) * size + col) {
        Logger::error("Row Major Index is incorrect: [%d][%d] != %d", row, col, rmi);
      }
      EnvironmentPlace* currentPlace = (EnvironmentPlace*) retVals[rmi];
      ss << currentPlace->getBacteria() << " ";
      macrophages << currentPlace->getAgentExists(true) << " ";
      tcells << currentPlace->getAgentExists(false) << " ";
    }
    ss << "\n";
    macrophages << "\n";
    tcells << "\n";
  }
  ss << "\n";
  macrophages << "\n";
  tcells << "\n";
  Logger::print(ss.str());
  Logger::print(macrophages.str());
  Logger::print(tcells.str());
}

void Tuberculosis::displayAgents(Agents* agents, int totalDays) {
	ostringstream ss;
	ss << "Tuberculosis Macrophages after " << totalDays << " days\n";
	mass::Agent** retVals = agents->getElements();
	int totalAliveAgents = agents->getNumAgents();
	for (int i = 0; i < totalAliveAgents; i++) {
    ss << "Macrophage[" << i << "] at location " << retVals[i]->getPlaceIndex();
    MacrophageState* currentState = (MacrophageState*)(retVals[i]->getState());
		ss << ", internalBacteria: " << currentState->internalBacteria;
    ss << ", currentState: " << currentState->state << "\n";
	}
	Logger::print(ss.str());
}
