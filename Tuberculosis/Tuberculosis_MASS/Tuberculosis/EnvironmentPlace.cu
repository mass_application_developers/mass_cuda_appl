#include "EnvironmentPlace.h"

using namespace std;
using namespace mass;

MASS_FUNCTION EnvironmentPlace::EnvironmentPlace(PlaceState* state, void* argument) : Place(state, argument) {
  myState = (EnvironmentPlaceState*) state;
  myState->bacteria = myState->bloodVessel = false;
  myState->chemokine = 0;
  myState->currentMacrophage = NULL;
  myState->currentTCell = NULL;
}

MASS_FUNCTION EnvironmentPlace::~EnvironmentPlace() {}

MASS_FUNCTION void EnvironmentPlace::callMethod(int functionId, void* arg) {
  switch (functionId) {
    case INIT_BACTERIA:
      initialize(myState->index, myState->size[0]);
      break;
    case CHEMOKINE_DECAY:
      chemokineDecay();
      break;
    case BACTERIA_GROWTH:
      bacteriaGrowth();
      break;
    default:
      break;
  }
}

MASS_FUNCTION bool EnvironmentPlace::getBacteria() {
  return myState->bacteria;
}

MASS_FUNCTION void EnvironmentPlace::setBacteria(bool newBacteria) {
  if (myState->bacteria != newBacteria) {
    myState->bacteria = newBacteria;
  }
}

MASS_FUNCTION int EnvironmentPlace::getChemokine() {
  return myState->chemokine;
}

MASS_FUNCTION void EnvironmentPlace::setChemokine(int newChemokine) {
  int chemokineLevel = newChemokine;
  if (chemokineLevel < 0)
    chemokineLevel = 0;
  else if (chemokineLevel > maxChemokine)
    chemokineLevel = maxChemokine;

  if (myState->chemokine != chemokineLevel)
    myState->chemokine = chemokineLevel;
}

MASS_FUNCTION bool EnvironmentPlace::isGoodForMigration(bool isMacrophage) {
  return !getAgentExists(isMacrophage);
}

MASS_FUNCTION EnvironmentPlace* EnvironmentPlace::getMigrationDest() {
  return myState->migrationDest;
}

MASS_FUNCTION int EnvironmentPlace::getMigrationDestRelIdx() {
    return myState->migrationDestRelativeIdx;
}

MASS_FUNCTION bool EnvironmentPlace::getAgentExists(bool isMacrophage) {
  return isMacrophage ? myState->currentMacrophage != NULL : myState->currentTCell != NULL;
}

MASS_FUNCTION void EnvironmentPlace::setAgentExists(mass::Agent* agent, bool isMacrophage) {
  if (isMacrophage)
    myState->currentMacrophage = agent == NULL ? NULL : (Macrophage*) agent;
  else
    myState->currentTCell = agent == NULL ? NULL : (TCell*) agent;
}

MASS_FUNCTION Macrophage* EnvironmentPlace::getCurrentMacrophage() {
  return myState->currentMacrophage;
}

MASS_FUNCTION TCell* EnvironmentPlace::getCurrentTCell() {
  return myState->currentTCell;
}

MASS_FUNCTION void EnvironmentPlace::burst() {
  setBacteria(true);
  for (int i = 0; i < MAX_NEIGHBORS; i++) {
    EnvironmentPlace* neighbor = (EnvironmentPlace*) myState->neighbors[i];
    if (neighbor != NULL)
      neighbor->setBacteria(true);
  }
}

MASS_FUNCTION int EnvironmentPlace::getHighestChemokine() {
  int highestChemokine = 0, neighborIdx = -1;
  for (int i = 0; i < MAX_NEIGHBORS; i++) {
    EnvironmentPlace* neighbor = (EnvironmentPlace*) myState->neighbors[i];
    if (neighbor != NULL) {
      int neighborChemokine = neighbor->getChemokine();
      if (neighborChemokine < highestChemokine) continue;
      highestChemokine = neighborChemokine;
      neighborIdx = i;
    }
  }
  return neighborIdx;
}

MASS_FUNCTION void EnvironmentPlace::initialize(int index, int size) {
  int j = index % size, i = index / size;
  int half = size / 2, quadrant = size / 4;
  if ((i == half-1 || i == half) && (j == half-1 || j == half)) {
    setBacteria(true);
  }
  if ((i == quadrant-1 || i == size-quadrant) && (j == quadrant-1 || j == size-quadrant)) {
    myState->bloodVessel = true;
  }
}

MASS_FUNCTION void EnvironmentPlace::chemokineDecay() {
  setChemokine(getChemokine() - 1);
  if (!getAgentExists(true)) return;
  Macrophage* current = getCurrentMacrophage();
  if (current->isProducingChemokine()) {
    setChemokine(maxChemokine);
    for (int i = 0; i < MAX_NEIGHBORS; i++) {
      EnvironmentPlace* neighbor = (EnvironmentPlace*) myState->neighbors[i];
      if (neighbor != NULL)
        neighbor->setChemokine(maxChemokine);
    }
  }
}

MASS_FUNCTION void EnvironmentPlace::bacteriaGrowth() {
  if (!getBacteria()) return;
  for (int i = 0; i < MAX_NEIGHBORS; i++) {
    EnvironmentPlace* neighbor = (EnvironmentPlace*) myState->neighbors[i];
    if (neighbor != NULL)
      neighbor->setBacteria(true);
  }
}
