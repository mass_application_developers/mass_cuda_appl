#ifndef TUBERCULOSIS_H
#define TUBERCULOSIS_H

#include "../src/Places.h"
#include "../src/Agents.h"
#include "Macrophage.h"

// Tuberculosis simulates the interaction between Tuberculosis bacteria and
// an immune system of macrophages and t-cells.
class Tuberculosis {
public:
  Tuberculosis();
  virtual ~Tuberculosis();

  // Executes the Tuberculosis simulation using the MASS CUDA library.
  void runMassSim(int size, int totalDays);

  // Displays the given simulation space of places after the given number of
  // days and at the given simulation size.
  void displayPlaces(mass::Places* places, int totalDays, int size);

  // Displays the internal state of the given agents after the given number of
  // days in the Tuberculosis simulation.
  void displayAgents(mass::Agents* agents, int totalDays);
};

#endif
