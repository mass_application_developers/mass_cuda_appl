#include "../src/Logger.h"
#include "Tuberculosis.h"

using namespace std;
using namespace mass;

// Executes the Tuberculosis simulation using various simulation sizes and steps.
int main() {
	Logger::setLogFile("Tuberculosis.txt");
	Logger::info("Tuberculosis (MASS Implementation)");

	const int simSizes = 5, totalDays = 20;
	int size[simSizes] = {32, 64, 128, 256, 512};

	Logger::print("Starting Simulation\n");
	Tuberculosis tuberculosis;
	for (int i = 0; i < simSizes; i++) {
		tuberculosis.runMassSim(size[i], totalDays);
	}
	Logger::print("Ending Simulation");

	return 0;
}
