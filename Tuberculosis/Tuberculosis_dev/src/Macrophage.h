#ifndef MACROPHAGE_H
#define MACROPHAGE_H

#include "mass/Agent.h"
#include "mass/AgentState.h"
#include "mass/Logger.h"
#include "MacrophageState.h"
#include "EnvironmentPlace.h"

// The Macrophage is an agent in the Tuberculosis simulation that detects foreign
// bacteria in the immune system. Macrophages can be various states depending on
// the history of interactions in the simulation space.
class Macrophage : public mass::Agent {
public:
  // The function ID to migrate.
  const static int MIGRATE = 0;

  // The function ID to apply the resting rules.
  const static int RESTING_RULES = 1;

  // The function ID to apply the infected rules.
  const static int INFECTED_RULES = 2;

  // The function ID to apply the activated rules.
  const static int ACTIVATED_RULES = 3;

  // The function ID to apply the chronically infected rules.
  const static int CHRONICALLY_INFECTED_RULES = 4;

  // The function ID to initialize the location of the current macrophage.
  const static int INIT_LOCATIONS = 5;

  // The function ID to update the current macrophage's state.
  const static int UPDATE_STATE = 6;

  // The macrophage's resting state.
  const static int RESTING = 0;

  // The macrophage's infected state.
  const static int INFECTED = 1;

  // The macrophage's activated state.
  const static int ACTIVATED = 2;

  // The macrophage's chronically infected state.
  const static int CHRONICALLY_INFECTED = 3;

  MASS_FUNCTION Macrophage(mass::AgentState* state, void* argument);
  MASS_FUNCTION ~Macrophage();

  // Calls the method with the given arguments specified by the given function ID.
  __device__ virtual void callMethod(int functionId, void* arg = NULL);

  // Returns the Macrophage's current state.
  MASS_FUNCTION virtual MacrophageState* getState();

  // Returns whether the current macrophage is producing chemokine.
  MASS_FUNCTION bool isProducingChemokine();

private:
  // The macrophage's current state.
  MacrophageState* myState;

  // The bacteria capacity that a macrophage can hold.
  const int bacteriaCapacity = 100;

  // The threshold of held bacteria which turns an infected macrophage to a
  // chronically infected macrophage.
  const int chronicInfectionLimit = 75;

  // Migrates the current macrophage to another EnvironmentPlace if possible.
  MASS_FUNCTION void migrate();

  // Applies the rules for a resting macrophage.
  MASS_FUNCTION void restingRules();

  // Applies the rules for a infected macrophage.
  MASS_FUNCTION void infectedRules();

  // Applies the rules for an activated macrophage.
  MASS_FUNCTION void activatedRules();

  // Applies the rules for a chronically infected macrophage.
  MASS_FUNCTION void chronicallyInfectedRules();

  // Destroys the infected macrophage and spreads chemokine to the surrounding
  // EnvironmentPlaces.
  MASS_FUNCTION void burst();

  // Initializes the location of a macrophage based on its' index in the given
  // bool array of locations.
  MASS_FUNCTION void initializeLocations(bool* locations);

  // Updates the state of the macrophage based on its' current state.
  MASS_FUNCTION void updateState();
};

#endif
