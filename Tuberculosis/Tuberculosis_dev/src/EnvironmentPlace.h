#ifndef ENVIRONMENT_PLACE_H
#define ENVIRONMENT_PLACE_H

#include "mass/Place.h"
#include "mass/Logger.h"
#include "EnvironmentPlaceState.h"
#include "Macrophage.h"
#include "TCell.h"

class Macrophage;
class TCell;

// An EnvironmentPlace is a cell in the Tuberculosis simulation space.
class EnvironmentPlace : public mass::Place {
public:
  // The function ID for initializing bacteria.
  const static int INIT_BACTERIA = 0;

  // The function ID for updating the decay of chemokine levels of cells.
  const static int CHEMOKINE_DECAY = 1;

  // The function ID for updating the growth of bacteria.
  const static int BACTERIA_GROWTH = 2;

  MASS_FUNCTION EnvironmentPlace(mass::PlaceState* state, void* arg);
  MASS_FUNCTION ~EnvironmentPlace();

  // Calls the method with the given arguments specified by the given function ID.
  MASS_FUNCTION virtual void callMethod(int functionId, void* arg = NULL);

  // Returns whether the bacterium exists.
  MASS_FUNCTION bool getBacteria();

  // Sets the existence of bacterium.
	MASS_FUNCTION void setBacteria(bool bacteria);

  // Returns the chemokine level.
  MASS_FUNCTION int getChemokine();

  // Sets the chemokine level.
	MASS_FUNCTION void setChemokine(int chemokine);

  // Returns whether the current cell has an agent specified by the given bool.
	MASS_FUNCTION bool isGoodForMigration(bool isMacrophage);

  // Returns a pointer to an EnvironmentPlace as the destination pointer for agent
  // migration.
	MASS_FUNCTION EnvironmentPlace* getMigrationDest();

  // Returns an index of the relative migration destination.
	MASS_FUNCTION int getMigrationDestRelIdx();

  // Returns whether there is an agent occupying the EnvironmentPlace specified
  // by the given bool.
  MASS_FUNCTION bool getAgentExists(bool isMacrophage);

  // Sets the occupancy of an agent at the EnvironmentPlace.
  MASS_FUNCTION void setAgentExists(mass::Agent* agent, bool isMacrophage);

  // Returns the current Macrophage agent occupying the EnvironmentPlace.
  MASS_FUNCTION Macrophage* getCurrentMacrophage();

  // Returns the current T-Cell agent occupying the EnvironmentPlace.
  MASS_FUNCTION TCell* getCurrentTCell();

  // Updates the surrounding EnvironmentPlaces when the occupying Macrophage bursts.
  MASS_FUNCTION void burst();

  // Returns the relative index of the EnvironmentPlace with the highest chemokine
  // levels.
  MASS_FUNCTION int getHighestChemokine();

private:
  // The state of the EnvironmentPlace.
  EnvironmentPlaceState* myState;

  // The maximum valid chemokine level.
  const int maxChemokine = 2;

  // Initializes an EnvironmentPlace using the given simulation space index and
  // simulation size.
  MASS_FUNCTION void initialize(int index, int size);

  // Updates the chemokine level to decay with each simulation step
  MASS_FUNCTION void chemokineDecay();

  // Updates the growth of bacterium in the simulation space.
  MASS_FUNCTION void bacteriaGrowth();
};

#endif
