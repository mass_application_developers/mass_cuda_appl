# AppTemplate

The AppTemplate is intended to assist application developers in getting started developing applications using the MASS CUDA library. To develop an application, simply copy the `apptemplate` directory to your project root and rename `PROJECT_NAME` in the Makefile. Running `make develop` will install all the dependencies necessary to start developing, and testing, your MASS CUDA application. The version of the MASS CUDA library installed is pinned at the `MASS_VERSION` variable within the Makefile. Modify this to change the library version used by your application. Lastly, the MASS CUDA library can be configured by changing the variables within the `MASS Config` section of the Makefile.

Once your development environment is setup, running `make build` will build your application and place the compiled binary within `./bin/<project_name>`. Once built, tests can be run for your application by running `make test`. Tests can placed within the `test` directory.

### Application Optimization
    
To tailor library settings and kernel launch configurations to a particular application, set the appropriate MASS config options under the `MASS Config` section of the application Makefile.

_Example_
```
## MASS Config
MASS_MAX_AGENTS		= 1
MASS_MAX_NEIGHBORS	= 12
MASS_MAX_DIMS		= 2
MASS_N_DESTINATIONS	= 12
MASS_BLOCK_SIZE		= 24
```