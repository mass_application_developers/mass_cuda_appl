#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "Tuberculosis.h"
#include <gtest/gtest.h>
#include <mass/Mass.h>

Tuberculosis tuberculosis;

TEST(Tuberculosis, Test1){
    const int simSizes = 5, totalDays = 150;
	int size[simSizes] = {32, 64, 128, 256, 512};

	for (int i = 0; i < 1; i++) {
		tuberculosis.runMassSim(64, totalDays);
	}
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
