# SugarScape

To build and run the SugarScape application, first run `make develop`. Once the development environment is setup run `make build` and then `make test` to ensure all tests are passing properly. Once built, a `SugarScape` executable will be placed within the `./bin` directory, and can be used to run the simulation.

Running the application with the `--help` flag will provide a description of the application and options that can be used to define simulation parameters.

Specifying an output interval, by using the `--interval` flag will tell the program to output the simulation to a `.viz` file every `<interval>` steps. This simulation file can be played using the MASS SimViz application for a graphical visualization of the simulation space.

In the examples below, the sugar mounds are colored in varying shades of yellow depending on the amount of sugar at the given cell. The lighter the shade, the less sugar. Ants are colored red.

_Example output with a simulaton space of 100x100_

![Braingrid (100x100)](./img/sugarscape_100x100.gif)

```
## Command to reproduce the above simulation
./bin/SugarScape --interval=1
```

_Example output with a simulaton space of 1000x1000_

![Braingrid (1000x1000)](./img/sugarscape_1000x1000.gif)

```
## Command to reproduce the above simulation
./bin/SugarScape --interval=1 --size=1000
```
