#pragma once

#include "mass/PlaceState.h"

class SugarPlace; //forward declaration

class SugarPlaceState: public mass::PlaceState {
public:

    int curSugar, maxSugar;
    double pollution, avePollution;
    int randState;

    SugarPlace* migrationDest;  //available migration destination within visibility range from this place
    int migrationDestRelativeIdx;
};
