#pragma once

#include "mass/Places.h"
#include "mass/Agents.h"
#include "simviz.h"

static const int maxInitAgentSugar = 10;
static const int maxMetabolism = 4;

void runMassSim(int size, int seed, int max_time, int interval, simviz::RGBFile& vizFile);
void displaySugar(simviz::RGBFile& rgbFile, mass::Places *places, int time, int *placesSize);
int* generateRandVals(int n);
