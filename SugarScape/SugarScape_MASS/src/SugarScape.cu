#include "SugarScape.h"

#include <iostream>
#include <cstdlib>
#include <math.h>  // floor
#include <sstream> // ostringstream
#include <vector>

#include "mass/Mass.h"
#include "mass/Logger.h"

#include "SugarPlace.h"
#include "Ant.h"
#include "Timer.h"
#include "CudaEventTimer.h"

void runMassSim(int size, int seed, int max_time, int interval, simviz::RGBFile &vizFile)
{
	mass::logger::debug("Starting MASS CUDA simulation\n");
	mass::logger::debug("MASS Config: MAX_AGENTS=%d, MAX_NEIGHBORS=%d, MAX_DIMS=%d, N_DESTINATIONS=%d",
						MAX_AGENTS,
						MAX_NEIGHBORS,
						MAX_DIMS,
						N_DESTINATIONS);

	std::srand(seed);
	mass::logger::info("seed used for simulation: %d", seed);

	int nDims = 2;
	int placesSize[] = {size, size};
	int numCells = placesSize[0] * placesSize[1];
	int *randVals = generateRandVals(numCells);

	// initialization parameters
	int nAgents = size * size / 5;

	// create an array of random agentSugar and agentMetabolism values
	int agentSugarArray[nAgents];
	int agentMetabolismArray[nAgents];
	for (int i = 0; i < nAgents; i++)
	{
		agentSugarArray[i] = rand() % maxInitAgentSugar + 1;
		agentMetabolismArray[i] = rand() % maxMetabolism + 1;
	}

	// create neighborhood for average pollution calculations
	std::vector<int *> neighbors;
	int top[2] = {0, 1};
	neighbors.push_back(top);
	int right[2] = {1, 0};
	neighbors.push_back(right);
	int bottom[2] = {0, -1};
	neighbors.push_back(bottom);
	int left[2] = {-1, 0};
	neighbors.push_back(left);

	// create a vector of possible target destination places for an ant
	std::vector<int *> migrationDestinations;
	for (int i = 1; i <= maxVisible; i++)
	{
		migrationDestinations.push_back(new int[2]{0, i});	// North
		migrationDestinations.push_back(new int[2]{i, 0});	// East
		migrationDestinations.push_back(new int[2]{0, -i}); // South
		migrationDestinations.push_back(new int[2]{-i, 0}); // West
	}

	// start a timer
	Timer timer;
	timer.start();

	// Timer for initialization CUDA time
    std::unique_ptr<mass::CudaEventTimer> initTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
    initTimer->startTimer();

	// start the MASS CUDA library processes
	mass::Mass::init();

	// initialize places
	mass::Places *places = mass::Mass::createPlaces<SugarPlace, SugarPlaceState>(
		0,	  // handle
		NULL, // args
		0,
		nDims,
		placesSize);

	places->callAll(SugarPlace::SET_SUGAR); // set proper initial amounts of sugar

	// initialize agents:
	mass::Agents *agents = mass::Mass::createAgents<Ant, AntState>(
		1 /*handle*/,
		NULL /*arguments*/,
		0,
		nAgents,
		0 /*placesHandle*/
	);

	// set proper initial amounts of sugar and metabolism for agents
	agents->callAll(Ant::SET_INIT_SUGAR, agentSugarArray, sizeof(int) * nAgents);
	agents->callAll(Ant::SET_INIT_METABOLISM, agentMetabolismArray, sizeof(int) * nAgents);

	places->callAll(SugarPlace::INIT_RAND, randVals, sizeof(int) * numCells);
	delete[] randVals;

	// Record initialization time
	initTimer->stopTimer();

	// Used to record the total CUDA event time for each step
	float avgStepTime = 0.0;

	int t = 0;
	for (; t < max_time; t++)
	{
		// Record per-step CUDA event time
		std::unique_ptr<mass::CudaEventTimer> stepTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
		stepTimer->startTimer();

		places->callAll(SugarPlace::INC_SUGAR_AND_POLLUTION);

		places->exchangeAll(&neighbors, SugarPlace::AVE_POLLUTIONS, NULL /*argument*/, 0 /*argSize*/);
		places->callAll(SugarPlace::UPDATE_POLLUTION_WITH_AVERAGE);

		places->exchangeAll(&migrationDestinations, SugarPlace::FIND_MIGRATION_DESTINATION, NULL /*argument*/, 0 /*argSize*/);

		agents->callAll(Ant::MIGRATE);

		agents->manageAll();

		agents->callAll(Ant::METABOLIZE);
		agents->manageAll();

		stepTimer->stopTimer();
		float elapsedTime = stepTimer->getElapsedTime();
		avgStepTime += elapsedTime;

		// display intermediate results
		if (interval != 0 && (t % interval == 0 || t == max_time - 1))
		{
			displaySugar(vizFile, places, t, placesSize);
		}
	}

	mass::logger::info("MASS total time %ld ms", timer.lap() / 1000);
	mass::logger::info("MASS initialization CUDA time %f ms", initTimer->getElapsedTime());
	mass::logger::info("MASS CUDA average step time %f ms", avgStepTime / max_time);
	mass::logger::info("simulation complete (seed=%d), shutting down...", seed);

	// terminate the processes
	mass::Mass::finish();
}

void displaySugar(simviz::RGBFile &rgbFile, mass::Places *places, int time, int *placesSize)
{
	mass::logger::debug("Entering SugarScape::displayResults");
	unsigned char *placeColor = new unsigned char[3]{192, 192, 192};  // grey
	unsigned char *sugarColor1 = new unsigned char[3]{255, 255, 153}; // light yellow
	unsigned char *sugarColor2 = new unsigned char[3]{255, 255, 102}; // medium yellow
	unsigned char *sugarColor3 = new unsigned char[3]{204, 204, 0};	  // dark yellow
	unsigned char *agentColor = new unsigned char[3]{192, 0, 0};	  // red

	mass::Place **retVals = places->getElements();
	int indices[2];
	for (int row = 0; row < placesSize[0]; row++)
	{
		indices[0] = row;
		for (int col = 0; col < placesSize[1]; col++)
		{
			indices[1] = col;
			int rmi = places->getRowMajorIdx(indices);
			if (rmi != (row % placesSize[0]) * placesSize[0] + col)
			{
				mass::logger::error("Row Major Index is incorrect: [%d][%d] != %d",
									row, col, rmi);

				continue;
			}

			int curSugar = ((SugarPlace *)retVals[rmi])->getCurSugar();
			int n_agents = retVals[rmi]->getAgentPopulation();

			if (n_agents > 0)
			{
				rgbFile.write((char *)agentColor, simviz::NumRGBBytes);
			}
			else if (curSugar > 2)
			{
				rgbFile.write((char *)sugarColor3, simviz::NumRGBBytes);
			}
			else if (curSugar > 1)
			{
				rgbFile.write((char *)sugarColor2, simviz::NumRGBBytes);
			}
			else if (curSugar > 0)
			{
				rgbFile.write((char *)sugarColor1, simviz::NumRGBBytes);
			}
			else
			{
				rgbFile.write((char *)placeColor, simviz::NumRGBBytes);
			}
		}
	}
}

int *generateRandVals(int n)
{
	int *randVals = new int[n];
	for (int i = 0; i < n; i++)
	{
		randVals[i] = rand();
	}

	return randVals;
}
