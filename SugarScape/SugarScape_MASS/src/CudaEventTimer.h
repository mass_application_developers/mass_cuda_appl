#pragma once

#include "mass/MassException.h"
#include "mass/cudaUtil.h"

namespace mass {
    class CudaEventTimer {
        private:
            cudaEvent_t start; // CUDA event to record start time
            cudaEvent_t stop; // CUDA event to record stop time
            bool synced; // Whether the timer has been synced
            bool started; // Whether the timer has been started
            bool stopped; // Whether the timer has been stopped
            float elapsedTime; // The elapsed time in milliseconds

            /**
             * @brief Syncs the timer and calculates the elapsed time
             * 
             * @return float The elapsed time in milliseconds
            */
            void sync() {
                if(!started){
                    throw MassException("CudaEventTimer::sync() - Timer has not been started before syncing");
                }
                if(!stopped){
                    throw MassException("CudaEventTimer::sync() - Timer has not been stopped before syncing");
                }

                // Sync the timer
                CATCH(cudaEventSynchronize(this->stop));
                CATCH(cudaEventElapsedTime(&this->elapsedTime, this->start, this->stop));
                this->synced = true;
            }

        public:
            /**
             * @brief Construct a new CudaEventTimer object
             * 
             */
            CudaEventTimer() {
                // Create the events
                CATCH(cudaEventCreate(&this->start));
                CATCH(cudaEventCreate(&this->stop));

                // Set the flags
                this->synced = false;
                this->started = false;
                this->stopped = false;

                // Set the elapsed time
                this->elapsedTime = 0.0f;
            }

            /**
             * @brief Destroy the CudaEventTimer object
             * 
             */
            ~CudaEventTimer() {
                // Destroy the events
                // CATCH(cudaEventDestroy(this->start));
                // CATCH(cudaEventDestroy(this->stop));

                // MASS.finish() will reset the entire device,
                // calling cudaEventDestroy() here will cause a
                // invalid context error
                // So we just leave the events as is
                // and MASS.finish() will clean it up
            }

            /**
             * @brief Starts the timer
             * 
             */
            void startTimer() {
                if(this->started){
                    throw MassException("CudaEventTimer::startTimer() - Timer has already been started");
                }
                if(this->stopped){
                    throw MassException("CudaEventTimer::startTimer() - Timer has already been stopped");
                }

                // Start the timer
                CATCH(cudaEventRecord(this->start));
                this->started = true;
            }

            /**
             * @brief Stops the timer
             * 
             */
            void stopTimer() {
                if(!this->started){
                    throw MassException("CudaEventTimer::stopTimer() - Timer has not been started");
                }
                if(this->stopped){
                    throw MassException("CudaEventTimer::stopTimer() - Timer has already been stopped");
                }

                // Stop the timer
                CATCH(cudaEventRecord(this->stop));
                this->stopped = true;
            }

            /**
             * @brief Gets the elapsed time in milliseconds
             * 
             * @return float The elapsed time in milliseconds
             */
            float getElapsedTime() {
                if(!this->synced){
                    this->sync();
                }
                return this->elapsedTime;
            }
    };
}