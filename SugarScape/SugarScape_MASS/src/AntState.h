
#pragma once

#include "mass/AgentState.h"

class AntState: public mass::AgentState {
public:
    int agentSugar, agentMetabolism, destinationIdx;
};
