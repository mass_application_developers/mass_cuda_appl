#include <ctime> // clock_t,
#include <stdio.h> //remove after debugging
#include "SugarPlace.h"

static const int maxMtSugar = 4; //max level of sugar in mountain peak

MASS_FUNCTION SugarPlace::SugarPlace(mass::PlaceState* state, void *argument) :
		mass::Place(state, argument) {
	myState = (SugarPlaceState*) state;

    myState -> pollution = 0.0;    // the current pollution
    myState -> avePollution = 0.0; // averaging four neighbors' pollution

    myState -> curSugar = 0;
    myState -> maxSugar = 0;
    myState -> randState = 0;
}

MASS_FUNCTION void SugarPlace::setSugar() {
	int mtCoord[2];
    int size = myState->size[0];

    mtCoord[0] = size/3;
    mtCoord[1] = size - size/3 - 1;
    
    int mt1 = initSugarAmount(myState ->index, size, mtCoord[0], mtCoord[1], maxMtSugar);
    int mt2 = initSugarAmount(myState ->index, size, mtCoord[1], mtCoord[0], maxMtSugar);
    
    myState -> curSugar = mt1 > mt2 ? mt1 : mt2;
    myState -> maxSugar = mt1 > mt2 ? mt1 : mt2;
}

MASS_FUNCTION int SugarPlace::initSugarAmount(int idx, int size, int mtPeakX, int mtPeakY, int maxMtSug) {
    int x_coord = idx % size;
    int y_coord = idx / size;  //division by 0
    
    float distance = std::sqrt((float)(( mtPeakX - x_coord ) * ( mtPeakX - x_coord ) + (mtPeakY - y_coord) * (mtPeakY - y_coord)));

    // radius is assumed to be simSize/2.
    int r = size/2;
    if ( distance < r )
    {
        // '+ 0.5' for rounding a value.
        return ( int )( maxMtSug + 0.5 - maxMtSug / ( float )r * distance );
    }
    else
        return 0;
}

MASS_FUNCTION void SugarPlace::incSugarAndPollution() {
    if ( myState->curSugar < myState->maxSugar )
    {
        myState->curSugar++;
    }
    myState->pollution += 1.0;
}

// Calculates average pollution between 4 neighbors
MASS_FUNCTION void SugarPlace::avePollutions() { 
    
    int idx = myState -> index;
    int size = myState -> size[0];

    double top, right, bottom, left;

    if (idx - size >= 0) { //top
    	top = ((SugarPlace*)myState->neighbors[0])->getPollution();
    } else {
    	top = 0.0;
    }
	
	if ((idx +1) % size != 0) { //right
    	right = ((SugarPlace*)myState->neighbors[1])->getPollution();
    } else {
    	right = 0.0;
    }

    if (idx + size < size*size) { //bottom
    	bottom = ((SugarPlace*)myState->neighbors[2])->getPollution();
    } else {
    	bottom = 0.0;
    }

    if (idx % size != 0) { //left
    	left = ((SugarPlace*)myState->neighbors[3])->getPollution();
    } else {
    	left = 0.0;
    }

    myState->avePollution = ( top + bottom + left + right ) / 4.0;
}

MASS_FUNCTION void SugarPlace::updatePollutionWithAverage() {
    myState->pollution = myState->avePollution;
    myState->avePollution = 0.0;
}

// canMoveNorth returns true if the sim-space can accomodate an agent
// moving north by `steps` number of cells, false otherwise.
MASS_FUNCTION bool canMoveNorth(int idx, int steps, int rowLength) {
    return idx - (steps * rowLength) >= 0;
}

// canMoveEast returns true if the sim-space can accomodate an agent
// moving east by `steps` number of cells, false otherwise.
MASS_FUNCTION bool canMoveEast(int idx, int steps, int rowLength) {
    int col = idx % rowLength;
    return (col + steps) < rowLength;
}

// canMoveSouth returns true if the sim-space can accomodate an agent
// moving south by `steps` number of cells, false otherwise.
MASS_FUNCTION bool canMoveSouth(int idx, int steps, int rowLength) {
    return idx + (steps * rowLength) < (rowLength * rowLength);
}

// canMoveWest returns true if the sim-space can accomodate an agent
// moving west by `steps` number of cells, false otherwise.
MASS_FUNCTION bool canMoveWest(int idx, int steps, int rowLength) {
    int col = idx % rowLength;
    return (col - steps) >= 0;
}

MASS_FUNCTION void SugarPlace::updateRandState() {
    myState->randState += (myState->randState+1) * (getIndex() + 42) * 1662169368;
}

// findMigrationDestination sets the migration destination for agents 
// residing on a sugar place. Neighbors are outlined as {N, E, S, W}
MASS_FUNCTION void SugarPlace::findMigrationDestination() {
    if (getAgentPopulation() == 0) return;
    
    myState->migrationDest = NULL;
    myState->migrationDestRelativeIdx -1;

    int idx = myState->index;
    int rowLength = myState->size[0];
    
    // Assumes a square simulation space...
    int validLocationIndices[N_DESTINATIONS];
    int numValidLocations = 0;

    SugarPlace* targetPlace;
    int neighborIdx = 0;

    // For each depth visibility level, check N/E/S/W positions.
    for (int i = 0; i < maxVisible; i++) {
        // Check North
        if (canMoveNorth(idx, i+1, rowLength)) {
            neighborIdx = i * 4;
            targetPlace = (SugarPlace*)myState->neighbors[neighborIdx];
            if (targetPlace->isGoodForMigration()) {
                validLocationIndices[numValidLocations] = neighborIdx;
                numValidLocations++;
            }
        }

        // Check East
        if (canMoveEast(idx, i+1, rowLength)) {
            neighborIdx = i * 4 + 1;
            targetPlace = (SugarPlace*)myState->neighbors[neighborIdx];
            if (targetPlace->isGoodForMigration()) {
                validLocationIndices[numValidLocations] = neighborIdx;
                numValidLocations++;
            }
        }

        // Check South
        if (canMoveSouth(idx, i+1, rowLength)) {
            neighborIdx = i * 4 + 2;
            targetPlace = (SugarPlace*)myState->neighbors[neighborIdx];
            if (targetPlace->isGoodForMigration()) {
                validLocationIndices[numValidLocations] = neighborIdx;
                numValidLocations++;
            }
        }

        // Check West
        if (canMoveWest(idx, i+1, rowLength)) {
            neighborIdx = i * 4 + 3;
            targetPlace = (SugarPlace*)myState->neighbors[neighborIdx];
            if (targetPlace->isGoodForMigration()) {
                validLocationIndices[numValidLocations] = neighborIdx;
                numValidLocations++;
            }
        }
    }

    // If there are no valid locations, simply return.
    if (numValidLocations == 0) { return; }

    // Start with how much sugar our current location has.
    int currentSugar = getCurSugar();
    int currentIdx = 0;
    int topNeighborSugar = currentSugar; 
    int topNeighborIdx = -1;

    // get random value to determine start index
    updateRandState();
    int randVal = myState->randState;
    currentIdx = randVal % numValidLocations;
    if (currentIdx < 0) { currentIdx = -currentIdx; }

    // Determine best visible neighbor to migrate to
    for (int i = 0; i < numValidLocations; i++) {
        currentIdx = (currentIdx + i) % numValidLocations;
        if (currentIdx < 0) { currentIdx = -currentIdx; }
        // currentIdx = (_time + i) % numValidLocations;
        targetPlace = (SugarPlace*) myState->neighbors[validLocationIndices[currentIdx]];
        currentSugar = targetPlace->getCurSugar();
        if (currentSugar >= topNeighborSugar) {
            topNeighborIdx = validLocationIndices[currentIdx];
            topNeighborSugar = currentSugar;
        }
    }

    // If there isn't a neighbor better than where we currently are,
    // then don't migrate.
    if (topNeighborIdx == -1) { return; }
    
    // Otherwise, migrate.
    myState->migrationDest = (SugarPlace*) myState->neighbors[topNeighborIdx];
    myState->migrationDestRelativeIdx = topNeighborIdx;
}

MASS_FUNCTION SugarPlace::~SugarPlace() {
	// nothing to delete
}

MASS_FUNCTION int SugarPlace::getCurSugar() {
	return myState->curSugar;
}

MASS_FUNCTION void SugarPlace::setCurSugar(int newSugar) {
    myState->curSugar = newSugar;
}

MASS_FUNCTION double SugarPlace::getPollution() {
    return myState -> pollution;
}

MASS_FUNCTION void SugarPlace::setPollution(double newPollution){
    myState -> pollution = newPollution;
}

MASS_FUNCTION bool SugarPlace::isGoodForMigration() {
    return (
        (getAgentPopulation() == 0) && // No agents currently occupying place.
        (myState->curSugar / ( 1.0 + myState->pollution) > 0.0) 
    );
}

MASS_FUNCTION SugarPlace* SugarPlace::getMigrationDest() {
    return myState -> migrationDest;
}

MASS_FUNCTION int SugarPlace::getMigrationDestRelIdx() {
    return myState -> migrationDestRelativeIdx;
}

MASS_FUNCTION void SugarPlace::initRand(int randVal) {
    myState->randState = randVal;
}

MASS_FUNCTION void SugarPlace::callMethod(int functionId, void *argument) {
	switch (functionId) {
		case SET_SUGAR:
			setSugar();
			break;
		case INC_SUGAR_AND_POLLUTION:
			incSugarAndPollution();
			break;
		case AVE_POLLUTIONS:
			avePollutions();
			break;
		case UPDATE_POLLUTION_WITH_AVERAGE:
			updatePollutionWithAverage();
			break;
        case FIND_MIGRATION_DESTINATION:
            findMigrationDestination();
            break;
        case INIT_RAND:
            initRand(((int*)argument)[getIndex()]);
            break;
		default:
			break;
	}
}
