#include <iostream>
#include <sstream>
#include <stdio.h>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>

// Boost APIs
#include <boost/program_options.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include <mass/Mass.h>
#include <mass/Logger.h>

#include "SugarScape.h"
#include "Timer.h"

namespace po = boost::program_options;
namespace logging = boost::log;

int main(int argc, char* argv[]) {
	// setup and parse program options
	po::options_description desc("options");
	desc.add_options()
		("help", "print help message")
		("verbose", po::bool_switch()->default_value(false), "set verbose output")
		("size", po::value<int>()->default_value(100), "size of simulation space")
		("seed", po::value<int>()->default_value(-1), "seed used for RNG. if left unset, current time is used")
		("max_time", po::value<int>()->default_value(100), "max simulation time steps")
		("interval", po::value<int>()->default_value(0), "output interval")
		("out_file", po::value<std::string>()->default_value("./sugarscape.viz"), "SimViz file output")
	;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		std::cout << desc << std::endl;

		return 0;
	}

	// Setup logger
	logging::trivial::severity_level log_level = logging::trivial::info;
	if (vm["verbose"].as<bool>()) {
		log_level = logging::trivial::debug;
	}

	mass::logger::setLogLevel(log_level);

	// Get simulation attributes
	int size = vm["size"].as<int>();
	int seed = vm["seed"].as<int>();
	int max_time = vm["max_time"].as<int>();
	int interval = vm["interval"].as<int>();

	// Evaluate seed
	if (seed == -1) {
		std::time_t _time = std::time(NULL);
		seed = _time;
	}

	mass::logger::info(
		"Running SugarScape with params: size=%d, seed=%d, max_time=%d, interval=%d",
		size, seed, max_time, interval
	);

	// Create viz file if interval is non-zero.
	simviz::RGBFile vizFile(size, size);
	if (interval > 0) {
		vizFile.open(vm["out_file"].as<std::string>().c_str());
	}

	// start timer
	Timer timer;
	timer.start();

	runMassSim(size, seed, max_time, interval, vizFile);

	mass::logger::info("Execution time %dus\n", timer.lap());

	vizFile.close();

	return 0;
}
