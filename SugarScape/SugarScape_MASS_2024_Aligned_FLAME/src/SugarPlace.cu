#include "SugarPlace.h"
#include "Ant.h"

#include "mass/settings.h"

#ifndef MAX_MT_SUGAR
#define MAX_MT_SUGAR 4
#endif

__device__ int SugarPlace::getInitialSugar(int idx, int size, int mtPeakX, int mtPeakY, int maxMtSugar)
{
	// Get the x and y coordinates of the place based on the index and the size
	int x = idx % size;
	int y = idx / size;

	// Calculate the distance from the mountain peak
	float distance = sqrtf((x - mtPeakX) * (x - mtPeakX) + (y - mtPeakY) * (y - mtPeakY));

	// Calculate the initial sugar level based on the distance from the mountain peak
	// We assume the radius of the mountain is half of the size of the grid
	float r = (float)size / 2.0;

	if (distance < r)
	{
		return (int)(maxMtSugar + 0.5 - maxMtSugar / r * distance);
	}
	else
	{
		return 0;
	}
}

__device__ void SugarPlace::setInitialSugar(int size)
{
	// Get the mountain peak coordinates
	int mtPeakX = size / 3;
	int mtPeakY = size - size / 3 - 1;

	// Get the sugar level based on the distance from the mountain peak
	int mt1 = getInitialSugar(getIndex(), size, mtPeakX, mtPeakY, MAX_MT_SUGAR);
	int mt2 = getInitialSugar(getIndex(), size, mtPeakY, mtPeakX, MAX_MT_SUGAR);

	// Set the current and max sugar of the place to the maximum of the two mountains
	int *currentSugar = getAttribute<int>(ATTRIBUTE::CURR_SUGAR, 1);
	int *maxSugar = getAttribute<int>(ATTRIBUTE::MAX_SUGAR, 1);
	*currentSugar = max(mt1, mt2);
	*maxSugar = max(mt1, mt2);
}

__device__ void SugarPlace::updateSugar()
{
	// Update the sugar level of the place
	int *currSugar = getAttribute<int>(ATTRIBUTE::CURR_SUGAR, 1);
	int *maxSugar = getAttribute<int>(ATTRIBUTE::MAX_SUGAR, 1);
	// We increase the sugar level of the place by 1 if it is less than the maximum sugar level
	if (*currSugar < *maxSugar)
	{
		(*currSugar)++;
	}
}

__device__ void SugarPlace::updateEnv()
{
	// Update the sugar level of the place
	updateSugar();
}

__device__ void SugarPlace::setCurandState(){
	// Get the curandState attribute
	curandState *state = getAttribute<curandState>(ATTRIBUTE::CURAND_STATE, 1);
	curand_init(getIndex(), getIndex(), 0, state);
}

__device__ void SugarPlace::findNextDest()
{
	// Get the neighbors of the place
	mass::Place **neighbors = getNeighborsPtr();

	// Get the sugar level of the place
	int *currSugar = getAttribute<int>(ATTRIBUTE::CURR_SUGAR, 1);

	// Initialize the next destination to the current place
	mass::Place **nextDest = getAttribute<mass::Place *>(ATTRIBUTE::NEXT_DEST, 1);
	int maxSugar = *currSugar;

	// Used to generate random float
	float maxRand = -1.0f;

	// Find the neighbor with the highest sugar level and the lowest pollution level
	for (int i = 0; i < MAX_NEIGHBORS; i++)
	{
		if (neighbors[i] != nullptr)
		{
			// Get the sugar level of the neighbor
			int sugar = *(neighbors[i]->getAttribute<int>(ATTRIBUTE::CURR_SUGAR, 1));

			// Aligned algorithm with FLAME GPU 2 SugarScape program:
			// The place is considered as the next destination if:
			// 1. If the sugar level of the neighbor is higher than the current maximum sugar level
			// 2. If the sugar level of the neighbor is equal to the current maximum sugar level
			//		and the random number is larger than the current random number

			// Generate a radom float
			curandState *state = getAttribute<curandState>(ATTRIBUTE::CURAND_STATE, 1);
			float currRand = curand_uniform(state);

			if (sugar > maxSugar || (sugar == maxSugar && currRand > maxRand))
			{
				// Update the next destination to the neighbor
				*nextDest = neighbors[i];
				maxSugar = sugar;
				maxRand = currRand;
			}
		}
	}
}

__device__ void SugarPlace::printNextDest()
{
	mass::Place **nextDest = getAttribute<mass::Place *>(ATTRIBUTE::NEXT_DEST, 1);

	if (*nextDest == nullptr)
	{
		// printf("%lu Next destination: nullptr\n", getIndex());
		return;
	}
	else
	{
		printf("%lu Next destination: %lu\n", getIndex(), (*nextDest)->getIndex());
	}
}

__device__ SugarPlace *SugarPlace::getNextDest()
{
	mass::Place **nextDest = getAttribute<mass::Place *>(ATTRIBUTE::NEXT_DEST, 1);

	return (SugarPlace *)(*nextDest);
}

__device__ void SugarPlace::callMethod(int functionId, void *argument)
{
	switch (functionId)
	{
	case SET_INITIAL_SUGAR:
		setInitialSugar(*(int *)argument);
		break;
	case UPDATE_SUGAR_AND_POLLUTION:
		updateSugar();
		break;
	case UPDATE_ENVIRONMENT:
		updateEnv();
		break;
	case SET_CURAND_STATE:
		setCurandState();
		break;
	case FIND_NEXT_DEST:
		findNextDest();
		break;
	case PRINT_NEXT_DEST:
		printNextDest();
		break;
	case GET_NEXT_DEST:
		getNextDest();
		break;
	default:
		break;
	}
}