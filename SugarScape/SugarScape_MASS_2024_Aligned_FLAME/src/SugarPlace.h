#pragma once

#include "mass/Place.h"
#include "mass/Logger.h"
#include <curand_kernel.h>

class SugarPlace : public mass::Place
{
public:
    MASS_FUNCTION SugarPlace(int index) : mass::Place(index) {}
    MASS_FUNCTION ~SugarPlace() {}
    // callMethod to override the default behavior of the Place
    __device__ virtual void callMethod(int functionId, void *arg = NULL);

    // Attributes
    enum ATTRIBUTE
    {
        CURAND_STATE,
        CURR_SUGAR,
        MAX_SUGAR,
        RAND_STATE,
        NEXT_DEST,
    };

    // Methods
    enum FUNCTION
    {
        SET_CURAND_STATE,
        SET_INITIAL_SUGAR,
        UPDATE_SUGAR_AND_POLLUTION,
        CALCULATE_AVG_POLLUTION,
        SET_POLLUTION_BY_AVG,
        UPDATE_ENVIRONMENT,
        FIND_NEXT_DEST,
        PRINT_NEXT_DEST,
        GET_NEXT_DEST,
    };

private:
    /**
     * @brief Calculate the initial sugar of the place
     *
     * @param idx the index of the place to calculate the sugar
     * @param size the size of the place (if the places is 1000x1000, size = 1000)
     * @param mtPeakX the x coordinate of the mountain peak
     * @param mtPeakY the y coordinate of the mountain peak
     * @param maxMtSugar the maximum sugar of the mountain
     *
     * @return int the amount of sugar
     */
    __device__ int getInitialSugar(int idx, int size, int mtPeakX, int mtPeakY, int maxMtSugar);

    /**
     * @brief Set the initial sugar of the place
     *
     * @param size the size of the place (if the places is 1000x1000, size = 1000)
     *
     * @return void
     */
    __device__ void setInitialSugar(int size);

    /**
     * @brief Increase the sugar and pollution of the place.
     * If the current sugar is smaller than the maximum sugar, increase the sugar by 1.
     * If there is an ant in the place, increase the pollution by 1.
     *
     * @return void
     */
    __device__ void updateSugar();

    /**
     * @brief Update the environment of the place
     * by calling `updateSugar`, `calculateAvgPollution`,
     * and `setPollutionByAvg`. So that all functions
     * can be called in one kernel call, which is more efficient
     * than running three separate kernel calls.
     *
     * @return void
     */
    __device__ void updateEnv();

    /**
     * Set the curandState for the place for later use
     *
     * @return void
     */
    __device__ void setCurandState();

    /**
     * @brief Find the next destination for the ant in the place
     * based on the vision of the ant. The destination will be stored
     * in the NEXT_DEST attribute.
     *
     * @return void
     */
    __device__ void findNextDest();

    /**
     * @brief Print the next destination of the ant in the place
     *
     * @return void
     */
    __device__ void printNextDest();

    /**
     * @brief Get the next destination of the ant in the place
     *
     * @param seed the seed for random number generation
     *
     * @return SugarPlace* the next destination of the ant
     */
    __device__ SugarPlace *getNextDest();
};