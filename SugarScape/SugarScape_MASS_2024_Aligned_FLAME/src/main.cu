#include <iostream>
#include <boost/program_options.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include "SugarScape.h"

namespace po = boost::program_options;
namespace logging = boost::log;

int main(int argc, char *argv[])
{
	// setup and parse program options
	po::options_description desc("options");
	desc.add_options()
	("help", "print help message")
	("timer", po::value<int>()->default_value(0), "enable timer: 0 - disable, 1 - enable basic timer, 2 - enable detailed timer")
	("verbose", po::bool_switch()->default_value(false), "set verbose output")
	("size", po::value<int>()->default_value(100), "size of simulation space")
	("seed", po::value<int>()->default_value(-1), "seed used for RNG. if left unset, current time is used")
	("step", po::value<int>()->default_value(100), "max simulation time steps")
	("interval", po::value<int>()->default_value(0), "output interval")
	("out_file", po::value<std::string>()->default_value("./sugarscape.viz"), "SimViz file output");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help"))
	{
		std::cout << desc << std::endl;

		return 0;
	}

	// Setup logger
	logging::trivial::severity_level log_level = logging::trivial::info;
	if (vm["verbose"].as<bool>())
	{
		log_level = logging::trivial::debug;
	}

	mass::logger::setLogLevel(log_level);

	// Get simulation attributes
	int size = vm["size"].as<int>();
	int seed = vm["seed"].as<int>();
	int step = vm["step"].as<int>();
	int interval = vm["interval"].as<int>();
	int useTimer = vm["timer"].as<int>();

	// Create viz file if interval is non-zero
	simviz::RGBFile file(size, size);
	if (interval > 0)
	{
		file.open(vm["out_file"].as<std::string>().c_str());
	}

	// Run simulation
	mass::logger::info("Running SugarScape simulation with size: %d, seed: %d, step: %d, interval: %d", size, seed, step, interval);

	runMassSim(size, seed, step, interval, file, useTimer);
	file.close();

	mass::logger::info("Simulation complete");
	return 0;
}
