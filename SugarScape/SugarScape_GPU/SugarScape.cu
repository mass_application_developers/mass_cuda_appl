
#include <ctime> // clock_t
#include <stdio.h> 
#include <iostream>
#include <math.h>     // floor
#include <sstream>     // ostringstream
#include <vector>
#include <cuda_runtime.h>
#include <curand.h>  // for CUDA's random number stuff
#include <curand_kernel.h>  // for CUDA's random number stuff

#include "Timer.h"

#define CERR
#define CATCH(err) __cudaCatch( err, __FILE__, __LINE__ )
#define CHECK() __cudaCheckError( __FILE__, __LINE__ )

static const int WORK_SIZE = 32; //num of threads per block
static const int HALO_SPACE = 1; //number of cells on each side required for calculaions
static const int maxVisible = 3;
static const int maxMetabolism = 4;
static const int maxInitAgentSugar = 10;
static const bool show = false; //print progress from gpu

using namespace std;

__device__ int initSugarAmount(int idx, int size, int mtPeakX, int mtPeakY, int maxMtSug);


// Utility functions:
__device__ int getGlobalIdx_1D_1D() {
    return blockIdx.x * blockDim.x + threadIdx.x;
}

void __cudaCatch(cudaError err, const char *file, const int line) {
#ifdef CERR
    if (cudaSuccess != err) {
        cerr << "MASS Cuda Util: " << cudaGetErrorString(err) << " in " << file << " at line " << line << endl;
        exit (EXIT_FAILURE);
    }
#endif
}

void __cudaCheckError(const char *file, const int line) {
#ifdef CERR
    __cudaCatch(cudaGetLastError(), file, line);
#endif
}

//Actual simulation functions:

// Sets initial amount of sugar for the cell
__global__ void setSugar(int *curSugar, int *maxSugar, int *nextAgentIdx, int *destinationIdx,
        double *pollution, double *avePollution, int size, int maxMtSug)
{
    int idx = getGlobalIdx_1D_1D();

    pollution[idx] = 0.0;    // the current pollution
    avePollution[idx] = 0.0; // averaging four neighbors' pollution
    destinationIdx[idx] = -1; // the next place to migrate to
    nextAgentIdx[idx] = -1;      // the next agent to come here
    int mtCoord[2];
    mtCoord[0] = size/3;
    mtCoord[1] = size - size/3 - 1;
    
    int mt1 = initSugarAmount(idx, size, mtCoord[0], mtCoord[1], maxMtSug);
    int mt2 = initSugarAmount(idx, size, mtCoord[1], mtCoord[0], maxMtSug);
    
    curSugar[idx] = mt1 > mt2 ? mt1 : mt2;
    maxSugar[idx] = mt1 > mt2 ? mt1 : mt2;;
}

__device__ int initSugarAmount(int idx, int size, int mtPeakX, int mtPeakY, int maxMtSug) {
    int x_coord = idx % size;
    int y_coord = idx / size;
    
    double distance = sqrt((float)(( mtPeakX - x_coord ) * ( mtPeakX - x_coord ) + (mtPeakY - y_coord) * (mtPeakY - y_coord)));

    // radius is assumed to be simSize/2.
    int r = size/2;
    if ( distance < r )
    {
        // '+ 0.5' for rounding a value.
        return ( int )( maxMtSug + 0.5 - maxMtSug / ( double )r * distance );
    }
    else
        return 0;
}

void initAgents(dim3 gridDim, dim3 threadDim, int *nAgentsInPlace, int nAgents, int size, int *agentSugar, int *agentMetabolism)
{
    // randomly allocate agents on CPU:
    int *nAgentsInPlace_host = new int[size*size];
    int *agentSugar_host = new int[size*size];
    int *agentMetabolism_host = new int[size*size];

    srand (239); //seed
    int ratio = 100 * nAgents / (size*size);  //percentage of non-empty cells

    for (int i =0; i < size*size; i ++) {
        unsigned int randNumber = rand() % 100; //random number from 0 to 100
        if (randNumber <= ratio) {
            nAgentsInPlace_host[i] = 1;
            agentSugar_host[i] = rand() % maxInitAgentSugar +1;
            agentMetabolism_host [i] = rand() % maxMetabolism +1;
        }
        else {
            nAgentsInPlace_host[i] = 0;
            agentSugar_host[i] = -1;
            agentMetabolism_host [i] = -1;
        }
    }

    //Copy the matrix to the host:
    cudaError_t cudaStat = cudaMemcpy(nAgentsInPlace, nAgentsInPlace_host, size*size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStat != cudaSuccess) {
        cerr << "cudaMemcpy failed!" << endl;
        exit (EXIT_FAILURE);
    }
    cudaStat = cudaMemcpy(agentSugar, agentSugar_host, size*size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStat != cudaSuccess) {
        cerr << "cudaMemcpy failed!" << endl;
        exit (EXIT_FAILURE);
    }
    cudaStat = cudaMemcpy(agentMetabolism, agentMetabolism_host, size*size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStat != cudaSuccess) {
        cerr << "cudaMemcpy failed!" << endl;
        exit (EXIT_FAILURE);
    }

    delete nAgentsInPlace_host;
    delete agentSugar_host;
    delete agentMetabolism_host;
}

__global__ void incSugarAndPollution(int *curSugar, int *maxSugar, double *pollution) {
    int idx = getGlobalIdx_1D_1D();

    if ( curSugar[idx] < maxSugar[idx] )
    {
        curSugar[idx]++;
    }
    pollution[idx] += 1.0;
}

// Calculates average pollution between 4 neighbors(top, bottom, left, right)
__global__ void avePollutions(double *pollution, double *avePollution, int size) {
    
    int idx = getGlobalIdx_1D_1D();
    if (idx < size*size) {

        // Using stencil pattern to optimize for the use of shared memory.
        // At this point doing as 1D model and cache left and right halo, but not top and bottom.
        __shared__ double pollution_cached[WORK_SIZE+2*HALO_SPACE];
        //printf("pollution_cached[%d] = pollution[%d];\n", HALO_SPACE+idx%WORK_SIZE, idx);
        pollution_cached[HALO_SPACE+idx%WORK_SIZE] = pollution[idx];

        for (int i=0; i<HALO_SPACE; i++) {
            int index_left = idx/WORK_SIZE * WORK_SIZE -HALO_SPACE +i;
            if (index_left >= 0) {
                //printf("Thread idx = %d, left halo index to be cached= %d\n", idx, index_left);
                pollution_cached[i] = pollution[index_left];
            }
            int index_right = idx/WORK_SIZE * WORK_SIZE + WORK_SIZE +i;
            if (index_right < size*size) {
                //printf("Thread idx = %d, right halo index to be cached= %d, cell to be assigned to = %d\n", idx, index_right, HALO_SPACE + WORK_SIZE + i);
                pollution_cached[HALO_SPACE + WORK_SIZE + i] = pollution[index_right];
            }
            
        }
         __syncthreads();
        //printf("Past __syncthreads for thread %d\n", idx);

        double top, bottom, right, left;
        idx + size < size*size ? top = pollution[idx + size] : 0.0;
        idx - size >= 0 ? bottom = pollution[idx-size] : 0.0;

        //printf("right pollution_cached[%d]\n", idx%WORK_SIZE + 1);
        (idx +1) % size != 0 ? right = pollution_cached[HALO_SPACE + idx%WORK_SIZE + 1] : 0.0; //from shared memory

        //printf("left pollution_cached[%d]\n", idx%WORK_SIZE -1);
        (idx - 1) % size != size-2 ? left = pollution_cached[HALO_SPACE + idx%WORK_SIZE -1] : 0.0; //from shared memory
       
        avePollution[idx] = ( top + bottom + left + right ) / 4.0;
    }
}

__global__ void updatePollutionWithAverage(double *pollution, double *avePollution) {
    int idx = getGlobalIdx_1D_1D();
    pollution[idx] = avePollution[idx];
    avePollution[idx] = 0.0;
}

__global__ void findPlaceForMigration(int *nAgentsInPlace, int *curSugar, double *pollution, 
    int maxVisible, int size, int *destinationIdx)
{
    int idx = getGlobalIdx_1D_1D();

    if (nAgentsInPlace[idx] > 0) {
        // consider visibility along x axis:
        for (int i = 1; i<=maxVisible; i++) {
            int destIdx = idx+i;
            if ((destIdx < size*size) && nAgentsInPlace[destIdx] == 0 &&
                (curSugar[destIdx] / ( 1.0 + pollution[destIdx]) > 0.0)) {

                destinationIdx[idx] = destIdx;
                return;
            }
        }

        // consider visibility along y axis:
        for (int i = 1; i<=maxVisible; i++) {
            int destIdx = idx+i*size;
            if ((destIdx < size*size) && nAgentsInPlace[destIdx] == 0 &&
                (curSugar[destIdx] / ( 1.0 + pollution[destIdx]) > 0.0)) {

                destinationIdx[idx] = destIdx;
                return;
            }
        }
    }
}

__global__ void selectAgentToAccept(int *destinationIdx, int *nextAgentIdx) {
    int idx = getGlobalIdx_1D_1D();
    int destIdx = destinationIdx[idx];

    atomicCAS(nextAgentIdx + destIdx, -1, idx); //atomic operation to assign agent to a free place
    atomicMin(nextAgentIdx + destIdx, idx); //atomic operation to replace the agent in a place if it's idx is smaller than existing
}

__global__ void migrate(int *nAgentsInPlace, int *destinationIdx, int *nextAgentIdx, int *agentSugar, int *agentMetabolism) {
    int idx = getGlobalIdx_1D_1D();
    if (nextAgentIdx[destinationIdx[idx]] == idx) {
        nAgentsInPlace[destinationIdx[idx]] = 1;
        nAgentsInPlace[idx] = 0;
        
        // move whatever data agents have:
        agentSugar[destinationIdx[idx]] = agentSugar[idx];
        agentSugar[idx] = -1;

        agentMetabolism[destinationIdx[idx]] = agentMetabolism[idx];
        agentMetabolism[idx] = -1;
    }
}

__global__ void resetMigrationData (int *destinationIdx, int *nextAgentIdx) {
    int idx = getGlobalIdx_1D_1D();
    destinationIdx[idx] = -1; // the next place to migrate to
    nextAgentIdx[idx] = -1;      // the next agent to come here
}

__global__ void metabolize(int *nAgentsInPlace, int *agentSugar, int *agentMetabolism, int *curSugar, double *pollution) {
    int idx = getGlobalIdx_1D_1D();
    if (nAgentsInPlace[idx] > 0) {
        agentSugar[idx] += curSugar[idx];
        agentSugar[idx] -= agentMetabolism[idx];

        curSugar[idx] = 0;
        pollution[idx] += agentMetabolism[idx];

        if( agentSugar[idx] < 0 )
        {
            // Kill agent:
            nAgentsInPlace[idx] = 0;
            agentSugar[idx] = -1;
            agentMetabolism[idx] = -1;
        }
    }

    
}

void displayResults(int *results_matrix, int size) {
    int *results_matrix_h = new int[size*size];

    //Copy the matrix to the host:
    CATCH(cudaMemcpy(results_matrix_h, results_matrix, size*size * sizeof(int), cudaMemcpyDeviceToHost));

    for (int i=0; i<size; i++) {
        for (int j=0; j<size; j++) {
            cout << results_matrix_h[i*size+j] << "\t";
        }
        cout << endl;
    }
    cout << endl;

    delete results_matrix_h;
}

void runDeviceSim(int size, int max_time, int nAgents) {
    cudaError_t cudaStat;

    int device_Count;
    cudaGetDeviceCount(&device_Count);
    cout << "Device Numbers = " << device_Count << endl;

    cudaStat = cudaSetDevice(0);
    if (cudaStat != cudaSuccess) {
        cerr << "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?" << cudaStat << endl;
        exit (EXIT_FAILURE);
    }

    int maxMtSugar = 4; //max level of sugar in mountain peak

    // Create a space:
    int *curSugar, *maxSugar, *nAgentsInPlace, *nextAgentIdx, *destinationIdx, *agentSugar, *agentMetabolism;
    double *pollution, *avePollution;

    int nBytesInt = sizeof(int) * size * size;
    int nBytesDbl = sizeof(double) * size * size;

    // Allocate memory on the device:
    CATCH(cudaMalloc((void** ) &curSugar, nBytesInt));
    CATCH(cudaMalloc((void** ) &maxSugar, nBytesInt));
    CATCH(cudaMalloc((void** ) &nAgentsInPlace, nBytesInt));
    CATCH(cudaMalloc((void** ) &nextAgentIdx, nBytesInt));
    CATCH(cudaMalloc((void** ) &destinationIdx, nBytesInt));
    CATCH(cudaMalloc((void** ) &agentSugar, nBytesInt));
    CATCH(cudaMalloc((void** ) &agentMetabolism, nBytesInt));
    CATCH(cudaMalloc((void** ) &pollution, nBytesDbl));
    CATCH(cudaMalloc((void** ) &avePollution, nBytesDbl));
    

    int gridWidth = (size * size - 1) / WORK_SIZE + 1;
    int threadWidth = (size * size - 1) / gridWidth + 1;

    dim3 gridDim(gridWidth);
    dim3 threadDim(threadWidth);

    setSugar<<<gridDim, threadDim>>>(curSugar, maxSugar, nextAgentIdx, destinationIdx,
        pollution, avePollution, size, maxMtSugar);
    CHECK();

    initAgents(gridDim, threadDim, nAgentsInPlace, nAgents, size, agentSugar, agentMetabolism);
    CHECK();

    // Start a timer
    Timer time;
    time.start();

    if (show) {
        cout << "INITIAL SUGAR AND ANT DISTRIBUTION" << endl;
        cout << "SUGAR LEVELS:" << endl;
        displayResults(curSugar, size);
        cout << "ANT DISTRIBUTION:" << endl;
        displayResults(nAgentsInPlace, size);
        cout << "AGENT STORED SUGAR:" << endl;
        displayResults(agentSugar, size);
        cout << "AGENT METABOLISM:" << endl;
        displayResults(agentMetabolism, size);
    }

    // Simulate ant behavior:
    for (int t=0; t < max_time; t++) {
        incSugarAndPollution<<<gridDim, threadDim>>>(curSugar, maxSugar, pollution);
        CHECK();

        // No need to syncronize here, because the next kernel execution is put in a queue after the previous one
        avePollutions<<<gridDim, threadDim>>>(pollution, avePollution, size);
        updatePollutionWithAverage<<<gridDim, threadDim>>>(pollution, avePollution);
        CHECK();

        findPlaceForMigration<<<gridDim, threadDim>>>(nAgentsInPlace, curSugar, pollution, maxVisible, size, destinationIdx);
        selectAgentToAccept<<<gridDim, threadDim>>>(destinationIdx, nextAgentIdx);
        migrate<<<gridDim, threadDim>>>(nAgentsInPlace, destinationIdx, nextAgentIdx, agentSugar, agentMetabolism);
        resetMigrationData<<<gridDim, threadDim>>>(destinationIdx, nextAgentIdx);
        metabolize<<<gridDim, threadDim>>>(nAgentsInPlace, agentSugar, agentMetabolism, curSugar, pollution);
        CHECK();

        if (show) {
            cout << "Time: t=" << t << endl;
            cout << "End of step sugar and ant distribution: " << endl;
            cout << "SUGAR LEVELS:" << endl;
            displayResults(curSugar, size);
            cout << "ANT DISTRIBUTION:" << endl;
            displayResults(nAgentsInPlace, size);
            cout << "AGENT STORED SUGAR:" << endl;
            displayResults(agentSugar, size);
        }
    }
    cudaDeviceSynchronize(); // this synchronization call is not nesessary, if we delete
                             // it the synchronization will take place as part of cudaFree()
                             // call (time remains the same) 

    //Deallocate memory:
    CATCH(cudaFree(curSugar));
    CATCH(cudaFree(maxSugar));
    CATCH(cudaFree(nAgentsInPlace));
    CATCH(cudaFree(nextAgentIdx));
    CATCH(cudaFree(destinationIdx));
    CATCH(cudaFree(agentSugar));
    CATCH(cudaFree(agentMetabolism));
    CATCH(cudaFree(pollution));
    CATCH(cudaFree(avePollution));

    cout << "Elapsed time on GPU = " << time.lap() << endl;
}

int main(int argc, char const *argv[])
{
    cout << "Starting the GPU simulation" << endl;
    int size[] = { 5, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };
    int max_time = 100;
    for (int i=0; i< (sizeof(size)/sizeof(size[0])) ; i++) {
        int nAgents = size[i]*size[i] / 5;
        runDeviceSim(size[i], max_time, nAgents);
    }

    cout << "End of the GPU simulation" << endl;
    return 0;
}
