# SugarScape

Build the environment with the following command: 
```bash
make develop
```

Build the project with the following command:
```bash
make build
```

Run the project with the following command:
```bash
./bin/SugarScape
```

To view the help message, run the following command:
```bash
./bin/SugarScape --help
```

## Visualization
To visualize the simulation, upon build the environment, run the following command to install the visualization tool:
```bash
make build-simviz
```

Specifying an output interval, by using the `--interval` flag will tell the program to output the simulation to a `.viz` file every `<interval>` steps. This simulation file can be played using the MASS SimViz application for a graphical visualization of the simulation space.

## Visualization Examples
1. Size: 100 x 100, steps: 100, interval: 1

![Sugarscape_100_100_1](./img/100_100_1.gif)

2. Size: 1000 x 1000, steps: 100, interval: 1

![Sugarscape_1000_100_1](./img/1000_100_1.gif)

3. Size: 3000 x 3000, steps: 100, interval: 1

![Sugarscape_3000_100_1](./img/3000_100_1.gif)
