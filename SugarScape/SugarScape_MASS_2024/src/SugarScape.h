#pragma once

#include <iostream>
#include <numeric>
#include <random>
#include <vector>

#include "mass/Mass.h"
#include "mass/Places.h"
#include "mass/Agents.h"
#include "mass/Logger.h"
#include "mass/CudaEventTimer.h"
#include "simviz.h"

#include "SugarPlace.h"
#include "Ant.h"
#include "Timer.h"

/**
 * @brief Display the sugar of the places
 * (save the immediate sugarPlace result to a simviz file)
 *
 * @param file the simviz file to save the result
 * @param sp the pointer to the sugarPlace instance
 * @param size the size of the grid
 *
 * @return void
 */
void displaySugar(simviz::RGBFile &file, mass::Places *sp, int size)
{
    /**
     * Configuration for colors in the simviz file.
     */
    unsigned char *COLOR_PLACE = new unsigned char[3]{192, 192, 192};           // Gray
    unsigned char *COLOR_SUGAR_LEVEL_LOW = new unsigned char[3]{255, 255, 255}; // White
    unsigned char *COLOR_SUGAR_LEVEL_MID = new unsigned char[3]{255, 255, 153}; // Light Yellow
    unsigned char *COLOR_SUGAR_LEVEL_HIGH = new unsigned char[3]{204, 204, 0};  // Dark Yellow
    unsigned char *COLOR_SUGAR_LEVEL_FULL = new unsigned char[3]{0, 255, 0};    // Green
    unsigned char *COLOR_AGENT = new unsigned char[3]{0, 0, 0};                 // Black

    // Download the CURR_SUGAR attribute
    int *currSugar = sp->downloadAttributes<int>(SugarPlace::CURR_SUGAR, 1);
    // Download the AGENT_POPS attribute
    size_t *ap = sp->downloadAttributes<size_t>(mass::PlacePreDefinedAttr::AGENT_POPS, 1);

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            int sugar = currSugar[i * size + j];
            int agentPop = ap[i * size + j];

            if (agentPop > 0)
            {
                file.write((char *)COLOR_AGENT, simviz::NumRGBBytes);
            }
            else
            {
                if (sugar > 3)
                {
                    file.write((char *)COLOR_SUGAR_LEVEL_FULL, simviz::NumRGBBytes);
                }
                else if (sugar > 2)
                {
                    file.write((char *)COLOR_SUGAR_LEVEL_HIGH, simviz::NumRGBBytes);
                }
                else if (sugar > 1)
                {
                    file.write((char *)COLOR_SUGAR_LEVEL_MID, simviz::NumRGBBytes);
                }
                else if (sugar > 0)
                {
                    file.write((char *)COLOR_SUGAR_LEVEL_LOW, simviz::NumRGBBytes);
                }
                else
                {
                    file.write((char *)COLOR_PLACE, simviz::NumRGBBytes);
                }
            }
        }
    }
}

/**
 * @brief Run the SugarScape simulation using MASS
 *
 * @param size the size of the grid
 * @param seed the seed for random number generation
 * @param maxStep the maximum number of steps to run the simulation
 * @param interval the interval to display the simulation (to save the simulation result to simviz file)
 *
 * @return void
 */
void runMassSim(int size, int seed, int steps, int interval, simviz::RGBFile &file, bool useTimer = false)
{
    mass::logger::debug("Starting SugarScape simulation");
    mass::logger::debug("MASS Configuration: MAX_AGENTS=%d, MAX_NEIGHBORS=%d, MAX_DIMS=%d, N_DESTINATIONS=%d", MAX_AGENTS, MAX_NEIGHBORS, MAX_DIMS, N_DESTINATIONS);
    mass::logger::debug("Simulation Configuration: size=%d, seed=%d, steps=%d, interval=%d", size, seed, steps, interval);

    std::srand(seed);
    mass::logger::debug("Seed: %d", seed);

    /**
     * Before initializing MASS, we generate all values needed for the simulation
     * so that we do not count the number generation time in the simulation time
     */

    // Generate the neighbor for places
    std::vector<int *> neighbors;
    int top[2] = {0, -1};
    int bottom[2] = {0, 1};
    int left[2] = {-1, 0};
    int right[2] = {1, 0};
    neighbors.push_back(top);
    neighbors.push_back(bottom);
    neighbors.push_back(left);
    neighbors.push_back(right);

    // Create potential migration destination for ants
    if (MAX_VISION * 4 > MAX_NEIGHBORS)
    {
        mass::logger::error("MAX_VISION * 4 should be less than or equal to MAX_NEIGHBORS");
        mass::logger::error("Current configuration: MAX_VISION: %d, MAX_NEIGHBORS: %d", MAX_VISION, MAX_NEIGHBORS);
        mass::logger::error("An ant can move to 4 directions (top, bottom, left, right) with a vision of MAX_VISION");
        mass::logger::error("And the MAX_NEIGHBORS is the maximum number of destinations an agent can move to");
        mass::logger::error("Therefore, MAX_VISION * 4 should be less than or equal to MAX_NEIGHBORS");
        exit(1);
    }
    std::vector<int *> migrationDestinations;
    for (int i = 1; i < MAX_VISION + 1; i++)
    {
        migrationDestinations.push_back(new int[2]{0, -i});
        migrationDestinations.push_back(new int[2]{0, i});
        migrationDestinations.push_back(new int[2]{-i, 0});
        migrationDestinations.push_back(new int[2]{i, 0});
    }

    // We current create num of places / 5 ants
    int numAgents = size * size / 5;
    // Create an array of indexes to place ants randomly on the grid
    std::vector<int> indexes(size * size);
    std::iota(indexes.begin(), indexes.end(), 0);
    std::random_shuffle(indexes.begin(), indexes.end());
    int *antPos = (int *)malloc(numAgents * sizeof(int));
    for (int i = 0; i < numAgents; i++)
    {
        antPos[i] = indexes[i];
    }

    // Create two arrays to hold the random values to pass to the agents
    int *initSugar = (int *)malloc(numAgents * sizeof(int));
    int *initMetabolism = (int *)malloc(numAgents * sizeof(int));
    for (int i = 0; i < numAgents; i++)
    {
        initSugar[i] = std::rand() % MAX_INIT_ANT_SUGAR + 1;
        initMetabolism[i] = std::rand() % MAX_METABOLISM + 1;
    }

    /**
     * End of number generation
     */

    // Start a timer to measure the overall simulation time
    Timer timer;
    if (useTimer)
    {
        timer.start();
    }

    // Timer for initialization CUDA time
    std::unique_ptr<mass::CudaEventTimer> initTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
    if (useTimer)
    {
        initTimer->startTimer();
    }

    mass::Mass::init();

        // Create places (sugar places)
    int nDims = 2;
    int placeSize[] = {size, size};
    mass::Places *sp = mass::Mass::createPlaces<SugarPlace>(0, nDims, placeSize, mass::Place::MemoryOrder::ROW_MAJOR);

    // Create place attributes
    sp->setAttribute<float>(SugarPlace::POLLUTION, 1, 0.0);
    sp->setAttribute<float>(SugarPlace::AVG_POLLUTION, 1, 0.0);
    sp->setAttribute<int>(SugarPlace::CURR_SUGAR, 1, 0);
    sp->setAttribute<int>(SugarPlace::MAX_SUGAR, 1, 0);
    sp->setAttribute<int>(SugarPlace::RAND_STATE, 1, 0);
    sp->setAttribute<mass::Place *>(SugarPlace::NEXT_DEST, 1, nullptr);
    sp->finalizeAttributes();

    // Set initial sugar
    sp->callAll(SugarPlace::SET_INITIAL_SUGAR, &size, sizeof(int));

    // Create agents (ants)
    mass::Agents *ants = mass::Mass::createAgents<Ant>(0, numAgents, sp, 0, antPos);

    // Create agent attributes
    ants->setAttribute<int>(Ant::SUGAR, 1, 0);
    ants->setAttribute<int>(Ant::METABOLISM, 1, 0);
    ants->setAttribute<int>(Ant::DESTINATION, 1, 0);
    ants->finalizeAttributes();

    // Set initial sugar and metabolism
    // Use callAll to set the initial sugar and metabolism
    ants->callAll(Ant::SET_INIT_SUGAR, initSugar, sizeof(int) * numAgents);
    ants->callAll(Ant::SET_INIT_METABOLISM, initMetabolism, sizeof(int) * numAgents);

    // Calculate the time for initialization
    if (useTimer)
    {
        // CPU timer
        long elapsedTime = timer.lap();
        mass::logger::info("Initialization time: %ld ms", elapsedTime / 1000);

        // CUDA timer
        initTimer->stopTimer();
    }

    // Start the simulation
    float avgStepTime = 0.0;
    for (int step = 0; step < steps; step++)
    {
        // Timer for per-step CUDA execution time
        std::unique_ptr<mass::CudaEventTimer> stepTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
        if (useTimer)
        {
            stepTimer->startTimer();
        }

        // Update the places environment based on the normal 4 directions neighbors
        sp->exchangeAll(&neighbors, SugarPlace::UPDATE_ENVIRONMENT);

        // Find the next destination for ant in each place based on the vision of the ant
        // Currently, the ant can see 3 places in each direction (top, bottom, left, right)
        // total 12 places
        // Shuffle the migrationDestinations to randomize the direction
        std::random_shuffle(migrationDestinations.begin(), migrationDestinations.end());
        sp->exchangeAll(&migrationDestinations, SugarPlace::FIND_NEXT_DEST);
        // sp->callAll(SugarPlace::PRINT_NEXT_DEST);

        // Move the ants to the next destination
        ants->callAll(Ant::MOVE);
        ants->manageAll();

        // Perform the metabolism of the ants
        ants->callAll(Ant::METABOLIZE);

        // Save result to simviz file if interval is set
        if (interval > 0 && step % interval == 0 || step == steps - 1)
        {
            displaySugar(file, sp, size);
        }

        // Calculate the time for the current step
        if (useTimer)
        {
            stepTimer->stopTimer();
            float elapsedTime = stepTimer->getElapsedTime();
            avgStepTime += elapsedTime;
            mass::logger::info("Step %d execution time: %f ms", step, elapsedTime);
        }

        mass::logger::debug("Step %d finished", step);
    }

    // Calculate the time
    if (useTimer)
    {
        // Print CUDA initialization time
        float initTime = initTimer->getElapsedTime();
        mass::logger::info("CUDA initialization time: %f ms", initTime);

        // Print average step time
        avgStepTime /= steps;
        mass::logger::info("CUDA average step execution time: %f ms", avgStepTime);

        // Overall simulation time
        long elapsedTime = timer.lap() / 1000;
        mass::logger::info("Overall simulation time: %ld ms", elapsedTime);
    }

    // Finish MASS
    mass::Mass::finish();

    // Free the allocated memory
    free(antPos);
    free(initSugar);
    free(initMetabolism);
    for (int i = 0; i < migrationDestinations.size(); i++)
    {
        delete[] migrationDestinations[i];
    }

    mass::logger::debug("Finished SugarScape simulation");
}
