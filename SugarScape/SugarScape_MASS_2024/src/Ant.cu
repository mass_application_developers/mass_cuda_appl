#include "Ant.h"

__device__ void Ant::setInitSugar(int *initSugar)
{
    // Set the initial sugar level of the ant
    int *currentSugar = getAttribute<int>(ATTRIBUTE::SUGAR, 1);
    *currentSugar = initSugar[getIndex()];
}

__device__ void Ant::setInitMetabolism(int *initMetabolism)
{
    // Set the initial metabolism of the ant
    int *metabolism = getAttribute<int>(ATTRIBUTE::METABOLISM, 1);
    *metabolism = initMetabolism[getIndex()];
}

__device__ void Ant::move()
{
    // Get the current residing place of the ant
    mass::Place *currentPlace = *getAttribute<mass::Place *>(mass::AgentPreDefinedAttr::RESIDE_PLACE, 1);

    // Get the next destination of the ant computed by the place
    mass::Place *nextDest = *currentPlace->getAttribute<mass::Place *>(SugarPlace::ATTRIBUTE::NEXT_DEST, 1);

    // Move the ant to the next destination
    if (nextDest)
    {
        migrate(nextDest);
    }
}

__device__ void Ant::metabolize()
{
    // Get the sugar level of the ant
    int *sugar = getAttribute<int>(ATTRIBUTE::SUGAR, 1);

    // Get the place where the ant resides
    mass::Place *place = *getAttribute<mass::Place *>(mass::AgentPreDefinedAttr::RESIDE_PLACE, 1);
    // Get the sugar level of the place
    int *placeSugar = place->getAttribute<int>(SugarPlace::ATTRIBUTE::CURR_SUGAR, 1);

    // Collect sugar from the place
    *sugar += *placeSugar;
    // Minus the sugar level of the place
    *placeSugar = 0;

    // Get the metabolism of the ant
    int *metabolism = getAttribute<int>(ATTRIBUTE::METABOLISM, 1);
    // Metabolize the sugar level of the ant
    *sugar -= *metabolism;
    
    // Get the pollution level of the place
    float *pollution = place->getAttribute<float>(SugarPlace::ATTRIBUTE::POLLUTION, 1);
    // Increase the pollution level of the place by the metabolism of the ant
    *pollution += *metabolism;

    // If the sugar level of the ant is less than or equal to 0
    // then the ant dies
    if (*sugar <= 0)
    {
        terminate(); // Actually, Agents::manageAll() is not necessary after calling this function
                     // because terminate() is a single step function, see details in
                     // the whitepaper
    }
}

__device__ void Ant::callMethod(int functionId, void *argument)
{
    switch (functionId)
    {
    case SET_INIT_SUGAR:
        setInitSugar((int *)argument);
        break;
    case SET_INIT_METABOLISM:
        setInitMetabolism((int *)argument);
        break;
    case MOVE:
        move();
        break;
    case METABOLIZE:
        metabolize();
        break;
    default:
        break;
    }
}