#pragma once

#include "mass/Agent.h"
#include "SugarPlace.h"

#ifndef MAX_INIT_ANT_SUGAR
#define MAX_INIT_ANT_SUGAR 10
#endif

#ifndef MAX_METABOLISM
#define MAX_METABOLISM 4
#endif

#ifndef MAX_VISION
#define MAX_VISION 10
#endif

class Ant : public mass::Agent
{
public:
    MASS_FUNCTION Ant(int index) : mass::Agent(index) {}
    MASS_FUNCTION ~Ant() {}
    // callMethod to override the default behavior of the Agent
    __device__ virtual void callMethod(int functionId, void *arg = NULL);

    // Attributes
    enum ATTRIBUTE
    {
        SUGAR,
        METABOLISM,
        DESTINATION,
    };

    // Methods
    enum FUNCTION
    {
        SET_INIT_SUGAR,
        SET_INIT_METABOLISM,
        METABOLIZE,
        MOVE,
    };

    private:
    /**
     * @brief Set the initial sugar of the ant
     * 
     * @param initSugar an array of initial sugar values
     * 
     * @return void
    */
    __device__ void setInitSugar(int* initSugar);

    /**
     * @brief Set the initial metabolism of the ant
     * 
     * @param initMetabolism an array of initial metabolism values
     * 
     * @return void
    */
    __device__ void setInitMetabolism(int* initMetabolism);

    /**
     * @brief Move the ant to the destination
     * computed by the current place
     * 
     * @return void
    */
    __device__ void move();

    /**
     * @brief Metabolize the sugar of the ant.
     * Ants first collect all the sugar from the place
     * and then metabolize the sugar based on the metabolism
     * 
     * @return void
    */
    __device__ void metabolize();
};