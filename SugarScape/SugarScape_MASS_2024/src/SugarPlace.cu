#include "SugarPlace.h"
#include "Ant.h"

#include "mass/settings.h"

#ifndef MAX_MT_SUGAR
#define MAX_MT_SUGAR 4
#endif

__device__ int SugarPlace::getInitialSugar(int idx, int size, int mtPeakX, int mtPeakY, int maxMtSugar)
{
	// Get the x and y coordinates of the place based on the index and the size
	int x = idx % size;
	int y = idx / size;

	// Calculate the distance from the mountain peak
	float distance = sqrtf((x - mtPeakX) * (x - mtPeakX) + (y - mtPeakY) * (y - mtPeakY));

	// Calculate the initial sugar level based on the distance from the mountain peak
	// We assume the radius of the mountain is half of the size of the grid
	float r = (float)size / 2.0;

	if (distance < r)
	{
		return (int)(maxMtSugar + 0.5 - maxMtSugar / r * distance);
	}
	else
	{
		return 0;
	}
}

__device__ void SugarPlace::setInitialSugar(int size)
{
	// Get the mountain peak coordinates
	int mtPeakX = size / 3;
	int mtPeakY = size - size / 3 - 1;

	// Get the sugar level based on the distance from the mountain peak
	int mt1 = getInitialSugar(getIndex(), size, mtPeakX, mtPeakY, MAX_MT_SUGAR);
	int mt2 = getInitialSugar(getIndex(), size, mtPeakY, mtPeakX, MAX_MT_SUGAR);

	// Set the current and max sugar of the place to the maximum of the two mountains
	int *currentSugar = getAttribute<int>(ATTRIBUTE::CURR_SUGAR, 1);
	int *maxSugar = getAttribute<int>(ATTRIBUTE::MAX_SUGAR, 1);
	*currentSugar = max(mt1, mt2);
	*maxSugar = max(mt1, mt2);
}

__device__ void SugarPlace::updateSugarAndPollution()
{
	// Update the sugar level of the place
	int *currSugar = getAttribute<int>(ATTRIBUTE::CURR_SUGAR, 1);
	int *maxSugar = getAttribute<int>(ATTRIBUTE::MAX_SUGAR, 1);
	// We increase the sugar level of the place by 1 if it is less than the maximum sugar level
	if (*currSugar < *maxSugar)
	{
		(*currSugar)++;
	}

	// Update the pollution level of the place
	// If there is an ant in the place, we increase the pollution level by 1
	// Otherwise, we decrease the pollution level by 1

	// Get the number of ants in the place
	int *numAnts = getAttribute<int>(mass::PlacePreDefinedAttr::AGENT_POPS, 1);
	// Get the pollution level of the place
	float *pollution = getAttribute<float>(ATTRIBUTE::POLLUTION, 1);

	if (*numAnts > 0)
	{
		(*pollution) += 1.0;
	}
	else
	{
		(*pollution) = max(0.0, *pollution - 1.0);
	}
}

__device__ void SugarPlace::calculateAvgPollution()
{
	float avgPollution = 0.0;
	int numNeighbors = 0;

	// Get neighbors of the place
	mass::Place **neighbors = getNeighborsPtr();

	for (int i = 0; i < MAX_NEIGHBORS; i++)
	{
		if (neighbors[i] != nullptr)
		{
			// Increase the number of neighbors
			numNeighbors++;

			// Get the pollution level of the neighbor
			float *pollution = neighbors[i]->getAttribute<float>(ATTRIBUTE::POLLUTION, 1);
			// Add the pollution level of the neighbor to the average pollution
			avgPollution += *pollution;
		}
	}

	// Calculate the average pollution level
	if (numNeighbors > 0)
	{
		avgPollution /= numNeighbors;
	}

	// Set the average pollution level of the place
	float *avgPollutionAttr = getAttribute<float>(ATTRIBUTE::AVG_POLLUTION, 1);
	*avgPollutionAttr = avgPollution;
}

__device__ void SugarPlace::setPollutionByAvg()
{
	// Get the average pollution level of the place
	float *avgPollution = getAttribute<float>(ATTRIBUTE::AVG_POLLUTION, 1);
	// Get the pollution level of the place
	float *pollution = getAttribute<float>(ATTRIBUTE::POLLUTION, 1);

	// Set the pollution level of the place to the average pollution level
	*pollution = *avgPollution;
	// Reset the average pollution level of the place
	*avgPollution = 0.0;
}

__device__ void SugarPlace::updateEnv()
{
	// Update the sugar level and pollution level of the place
	updateSugarAndPollution();
	// Calculate the average pollution level of the place
	calculateAvgPollution();
	// Set the pollution level of the place to the average pollution level
	setPollutionByAvg();
}

__device__ void SugarPlace::findNextDest()
{
	// Get the neighbors of the place
	mass::Place **neighbors = getNeighborsPtr();

	// Get the sugar level of the place
	int *currSugar = getAttribute<int>(ATTRIBUTE::CURR_SUGAR, 1);
	// Get the pollution level of the place
	float *pollution = getAttribute<float>(ATTRIBUTE::POLLUTION, 1);

	// Initialize the next destination to the current place
	mass::Place **nextDest = getAttribute<mass::Place *>(ATTRIBUTE::NEXT_DEST, 1);
	int maxSugar = *currSugar;
	float minPollution = *pollution;

	// Find the neighbor with the highest sugar level and the lowest pollution level
	for (int i = 0; i < MAX_NEIGHBORS; i++)
	{
		if (neighbors[i] != nullptr)
		{
			// Get the sugar level of the neighbor
			int *sugar = neighbors[i]->getAttribute<int>(ATTRIBUTE::CURR_SUGAR, 1);
			// Get the pollution level of the neighbor
			float *poll = neighbors[i]->getAttribute<float>(ATTRIBUTE::POLLUTION, 1);

			// If the sugar level of the neighbor is higher than the current maximum sugar level
			// or the sugar level of the neighbor divided by the pollution level of the neighbor
			// is higher than the current maximum sugar level divided by the minimum pollution level
			// then set the next destination to the neighbor
			if (*sugar > maxSugar || (*sugar / (1.0 + *poll)) > (maxSugar / (1.0 + minPollution)))
			{
				(*nextDest) = neighbors[i];
				maxSugar = *sugar;
				minPollution = *poll;
			}
		}
	}
}

__device__ void SugarPlace::printNextDest()
{
	mass::Place **nextDest = getAttribute<mass::Place *>(ATTRIBUTE::NEXT_DEST, 1);

	if (*nextDest == nullptr)
	{
		// printf("%lu Next destination: nullptr\n", getIndex());
		return;
	}
	else
	{
		printf("%lu Next destination: %lu\n", getIndex(), (*nextDest)->getIndex());
	}
}

__device__ SugarPlace *SugarPlace::getNextDest()
{
	mass::Place **nextDest = getAttribute<mass::Place *>(ATTRIBUTE::NEXT_DEST, 1);

	return (SugarPlace *)(*nextDest);
}

__device__ void SugarPlace::callMethod(int functionId, void *argument)
{
	switch (functionId)
	{
	case SET_INITIAL_SUGAR:
		setInitialSugar(*(int *)argument);
		break;
	case UPDATE_SUGAR_AND_POLLUTION:
		updateSugarAndPollution();
		break;
	case CALCULATE_AVG_POLLUTION:
		calculateAvgPollution();
		break;
	case SET_POLLUTION_BY_AVG:
		setPollutionByAvg();
		break;
	case UPDATE_ENVIRONMENT:
		updateEnv();
		break;
	case FIND_NEXT_DEST:
		findNextDest();
		break;
	case PRINT_NEXT_DEST:
		printNextDest();
		break;
	case GET_NEXT_DEST:
		getNextDest();
		break;
	default:
		break;
	}
}