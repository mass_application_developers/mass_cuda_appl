# Sugarscape
Updateed: 2024-05-10

## Description
- **Sugarscape_GPU**: Not sure what this version is, it is never used.

- **Sugarscape_MASS**: The old implementation of Sugarscape using MASS version 0.5.0, which is the version before the data structure change. It is able to build and run without changing the makefile. It is used to compare the performance of the new implementation.

- **Sugarscape_MASS_2024**: The new implementation of Sugarscape using MASS version >= `0.7.2`, which is the version after the data structure change. It is used to compare the performance of the old implementation. However, the algorithm in this version is not aligned with FLAME GPU 2 implementation. Therefore, it is not used in the comparison, and we develop a new version of Sugarscape to align with FLAME GPU 2 implementation.

- **Sugarscape_MASS_2024_Aligned_FLAME**: The new implementation of Sugarscape using MASS version >= `0.7.2`, which is the version after the data structure change. It is aligned with FLAME GPU 2 implementation. It is used to compare the performance of the old implementation.