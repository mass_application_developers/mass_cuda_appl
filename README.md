# MASS CUDA Applications
Updated: 2024-01-29

The latest implementation of all applications are now merged into the `main` branch. To start implementing your own application, please refer to `AppTemplate` in the `main` branch.

## Introduction
This repository is still under construction. The goal is to provide a set of applications that can be used to test the MASS CUDA framework. The applications are written in C++ and CUDA. More applications, details, and documentation will be added in the future.