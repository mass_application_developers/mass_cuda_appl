# MASS_BailInBailOut

The purpose of this simulation is to model the interaction between individuals, financial firms and banks, and the outcomes of employing bail-in or bail-out to resolve bankruptcy in banks.

## Running the Program

To build and run the Tuberculosis application, first run `make develop`. Once the development environment is setup, run `make build` and then `make test` to ensure all tests are passing properly. Once built, a `MASS_BailInBailOut` executable will be placed within the `./bin` directory, and can be used to run the simulation.

Running the application with the `--help` flag will provide a description of the application and options that can be used to define simulation parameters.

Specifying an output interval, by using the `--interval` flag will tell the program to output the simulation to a `.viz` file every `<interval>` steps. This simulation file can be played using the MASS SimViz application for a graphical visualization of the simulation space. To build the simviz application, run `make build-simviz`.
