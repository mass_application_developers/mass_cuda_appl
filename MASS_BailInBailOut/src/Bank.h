#ifndef BANK_H
#define BANK_H
using namespace std;
#include <iostream>
#include <utility>

#include <mass/Place.h>

#include <mass/Agents.h>
#include "mass/AgentState.h"
#include "mass/Logger.h"
#include <mass/Agent.h>
#include "BankState.h"
#include "FinancialMarket.h"
#include "bailinout.h"

enum BankFunctions : int
{
    INITIALIZE_BANK = 0,
    ACT_ROUND = 1,
    READROUNDTRANSACTIONS = 2,
    ISBANKRUPT = 3,
    RANDOMIZERATE = 4,
    REPORTROUNDVALUES = 5,
    UPDATE_BANK_PLACE_STATE = 6
};

enum State
{
    ACTIVE,
    BANKRUPT
};
// The bank is an agent in the bail-in/out simulation
// Bank can be various states depending on
// the history of interactions in the simulation space.
class Bank : public mass::Agent
{
public:
    MASS_FUNCTION Bank(mass::AgentState *state, void *argument);
    MASS_FUNCTION ~Bank();

    // Calls the method with the given arguments specified by the given function ID.
    MASS_FUNCTION virtual void callMethod(int functionId, void *arg = NULL);

    // Returns the Macrophage's current state.
    MASS_FUNCTION virtual BankState *getState();

private:
    // The macrophage's current state.
    BankState *myState;

    // The bacteria capacity that a macrophage can hold.
    // const int bacteriaCapacity = 100;

    // The threshold of held bacteria which turns an infected macrophage to a
    // chronically infected macrophage.
    // const int chronicInfectionLimit = 75;

    // Number of bloodvessels for macrophage spawn points
    // const int spawnPtNum = 4;

    // Initializes start state
    MASS_FUNCTION void init(void *);

    // Migrates the current macrophage to another EnvironmentPlace if possible.
    MASS_FUNCTION void act();

    //--------------------------- readRoundTransactions --------------------------
    /** Reads all payments made and loans given during this round from the
     *  Financial Market Place.
     *--------------------------------------------------------------------------*/
    MASS_FUNCTION void readRoundTransactions();

    // Applies the rules for a infected macrophage.
    MASS_FUNCTION bool *isBankrupt();

    //---------------------------------- payLoans --------------------------------
    /** Iterates through the vector of loans and attempts to pay back half of the
     *  amount of each outstanding loan. Returns false if the loans could not be
     *  repaid.
     *--------------------------------------------------------------------------*/
    MASS_FUNCTION void payLoans();
    //------------------------------ accumulateInterest --------------------------
    /** Iterates through the vector of loans and compounds interest on each loan
     *  based on the rate at which the loan was given at.
     *--------------------------------------------------------------------------*/
    MASS_FUNCTION void accumulateInterest();

    //----------------------------------- getLoan --------------------------------
    /** Attempts to get a loan from another bank. Returns true if the loan was
     *  secured, and false if the loan was rejected.
     *--------------------------------------------------------------------------*/
    MASS_FUNCTION bool getLoan(int k);

    //------------------------------- randomizeRate ------------------------------
    /** Randomizes the bank's interest rate for a new round.
     *--------------------------------------------------------------------------*/
    MASS_FUNCTION void randomizeRate();

    //----------------------------- reportRoundValues ----------------------------
    /** Writes the Bank's Id, interest rate, liquidity, and location in the place
     *  matrix to the Financial Market.
     *--------------------------------------------------------------------------*/
    MASS_FUNCTION void reportRoundValues();

    MASS_FUNCTION void updatePlaceState();
};

#endif
