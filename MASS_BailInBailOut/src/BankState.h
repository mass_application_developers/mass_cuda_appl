#ifndef BANK_STATE_H
#define BANK_STATE_H

#include "mass/AgentState.h"

#include "FinancialMarketState.h"

class BankState : public mass::AgentState
{
public:
    // the current state, whether it is a spawner,
    double interestRate, liquidity, totalDebt;
    int bankID, state, linearIndex, isSpawner;
    vector<Loan> debt;
};

#endif
