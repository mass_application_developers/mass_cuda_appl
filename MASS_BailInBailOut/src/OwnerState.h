/*==============================================================================
    Owner Class
================================================================================
    The Owner Class owns a single Firm in the Bail-in/Bail-out simulation.
  If the Firm's profits are positive, the Owner gets paid. When the Firm
  has a negative liquidity, the Owner will attempt to provide the Firm
  with a loan to bring them back into the positive.
==============================================================================*/

#ifndef OWNER_STATE_H
#define OWNER_STATE_H
#include "mass/AgentState.h"

class OwnerState : public mass::AgentState
{
public:
    double capital;
    int firmId;
};
#endif