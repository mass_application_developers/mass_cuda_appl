/*==============================================================================
    Firm Class
================================================================================
    The Firm Class is an Agent in the Bail-in/Bail-out simulation. Each
  round, it calculates its costs and profits, pays its workers and owner,
  pays off loans, and determines if it needs a loan if it goes into debt.
  The loan may come from its owner, or a bank. When the Firm takes out a
  loan, it sends a negative value to the bank. If it makes a payment, it
  sends a positive value to the bank.
==============================================================================*/
#ifndef FIRM_H
#define FIRM_H

#include <mass/Agents.h>
#include <mass/Agent.h>
#include "mass/AgentState.h"
#include "FirmState.h"
#include "FinancialMarket.h"
#include "Owner.h"
enum FirmFunctions : int
{
    FINIT = 0,
    ACT = 1,
    FCALCULATEWORKFORCECOST = 2,
    ACCUMULATEINTEREST = 3,
    CALCULATEPROFITS = 4,
    PAYWORKFORCE = 5,
    CALCULATELIQUIDITY = 6,
    GETOWNERLOAN = 7,
    REQUESTBANKLOAN = 8,
    PAYBANK = 9,
    RESETFIRM = 10
};

class Firm : public mass::Agent
{
public:
    MASS_FUNCTION Firm(mass::AgentState *state, void *argument);
    MASS_FUNCTION ~Firm();

    MASS_FUNCTION virtual void callMethod(int funcitonID, void *arg = NULL);
    MASS_FUNCTION virtual FirmState *getState();

public:
    FirmState *myState;
    //============================ Call Method Helpers ===========================

    //---------------------------------- init --------------------------------------
    /** Initializes default values.
     *  @param initArgs - a pair<double,double>* where the first item is the
     *         simulation's production costs, and the second value is the liquidity.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void init(void *initArgs);

    //---------------------------------- act ---------------------------------------
    /** Performs all round actions for the Firm.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void act();

    //---------------------------- calculateWorkforceCost---------------------------
    /** Computes the total sum of all worker wages to be paid out each round.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void calculateWorkforceCost();

    //------------------------------- accumulateInterest ---------------------------
    /** Accumulates interest for every outstanding loan.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void accumulateInterest();

    //--------------------------- calculateProductionCosts -------------------------
    /** Calculates the round's production costs
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void calculateProductionCosts();

    //------------------------------ calculateProfits ------------------------------
    /** Calculates the round's traffic.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void calculateProfits();

    //------------------------------ payWorkforce ----------------------------------
    /** Pays the workers their wages and pays the owner if profits are positive.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void payWorkforce();

    //----------------------------- calculateLiquidity -----------------------------
    /** Computes the liquidity for the round.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void calculateLiquidity();

    //-------------------------------- getOwnerLoan --------------------------------
    /** Asks the Firm's owner for a loan
     *  @return - true if the owner could provide one and false otherwise.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION bool getOwnerLoan();

    //------------------------------- requestBankLoan ------------------------------
    /** Approaches k random banks to ask for a loan at a low interest rate.
     *  @return - returns true if a loan was granted and false otherwise.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION bool requestBankLoan(int k);

    //---------------------------------- payBank -----------------------------------
    /** Attempts to pay back half the amount for all outstanding loans.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void payBank();

    //----------------------------------- resetFirm --------------------------------
    /** Resets a bankrupt firm to default starting values to re-enter simulation.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void resetFirm();
};
#endif