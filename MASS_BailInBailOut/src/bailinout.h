#pragma once

#include <boost/program_options.hpp>

#include <mass/Places.h>
#include <mass/Agents.h>
#include <mass/Place.h>
#include <simviz.h>

#include <vector>

namespace po = boost::program_options;

namespace bailinout
{
    // Simulation configuration options. These are defined in this document:
    // https://docs.google.com/document/d/0B-DdRv6zRAzAS1k1SkJ0aEJ1SDFyTEJWMGlnVDFWVlA3eHhz
    struct ConfigOpts
    {
        int interestRate;
        int sizeX;
        int sizeY;
        int rounds;
        int init_banks;
        double liquidity;
    };

    // runSimulation runs the bailinout simulation, configured with the
    // provided options.
    void runSimulation(ConfigOpts opts, int interval, simviz::RGBFile &vizFile);

    // outputSimSpace outputs a graphical visualiztion of the simulation space
    // to the provided simviz file.
    // Each place is represented by a 2x2 grid. The top left pixel is for
    // the Bank (black if present).
    // The top right pixel is for the Firm
    // The liquidity is represented by red
    // The bottom right pixel next to Firm is owner represents  (1 - orange, 2 - red)
    void outputSimSpace(simviz::RGBFile &vizFile, mass::Places *places, int sizeX, int sizeY);

    // parseSimConfig parses the provided variables_map and
    // returns configs options for which to run the simulation
    // with.
    ConfigOpts parseSimConfig(po::variables_map vm);

    void spawnInitialBank(int sizeX, int sizeY, int *bankSpawn, int totalBank);

    int *generateRandVals(int n);

}