#ifndef WORKER_H
#define WORKER_H

#include "FinancialMarket.h"
#include "WorkerState.h"
#include <mass/Agents.h>
#include "mass/AgentState.h"
#include "mass/Logger.h"
#include <mass/Agent.h>
enum WorkerFunctions : int
{
    WINIT = 0,
    WACT = 1,
    RECEIVEWAGES = 2,
    CONSUME = 3,
    DEPOSITFUNDS = 4
};

class Worker : public mass::Agent
{
public:
    MASS_FUNCTION Worker(mass::AgentState *state, void *argument);
    MASS_FUNCTION ~Worker();

    // Calls the method with the given arguments speicifed by the given funciton

    MASS_FUNCTION virtual void callMethod(int functionId, void *arg = NULL);

    // Returns the Worker's current state
    MASS_FUNCTION virtual WorkerState *getState();

private:
    // Worker's current state
    WorkerState *myState;
    //============================ Call Method Helpers ===========================

    //----------------------------------- init -------------------------------------
    /** Initializes Worker agent's member data.
     *  @param initArgs - an InitWorker* object storing values for the member data.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void init(void *initArgs);

    //------------------------------------- act ------------------------------------
    /** Performs all round actions for the Firm.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void act();

    //-------------------------------- receiveWages --------------------------------
    /** Receive wages for this round.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void receiveWages();

    //---------------------------------- consume -----------------------------------
    /** Spend wages consuming products. Note: this function reduces wages for next
     *  round as well.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void consume();

    //-------------------------------- depositFunds --------------------------------
    /** Sends a deposit to the bank
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void depositFunds();
};
#endif