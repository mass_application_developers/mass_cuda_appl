#include "Firm.h"

MASS_FUNCTION Firm::Firm(mass::AgentState *state, void *argument) : Agent(state, argument)
{
    myState = (FirmState *)state;
}

MASS_FUNCTION Firm::~Firm() {}

MASS_FUNCTION void Firm::callMethod(int functionId, void *argument)
{
    switch (functionId)
    {
    case FirmFunctions::FINIT:
        init(argument);
        break;
    case FirmFunctions::FCALCULATEWORKFORCECOST:
        calculateWorkforceCost();
        break;
    case FirmFunctions::ACT:
        act();
        break;
    case FirmFunctions::ACCUMULATEINTEREST:
        accumulateInterest();
        break;
    case FirmFunctions::CALCULATEPROFITS:
        calculateProfits();
        break;
    case FirmFunctions::PAYWORKFORCE:
        payWorkforce();
        break;
    case FirmFunctions::CALCULATELIQUIDITY:
        calculateLiquidity();
        break;
    case FirmFunctions::PAYBANK:
        payBank();
        break;
    case FirmFunctions::GETOWNERLOAN:
        getOwnerLoan();
        break;
    case FirmFunctions::REQUESTBANKLOAN:
        requestBankLoan(*(static_cast<int *>(argument)));
        break;
    case FirmFunctions::RESETFIRM:
        resetFirm();
        break;
    default:
        break;
    }
}

MASS_FUNCTION FirmState *Firm::getState()
{
    return myState;
}

//---------------------------------- init --------------------------------------
/** Initializes default values.
 *  @param initArgs - a pair<double,double>* where the first item is the
 *         simulation's production costs, and the second value is the liquidity.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void Firm::init(void *initArgs)
{
    myState->productionCost = myState->resetVals.first = ((pair<double, double> *)initArgs)->first;
    myState->liquidity = myState->resetVals.second = ((pair<double, double> *)initArgs)->second;
    myState->profit = 0;
    myState->firmId = getIndex();
    myState->owner = new Owner(myState, 0);
    FinancialMarket *myPlace = (FinancialMarket *)getPlace();
    myPlace->setOwner(true);
}

//---------------------------- calculateWorkforceCost---------------------------
/** Computes the total sum of all worker wages to be paid out each round.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void Firm::calculateWorkforceCost()
{
    double *temp = static_cast<double *>(((FinancialMarket *)getPlace())->calculateWorkforceCost());
    myState->workforceCost = *temp;
    delete temp;
}

//---------------------------------- act ---------------------------------------
/** Performs all round actions for the Firm.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void Firm::act()
{
    accumulateInterest();       // 1) Calculate Interest on outstanding loans
    calculateProductionCosts(); // 2) Calculate production costs
    calculateProfits();         // 3) Calculate Profits
    payWorkforce();             // 4) Pay Worker Wages
    calculateLiquidity();       // 5) Calculate liquidity
    payBank();                  // 6) Pay Banks for outstanding loans
    bool costsCovered = (myState->liquidity >= 0);
    if (!costsCovered)
    {
        costsCovered = getOwnerLoan(); // 7a) Ask Owner for loan
        if (!costsCovered && myState->debt.size() <= 0)
        {
            costsCovered = requestBankLoan(3);
        }
    }
    if (myState->liquidity < 0)
        resetFirm();
}

//------------------------------- accumulateInterest ---------------------------
/** Accumulates interest for every outstanding loan.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void Firm::accumulateInterest()
{
    for (int curLoan = 0; curLoan < myState->debt.size(); curLoan++)
    {
        myState->debt[curLoan].amount = myState->debt[curLoan].amount * (1.0 + myState->debt[curLoan].interestRate);
    }
}

//--------------------------- calculateProductionCosts -------------------------
/** Calculates the round's production costs
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void Firm::calculateProductionCosts()
{
    double modifier = (float)((float)rand() / RAND_MAX + 0.5);
    myState->productionCost *= modifier;
}

//------------------------------ calculateProfits ------------------------------
/** Calculates the round's traffic.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void Firm::calculateProfits()
{
    double x = (float)((float)rand() / RAND_MAX + 0.5);
    myState->profit = (myState->productionCost * x) - myState->productionCost;
}

//------------------------------ payWorkforce ----------------------------------
/** Pays the workers their wages and pays the owner if profits are positive.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void Firm::payWorkforce()
{
    myState->profit -= myState->workforceCost; // Pay Workers
    double dividends;                          // Owner's cut
    if (myState->profit > 0)
    {
        dividends = myState->profit * 0.2;
        myState->profit -= dividends;
    }
    else
    {
        dividends = 0;
    }
    myState->owner->setCapital(myState->owner->getCapital() + dividends);
}

//----------------------------- calculateLiquidity -----------------------------
/** Computes the liquidity for the round.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void Firm::calculateLiquidity()
{
    myState->liquidity += myState->profit;
}

//---------------------------------- payBank -----------------------------------
/** Attempts to pay back half the amount for all outstanding loans.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void Firm::payBank()
{
    for (int curLoan = 0; curLoan < myState->debt.size(); curLoan++)
    {
        double amtToPay = myState->debt[curLoan].amount / 2;
        myState->debt[curLoan].amount -= amtToPay;
        myState->liquidity -= amtToPay;
        pair<int, double> payment;
        payment.first = myState->debt[curLoan].loaningBankId;
        payment.second = amtToPay; // Positive value
        FinancialMarket *myPlace = (FinancialMarket *)getPlace();
        myPlace->callMethod(FinancialMarketFunctions::ADDBANKTRANSACTION, (void *)&payment);
    }
}

//-------------------------------- getOwnerLoan --------------------------------
/** Asks the Firm's owner for a loan
 *  @return - true if the owner could provide one and false otherwise.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION bool Firm::getOwnerLoan()
{
    if (myState->owner->getCapital() > myState->liquidity * -1)
    {
        double amount = myState->liquidity * -1;
        myState->owner->setCapital(myState->owner->getCapital() - amount);
        myState->liquidity += amount;
        return true;
    }
    return false;
}

//------------------------------- requestBankLoan ------------------------------
/** Approaches k random banks to ask for a loan at a low interest rate.
 *  @return - returns true if a loan was granted and false otherwise.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION bool Firm::requestBankLoan(int k)
{
    FinancialMarketState *myStatePlace = (FinancialMarketState *)getState();

    BankSelectionArgs selArgs;
    myStatePlace->bankselectionargs->minAcceptableInterest = 2134.0; // Value matches RepastHPC benchmark
    myStatePlace->bankselectionargs->amountNeeded = myState->liquidity * -1;
    myStatePlace->bankselectionargs->k = k;
    FinancialMarket *myPlace = (FinancialMarket *)getPlace();

    myPlace->selectFromBanks();
    if (myStatePlace->bankinfo != nullptr)
    {
        Loan newLoan;
        newLoan.amount = myState->liquidity * -1; // Positive value
        newLoan.interestRate = myStatePlace->bankinfo->interestRate;
        newLoan.loaningBankId = myStatePlace->bankinfo->bankId;
        myState->debt.push_back(newLoan);
        myState->liquidity += newLoan.amount;
        /*Add loan information to Financial Market's Bank Transactions*/
        pair<int, double> loanReceipt = {myStatePlace->bankinfo->bankId, newLoan.amount * -1};
        myPlace->callMethod(FinancialMarketFunctions::ADDBANKTRANSACTION, (void *)&loanReceipt);
        return true;
    }
    else
    {
        return false;
    }
}

//----------------------------------- resetFirm --------------------------------
/** Resets a bankrupt firm to default starting values to re-enter simulation.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void Firm::resetFirm()
{
    myState->productionCost = myState->resetVals.first;
    myState->liquidity = myState->resetVals.second;
    myState->profit = 0;
    myState->owner->setCapital(myState->liquidity * ((float)rand() / RAND_MAX + 0.5));
    myState->debt.clear();
}

// End Firm.cpp