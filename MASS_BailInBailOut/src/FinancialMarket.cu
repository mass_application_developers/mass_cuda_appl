#include "FinancialMarket.h"

using namespace std;
using namespace mass;

MASS_FUNCTION FinancialMarket::FinancialMarket(PlaceState *state, void *argument) : Place(state, argument)
{ // not sure this is right initialization of place constructor
    myState = (FinancialMarketState *)state;
    myState->numBanks = ((pair<int, int> *)argument)->first;
    myState->numFirms = ((pair<int, int> *)argument)->second;
}

MASS_FUNCTION FinancialMarket::~FinancialMarket() {}

MASS_FUNCTION void FinancialMarket::callMethod(int functionId, void *arg)
{
    switch (functionId)
    {
    case FinancialMarketFunctions::INIT:
        init(arg);
        break;
    case FinancialMarketFunctions::PREPBANKINFOEXCHANGE:
        prepBankInfoExchange(arg);
        break;
    case FinancialMarketFunctions::ADDWORKER:
        addWorker(arg);
        break;
    case FinancialMarketFunctions::CALCULATEWORKFORCECOST:
        calculateWorkforceCost();
        break;
    case FinancialMarketFunctions::ADDBANK:
        addBank(arg);
        break;
    case FinancialMarketFunctions::EXCHANGEBANKINFO:
        exchangeBankInfo();
        break;
    case FinancialMarketFunctions::PREPFORINCOMINGTRANSACTIONS:
        prepForIncomingTransactions(arg);
        break;
    case FinancialMarketFunctions::EXCHANGEOUTGOINGTRANSACTIONS:
        exchangeOutgoingTransactions();
        break;
    case FinancialMarketFunctions::MANAGEINCOMINGTRANSACTIONS:
        manageIncomingTransactions();
        break;
    case FinancialMarketFunctions::GETINCOMINGTRANSACTIONAMT:
        getIncomingTransactionAmt();
        break;
    case FinancialMarketFunctions::RESETROUNDVALUES:
        resetRoundValues();
        break;
    case FinancialMarketFunctions::SELECTFROMBANKS:
        selectFromBanks();
        break;
    case FinancialMarketFunctions::ADDBANKTRANSACTION:
        addBankTransaction(arg);
        break;
    default:
        break;
    }
}

MASS_FUNCTION void FinancialMarket::init(void *nAgents)
{
    myState->residingBank = -1;
    myState->numBanks = ((pair<int, int> *)nAgents)->first;
    myState->numFirms = ((pair<int, int> *)nAgents)->second;
    myState->randState = 0;
}

MASS_FUNCTION void FinancialMarket::addBank(void *bankInfo)
{
    myState->residingBank = ((BankInfo *)bankInfo)->bankId;

    myState->bankRegistry[myState->residingBank] = (BankInfo *)bankInfo;
}
//-------------------------------- addWorker -----------------------------------
/** Inserts the wages of a residing worker into the localWorkerWages map for
 *  firms to read and calculate their workforce cost.
 *  @param workerInfo - a pair<double,int> with the first item as the wages and
 *         the second item as the workerId.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void FinancialMarket::addWorker(void *workerInfo)
{
    myState->localWorkerWages[((pair<double, int> *)workerInfo)->second] = ((pair<double, int> *)workerInfo)->first;
    setWorker(true);
}

//-------------------------- calculateWorkforceCost ----------------------------
/** Calculates the sum of the wages of all residing workers.
 *  @return - a double* reporting the sum of all worker wages on this place.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void *FinancialMarket::calculateWorkforceCost()
{
    double *totalWages = new double(0.0);
    for (auto worker : myState->localWorkerWages)
    {
        *totalWages += worker.second;
    }
    myState->localWorkerWages.clear();

    return (void *)totalWages;
}

//------------------------------ updateBankInfo --------------------------------
/** Updates the residing Bank's information to reflect any changes to its values
 *  during the last round.
 *  @param updatedInfo - a BankInfo* storing the info of the residing bank.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void FinancialMarket::updateBankInfo(void *updatedInfo)
{
    if (updatedInfo != nullptr)
    {
        BankInfo *b = new BankInfo(*(BankInfo *)updatedInfo);
        unordered_map<int, BankInfo *>::iterator it = myState->bankRegistry.find(b->bankId);
        if (it != myState->bankRegistry.end())
        {
            delete it->second; // Delete old BankInfo* if it already exists
            it->second = b;    // Set new BankInfo*
        }
        else
        {
            myState->bankRegistry[b->bankId] = b;
        }
    }
}
//---------------------------- prepBankInfoExchange ----------------------------
/** Prepares each Place to receive information on the Banks in the simulation.
 *  Must be called before exchangeBankInfo().
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void FinancialMarket::prepBankInfoExchange(void *agent)
{

    cleanNeighbors();                           // Clear old neighbors
    for (int n = 0; n < myState->numBanks; n++) /*Add places with banks as neighbors*/
    {
        FinancialMarket *neighbor = (FinancialMarket *)myState->neighbors[n];
        if (neighbor != NULL)
        {
            neighbor->setBank(true);
            neighbor->addAgent((mass::Agent *)agent);
        }
    }
}
MASS_FUNCTION void FinancialMarket::cleanNeighbors()
{

    for (int i = 0; i < MAX_NEIGHBORS; i++)
    {
        if (myState->neighbors[i])
        {
            delete myState->neighbors[i];
            myState->neighbors[i] = NULL;
        }
    }
}

//------------------------------ exchangeBankInfo ------------------------------
/** Returns a BankInfo object containing all information for the residing bank.
 *  prepBankInfoExchange() must be called before this function.
 *  @return - a BankInfo* containing the information of the residing bank.
 *----------------------------------------------------------------------------*/

MASS_FUNCTION void *FinancialMarket::exchangeBankInfo()
{
    BankInfo *msg = new BankInfo;
    if (myState->residingBank != -1)
        *msg = *(myState->bankRegistry[myState->residingBank]);
    else
        msg->bankId == -1;

    return msg;
}

//------------------------ prepForIncomingTransactions -------------------------
/** Prepares all Financial Market elements with a residing Bank agent to send
 *  the id of the bank as an integer, and receive the total value of
 *  transactions accumulated from all other Financial Market places during the
 *  round. This function must precede exchangeOutgoingTransactions.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void FinancialMarket::prepForIncomingTransactions(void *agent)
{
    cleanNeighbors(); // Remove old enighbors
    for (int i = 0; i < myState->numFirms; i++)
    {
        FinancialMarket *neighbor = (FinancialMarket *)myState->neighbors[i];
        if (neighbor != NULL)
        {
            neighbor->setFirm(true);
            neighbor->addAgent((mass::Agent *)agent);
        }
    }
}
//------------------------ exchangeOutgoingTransactions ------------------------
/** Receives a bankId and returns the total sum of all transactions accumulated
 *  for that bank during the round. Must be preceded by the function
 *  prepForIncomingTransactions().
 *  @param givenBankId - an int representing the bank's id.
 *  @return - a double representing the value of the transactions.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void FinancialMarket::exchangeOutgoingTransactions()
{
    int givenBankId = getIndex();
    double *amount = new double; // value to be returned
    *amount = 0.0;
    if (givenBankId == -1)
    {
        unordered_map<int, double>::iterator transaction = myState->outgoingTransactions.find(givenBankId);
        if (transaction != myState->outgoingTransactions.end())
        {
            *amount = myState->outgoingTransactions[givenBankId];
        }
    }

    myState->loan->amount = *amount;
}
//------------------------------ selectFromKBanks ------------------------------
/** Chooses from k banks the bank with the lowest interest rate that can afford
 *  to provide the loan/
 *  @param arg - a BankSelectionArgs* where k is the number of banks to choose
 *         from, minAcceptableInterest is the lowest interest rate that is
 *         acceptable, and amountNeeded is the amount needed from the bank.
 *  @return - Returns a BankInfo* storing the information on the bank selected.
 *         if no bank fit the criteria, nullptr is returned.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void FinancialMarket::selectFromBanks()
{

    if (myState->bankselectionargs->k > myState->numBanks)
        myState->bankselectionargs->k = myState->numBanks;

    set<int> consideredBanks;
    for (int i = 0; i < myState->bankselectionargs->k; i++) /* Select k random, unique banks*/
    {
        int randomBankId;
        do
        {
            randomBankId = (int)(rand()) % (myState->numBanks);
        } while (consideredBanks.find(randomBankId) != consideredBanks.end());
        consideredBanks.insert(randomBankId);
        unordered_map<int, BankInfo *>::iterator it = myState->bankRegistry.find(randomBankId);
        if (it != myState->bankRegistry.end())
        {
            // Find bank with lowest interest rate that can afford to offer the loan
            if (it->second->interestRate < myState->bankselectionargs->minAcceptableInterest && it->second->liquidity > myState->bankselectionargs->amountNeeded)
            {
                myState->bankselectionargs->minAcceptableInterest = it->second->interestRate;
                myState->bankinfo = it->second;
            }
        }
    }
}
//-------------------------- addBankTransaction --------------------------------
/** Adds the monetary value of a transaction for a specified bank. A loan will
 *  be a negative value, decreasing the running total, and a deposit will be
 *  a positive value, increasing the running total.
 *  @param transactionInfo - a pair<int,double>* where the first item is the
 *         bankId and the second item is the transaction amount.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void FinancialMarket::addBankTransaction(void *transaction)
{
    // mtx.lock(); // Begin Critical Section
    myState->outgoingTransactions[((pair<int, double> *)transaction)->first] += ((pair<int, double> *)transaction)->second;
    // mtx.unlock(); // End Critical Section
}
//-------------------------- manageIncomingTransactions ------------------------
/** Has any Financial Market with a residing Bank sort through its inMessages
 *  and total the transactions received from other place elements. Must be
 *  preceded by exchangeOutgoingTransactions().
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void FinancialMarket::manageIncomingTransactions()
{
    if (myState->residingBank != -1) /* Have places w/ banks read transactions*/
    {
        // Add transactions from self
        unordered_map<int, double>::iterator transactionLookup = myState->outgoingTransactions.find(myState->residingBank);
        if (transactionLookup != myState->outgoingTransactions.end())
        {
            myState->incomingTransactions += transactionLookup->second;
        }
    }
}
//------------------------- getIncomingTransactionAmt --------------------------
/** Returns the sum of all incoming transactions totalled in the
 *  manageIncomingTransactions() function.
 *  @return - a double storing the value of all incoming transactions
 * */
MASS_FUNCTION void *FinancialMarket::getIncomingTransactionAmt()
{
    return (void *)&myState->incomingTransactions;
}
//---------------------------- resetRoundValues --------------------------------
// Resets the round values to prepare for next round
MASS_FUNCTION void FinancialMarket::resetRoundValues()
{
    myState->incomingTransactions = 0.0;
    myState->outgoingTransactions.clear();
    cleanNeighbors();
}

MASS_FUNCTION bool FinancialMarket::getBank()
{
    return myState->bank;
}

MASS_FUNCTION void FinancialMarket::setBank(bool newBank)
{
    if (myState->bank != newBank)
    {
        myState->bank = newBank;
    }
}

// Firm
MASS_FUNCTION bool FinancialMarket::getFirm()
{
    return myState->firm;
}
MASS_FUNCTION void FinancialMarket::setFirm(bool newFirm)
{

    if (myState->firm != newFirm)
    {
        myState->firm = newFirm;
    }
}
// Worker
MASS_FUNCTION bool FinancialMarket::getWorker()
{
    return myState->worker;
}
MASS_FUNCTION void FinancialMarket::setWorker(bool newWorker)
{
    if (myState->worker != newWorker)
    {
        myState->worker = newWorker;
    }
}
// Owner
MASS_FUNCTION bool FinancialMarket::getOwner()
{
    return myState->owner;
}

MASS_FUNCTION void FinancialMarket::setOwner(bool newOwner)
{
    if (myState->owner != newOwner)
    {
        myState->owner = newOwner;
    }
}