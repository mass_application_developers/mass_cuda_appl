#include <iostream>

// Boost APIs
#include <boost/program_options.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include <simviz.h>

#include <mass/Mass.h>
#include <mass/Logger.h>

#include "bailinout.h"
#include "Timer.h"

namespace po = boost::program_options;
namespace logging = boost::log;

int main(int argc, char *argv[])
{
    // Setup and parse program options.
    po::options_description desc("general options");
    printf("testing");
    desc.add_options()("help", "print help message")("verbose", po::bool_switch()->default_value(false), "set verbose output")("interval", po::value<int>()->default_value(0), "output interval")("out_file", po::value<std::string>()->default_value("./bailinout.viz"), "SimViz file output");
    printf("testing");
    po::options_description sim_opts("simulation options");
    sim_opts.add_options()("interestRate", po::value<int>()->default_value(-1), "interestRate used for RNG. if left unset, current time is used")("sizeX", po::value<int>()->default_value(30), "size horizontal of simulation simulation")("sizeY", po::value<int>()->default_value(30), "size vertical space of simulation")("rounds", po::value<int>()->default_value(100), "max simulation time steps")("init_Banks", po::value<int>()->default_value(100), "initial number of Banks")("liquidity", po::value<double>()->default_value(1000.0), "initial amounts of liquidity");
    desc.add(sim_opts);

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help"))
    {
        std::cout << desc << std::endl;

        return 0;
    }

    // Setup logger.
    logging::trivial::severity_level log_level = logging::trivial::info;
    if (vm["verbose"].as<bool>())
    {
        log_level = logging::trivial::debug;
    }

    mass::logger::setLogLevel(log_level);

    // Parse simulation options and get a config struct.
    bailinout::ConfigOpts opts = bailinout::parseSimConfig(vm);

    int interval = vm["interval"].as<int>();
    mass::logger::info(
        "Running bailinout with params: interestRate=%d, sizeX=%d, sizeY=%d, nums_rounds =%d, init_banks= %d, liquidity=%d, interval=%d",
        opts.interestRate, opts.sizeX, opts.sizeY, opts.rounds, opts.init_banks, opts.liquidity, interval);

    // Create viz file if interval is non-zero.
    simviz::RGBFile vizFile(opts.sizeX * 2, opts.sizeY * 2);
    if (interval > 0)
    {
        vizFile.open(vm["out_file"].as<std::string>().c_str());
    }

    Timer timer;
    timer.start();

    // Run application
    bailinout::runSimulation(opts, interval, vizFile);

    mass::logger::info("Total execution time %dus\n", timer.lap());

    vizFile.close();

    return 0;
}