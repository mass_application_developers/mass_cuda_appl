#include "Owner.h"

using namespace std;

MASS_FUNCTION Owner::Owner(mass::AgentState *state, void *argument) : Agent(state, argument)
{
    myState = (OwnerState *)state;
}

MASS_FUNCTION Owner::~Owner() {}

MASS_FUNCTION void Owner::callMethod(int functionId, void *argument)
{
    switch (functionId)
    {
    case OwnerFunctions::GETCAPITAL:
        getCapital();
        break;
    case OwnerFunctions::SETCAPITAL:
        setCapital(*(static_cast<double *>(argument)));
        break;
    default:
        break;
    }
}

MASS_FUNCTION OwnerState *Owner::getState()
{
    return myState;
}

MASS_FUNCTION void Owner::setCapital(double newCapital)
{
    myState->capital = newCapital;
}

MASS_FUNCTION double Owner::getCapital()
{
    return myState->capital;
}
