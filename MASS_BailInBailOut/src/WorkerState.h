/*==============================================================================
    Worker Class
    Author: Ian Dudder
================================================================================
    The Worker Class is an Agent in the Bail-in/Bail-out simulation. In
  each round of the simulation, the Worker will receive wages from its
  Firm, spend wages, and deposit leftover wages in the Bank.
==============================================================================*/
#ifndef WORKER_STATE_H
#define WORKER_STATE_H

#include "mass/AgentState.h"

struct InitWorker
{
    double defaultWage;
    int numWorker;
    int numBanks;
};

class WorkerState : public mass::AgentState
{
public:
    InitWorker *initworker;
    double wages;             // Income
    double capital;           // Saved money
    double consumptionBudget; // Amount spend on products each step
    int workerId;             // Unique idetifier for worker
    int myBank;               // Identifies bank holding personal account
};
#endif