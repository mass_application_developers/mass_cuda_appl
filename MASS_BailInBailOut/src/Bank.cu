#include "Bank.h"

MASS_FUNCTION Bank::Bank(mass::AgentState *state, void *argument) : Agent(state, argument)
{
    myState = (BankState *)state;
    myState->state = ACTIVE;
    myState->totalDebt = 0;
}

MASS_FUNCTION Bank::~Bank() {}

MASS_FUNCTION void Bank::callMethod(int functionId, void *argument)
{
    switch (functionId)
    {
    case BankFunctions::INITIALIZE_BANK:
        init(argument);
        break;
    case BankFunctions::ACT_ROUND:
        act();
        break;
    case BankFunctions::READROUNDTRANSACTIONS:
        readRoundTransactions();
        break;
    case BankFunctions::ISBANKRUPT:
        isBankrupt();
        break;
    case BankFunctions::RANDOMIZERATE:
        randomizeRate();
        break;
    case BankFunctions::REPORTROUNDVALUES:
        reportRoundValues();
        break;
    default:
        break;
    }
}

MASS_FUNCTION BankState *Bank::getState()
{
    return myState;
}

// MASS_FUNCTION void Bank::init(bailinout::Bank bank)
MASS_FUNCTION void Bank::init(void *initVals)
{
    BankInfo *myInfo = new BankInfo;
    // set up agent variable
    // interest Rate
    myState->interestRate = myInfo->interestRate = ((pair<double, double> *)initVals)->first;

    myState->liquidity = myInfo->interestRate = ((pair<double, double> *)initVals)->second;
    // liquidity

    // BankID (it is supposed to put agent id!! instead of getIndex())
    myState->bankID = myInfo->bankId = getIndex();

    // Index;
    myState->linearIndex = getIndex();

    // totalDebt
    myState->totalDebt = 0;
    // place->callMethod(FinancialMarket::addBank, myinfo);
    ((FinancialMarket *)getPlace())->addBank(myInfo);
}

MASS_FUNCTION void Bank::act()
{
    payLoans();                 // 1) Pay outstanding loans to other banks
    accumulateInterest();       // 2) Accumulate interest on outstanding loans
    if (myState->liquidity < 0) // 3) Get a loan if needed
        getLoan(3);
}

//---------------------------- readRoundTransactions ---------------------------
/** Reads all payments made and loans given during this round from the Financial
 *  Market Place.
 *-----------------------------------------------------------------------------*/
MASS_FUNCTION void Bank::readRoundTransactions()
{
    FinancialMarket *myPlace = (FinancialMarket *)getPlace();

    double transactionAmt = *(double *)myPlace->getIncomingTransactionAmt();

    myState->liquidity += transactionAmt; // TransactionAmt may be positive (from deposits) or negative (from loans)
}

MASS_FUNCTION bool *Bank::isBankrupt()
{
    bool *ret = new bool(myState->liquidity + myState->totalDebt < 0);
    return ret;
}

MASS_FUNCTION void Bank::reportRoundValues()
{
    BankInfo myInfo;
    myInfo.interestRate = myState->interestRate;
    myInfo.liquidity = myState->liquidity;
    myInfo.bankId = myState->bankID;
    // myInfo.linearIndex = index[0];
    FinancialMarket *myPlace = (FinancialMarket *)getPlace();
    myPlace->callMethod(FinancialMarketFunctions::UPDATEBANKINFO, &myInfo);
}
//----------------------------------- payLoans ---------------------------------
/** Iterates through the vector of loans and attempts to pay back half of the
 *  amount of each outstanding loan. If a loan cannot be repaid, the liquidity
 *  is set to -1.
 *---------------------------------------------------------------------------*/
MASS_FUNCTION void Bank::payLoans()
{
    for (int curLoan = 0; curLoan < myState->debt.size(); curLoan++)
    {
        double amtToPay = myState->debt[curLoan].amount / 2;
        if (myState->liquidity > amtToPay)
        {
            myState->debt[curLoan].amount -= amtToPay;
            myState->liquidity -= amtToPay;
            myState->totalDebt -= amtToPay;
            // Pass payment to the Financial Market to forward to the bank
            pair<int, double> payment = {myState->debt[curLoan].loaningBankId, amtToPay};
            FinancialMarket *myPlace = (FinancialMarket *)getPlace();
            myPlace->callMethod(FinancialMarketFunctions::ADDBANKTRANSACTION, (void *)&payment);
        }
        else
        {
            myState->liquidity = -1;
        }
    }
}
//------------------------------- accumulateInterest ---------------------------
/** Iterates through the vector of loans and compounds interest on each loan
 *  based on the rate at which the loan was given at.
 *-----------------------------------------------------------------------------*/
MASS_FUNCTION void Bank::accumulateInterest()
{
    for (int curLoan = 0; curLoan < myState->debt.size(); curLoan++)
    {
        double increase = myState->debt[curLoan].amount * myState->debt[curLoan].interestRate;
        myState->debt[curLoan].amount += increase;
        myState->totalDebt += increase;
    }
}
//------------------------------------ getLoan ---------------------------------
/** Attempts to get a loan from another bank. Returns true if the loan was
 *  secured, and false if the loan was rejected.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION bool Bank::getLoan(int k)
{
    FinancialMarketState *myStatePlace = (FinancialMarketState *)getState();
    myStatePlace->bankselectionargs->minAcceptableInterest = 2134.0;
    myStatePlace->bankselectionargs->amountNeeded = myStatePlace->bankinfo->liquidity * -1;
    myStatePlace->bankselectionargs->k = k;
    FinancialMarket *myPlace = (FinancialMarket *)getPlace();

    myPlace->selectFromBanks();
    if (myStatePlace->bankinfo != nullptr)
    {
        Loan newLoan;                             // Add loan to debt
        newLoan.amount = myState->liquidity * -1; // Save loan as a positive value (liq is negative)
        newLoan.interestRate = myStatePlace->bankinfo->interestRate;
        newLoan.loaningBankId = myStatePlace->bankinfo->bankId;
        myState->debt.push_back(newLoan);
        myState->totalDebt += newLoan.amount;
        myState->liquidity += newLoan.amount;
        // Send Loan to the Financial Market
        pair<int, double> loanReceipt = {myStatePlace->bankinfo->bankId, (newLoan.amount * -1)};
        myPlace->callMethod(FinancialMarketFunctions::ADDBANKTRANSACTION, (void *)&loanReceipt);
        return true;
    }
    else
    {
        return false;
    }
}

//--------------------------------- randomizeRate ------------------------------
/** Randomizes the bank's interest rate for a new round.
 *---------------------------------------------------------------------------*/
MASS_FUNCTION void Bank::randomizeRate()
{
    myState->interestRate = (float)rand() / RAND_MAX;
}
