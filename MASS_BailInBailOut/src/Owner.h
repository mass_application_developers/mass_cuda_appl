#ifndef OWNER_H
#define OWNER_H

#include <mass/Agent.h>
#include <mass/Agents.h>
#include "mass/AgentState.h"
#include "OwnerState.h"
#include "FinancialMarket.h"

enum OwnerFunctions : int
{
    GETCAPITAL = 0,
    SETCAPITAL = 1
};

class Owner : public mass::Agent
{
public:
    MASS_FUNCTION Owner(mass::AgentState *state, void *argument);
    MASS_FUNCTION ~Owner();

    MASS_FUNCTION virtual void callMethod(int functionId, void *arg = NULL);

    MASS_FUNCTION virtual OwnerState *getState();

public:
    OwnerState *myState;
    MASS_FUNCTION double getCapital();
    MASS_FUNCTION void setCapital(double newCapital);
};

#endif