#include "Worker.h"

MASS_FUNCTION Worker::Worker(mass::AgentState *state, void *argument) : Agent(state, argument)
{
    myState = (WorkerState *)state;
}

MASS_FUNCTION Worker::~Worker() {}

MASS_FUNCTION void Worker::callMethod(int functionId, void *argument)
{
    switch (functionId)
    {
    case WorkerFunctions::WINIT:
        init(argument);
        break;
    case WorkerFunctions::WACT:
        act();
        break;
    case WorkerFunctions::RECEIVEWAGES:
        receiveWages();
        break;
    case WorkerFunctions::CONSUME:
        consume();
        break;
    case WorkerFunctions::DEPOSITFUNDS:
        depositFunds();
        break;
    default:
        break;
    }
}

MASS_FUNCTION WorkerState *Worker::getState()
{
    return myState;
}
//----------------------------------- init -------------------------------------
/** Initializes Worker agent's member data.
 *  @param initArgs - an InitWorker* object storing values for the member data.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void Worker::init(void *initArgs)
{
    myState->wages = ((InitWorker *)initArgs)->defaultWage / (((InitWorker *)initArgs)->numWorker * ((float)rand() / RAND_MAX + 0.5));
    myState->capital = 0;
    myState->consumptionBudget = 0.3;
    myState->workerId = getIndex();
    pair<double, int> *workerCost = new pair<double, int>(myState->wages, myState->workerId);
    ((FinancialMarket *)getPlace())->addWorker((void *)workerCost);
    delete workerCost;
    myState->myBank = (int)(rand()) % ((InitWorker *)initArgs)->numBanks;
}

//------------------------------------- act ------------------------------------
/** Performs all round actions for the Firm.
 *----------------------------------------------------------------------------*/

MASS_FUNCTION void Worker::act()
{
    int fixedWage = myState->wages;
    receiveWages();
    consume();
    depositFunds();
    myState->wages = fixedWage;
}

//-------------------------------- receiveWages --------------------------------
/** Receive wages for this round.
 *----------------------------------------------------------------------------*/

MASS_FUNCTION void Worker::receiveWages()
{
    myState->capital += myState->wages;
}
//---------------------------------- consume -----------------------------------
/** Spend wages consuming products. Note: this function reduces wages for next
 *  round as well.
 *----------------------------------------------------------------------------*/
MASS_FUNCTION void Worker::consume()
{
    // it seems that the consumptionBudget should be deducted from the wage.
    // this has to be double checked
    myState->wages *= myState->consumptionBudget; // Decrease wages (matches RepastHPC benchmark)
}

//-------------------------------- depositFunds --------------------------------
/** Sends a deposit to the bank
 *----------------------------------------------------------------------------*/

MASS_FUNCTION void Worker::depositFunds()
{
    pair<int, double> deposit = {myState->myBank, myState->wages};
    ((FinancialMarket *)getPlace())->addBankTransaction((void *)&deposit);
}