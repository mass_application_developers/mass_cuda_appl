#include "bailinout.h"

#include <iostream>
#include <sstream> // ostringstream
#include <vector>
#include <unistd.h>
#include <mass/Mass.h>
#include "FinancialMarket.h"
#include "FinancialMarketState.h"
#include "Bank.h"
#include "BankState.h"
#include "Firm.h"
#include "FirmState.h"
#include "Owner.h"
#include "OwnerState.h"
#include "Worker.h"
#include "WorkerState.h"
#include <mass/Place.h>

// using namespace std;
ostringstream logText;
bool foundBankruptcy(mass::Agents *banks, int numBanks);
namespace bailinout
{
    void runSimulation(ConfigOpts opts, int interval, simviz::RGBFile &vizFile)
    {
        mass::logger::info("Starting MASS CUDA bailinout simulation");
        mass::logger::info("MASS Config: MAX_AGENTS=%d, MAX_NEIGHBORS=%d, MAX_DIMS=%d, N_DESTINATIONS=%d",
                           MAX_AGENTS,
                           MAX_NEIGHBORS,
                           MAX_DIMS,
                           N_DESTINATIONS);

        std::srand(opts.interestRate);
        mass::logger::info("interestRate used for simulation: %d", opts.interestRate);

        int dims = 2;
        int simSpace[] = {opts.sizeX, opts.sizeY};
        int numCells = opts.sizeX * opts.sizeY;
        // I don't exactly know why we need spawnPtNum

        int spawnPtNum = 4;
        int totalBank = opts.init_banks + spawnPtNum;

        // initialize MASS
        mass::Mass::init();

        //--------------------------Set up Simulation Arguments-------------------------
        const int marketHandle = 1, bankHandle = 2, firmHandle = 3, workerHandle = 4;
        // below line, value should be called from simArg.txt
        // that's how mass cpp implemented
        int numWorkers = 2, numFirms = 4, numBanks = 4;
        double initialProductionCost = 35000, initialInterest = 0.8, initialLiquidity = 20000;
        double defaultWage = initialProductionCost * numFirms * 0.1 * 0.8;
        // create cell places to represent our simulation space.
        //---Create the Financial Market large enough to house one firm per element---
        mass::Places *Market = mass::Mass::createPlaces<FinancialMarket, FinancialMarketState>(
            marketHandle, // handle
            NULL,         // args
            0,            // arg size
            dims,         // dimensions
            simSpace      // size of sim space
        );

        pair<int, int> numAgents = {numBanks, numFirms};
        Market->callAll(FinancialMarketFunctions::INIT, (void *)&numAgents, sizeof(pair<int, int>));
        //--------------------------------Create Banks----------------------------------
        pair<double, double> bankArgs = {initialInterest, initialLiquidity};
        int *bankSpawn = new int[totalBank];
        // I am not sure I need spawnInitialBank method.
        spawnInitialBank(opts.sizeX, opts.sizeY, bankSpawn, totalBank);
        // Create Bank Agent
        mass::Agents *bankAgent = mass::Mass::createAgents<Bank, BankState>(
            bankHandle,   // handle
            NULL,         // args
            0,            // arg size
            4,            // number of agent
            marketHandle, // places handle
            numCells,     // max agents
            bankSpawn     // initial starting places
        );
        bankAgent->callAll(BankFunctions::INITIALIZE_BANK, (void *)&bankArgs, sizeof(pair<double, double>));

        //---------------------Have Banks broadcast their information-------------------
        Market->callAll(FinancialMarketFunctions::PREPBANKINFOEXCHANGE, bankAgent, sizeof(mass::Agents));

        // right here, I need to add neighbors for the places
        /*

        exchangeAll (std::vector<int*> *destinations, ….)
        -> exchange data with their neighbors in places
        In PlaceState->
        Place * neighbors[MAX_NEIGHBORS] -> which is array data type
        But I need to pass this to vector
        within the financial class, store place’s neighbor(array)
        Within the main(BailInBailOut), retrieve the neighbors and casting or transform to vector
        Call exchangeAll()

        */

        // how to compute in one dimensional neighborhood.....
        std::vector<int *> neighbors;
        neighbors.push_back(new int[2]{0, 1});   // North
        neighbors.push_back(new int[2]{1, 1});   // North-East
        neighbors.push_back(new int[2]{1, 0});   // East
        neighbors.push_back(new int[2]{1, -1});  // South-East
        neighbors.push_back(new int[2]{0, -1});  // South
        neighbors.push_back(new int[2]{-1, -1}); // South-West
        neighbors.push_back(new int[2]{-1, 0});  // West
        neighbors.push_back(new int[2]{-1, 1});  // North-West
        Market->exchangeAll(&neighbors, FinancialMarketFunctions::EXCHANGEBANKINFO, NULL, 0);

        //--------------------------------Create Firms----------------------------------
        pair<double, double> firmInitVals = {initialProductionCost, initialLiquidity};
        int *firmSpawn = new int[4];
        mass::Agents *firmAgent = mass::Mass::createAgents<Firm, FirmState>(
            firmHandle,
            NULL,
            0,
            4,
            marketHandle,
            numCells,
            firmSpawn);
        firmAgent->callAll(FirmFunctions::FINIT, (void *)&firmInitVals, sizeof(pair<double, double>));
        //--------------------------------Create Workers--------------------------------
        int *workerSpawn = new int[4];
        InitWorker workerInitVals;
        workerInitVals.defaultWage = defaultWage;
        workerInitVals.numWorker = numWorkers;
        workerInitVals.numBanks = numBanks;
        mass::Agents *workerAgent = mass::Mass::createAgents<Worker, WorkerState>(
            workerHandle, // handle
            NULL,         // args
            0,            // arg size
            4,            // number of agent
            marketHandle, // places handle
            numCells,     // max agent
            workerSpawn); // initial starting places
        workerAgent->callAll(WorkerFunctions::WINIT, (void *)&workerInitVals, sizeof(pair<double, int>));

        //-----------------Calculate workforce cost based on workers wages--------------
        firmAgent->callAll(FirmFunctions::FCALCULATEWORKFORCECOST);
        //----------------------------- Simulation Loop --------------------------------

        // Output initial state
        if (interval != 0)
        {
            outputSimSpace(vizFile, Market, opts.sizeX, opts.sizeY);
        }
        printf("Testing");
        //----------------------------- Simulation Loop --------------------------------
        // for (int i = 0; i < opts.rounds; i++)
        // int round = 1;
        for (int round = 1; !foundBankruptcy(bankAgent, numBanks) && round <= 10; round++)
        {
            // need to call agent and place here.
            // Agents acts in Round
            firmAgent->callAll(FirmFunctions::ACT);
            workerAgent->callAll(WorkerFunctions::WACT);
            bankAgent->callAll(BankFunctions::ACT_ROUND);
            // Send Transaction to Banks
            Market->callAll(FinancialMarketFunctions::PREPFORINCOMINGTRANSACTIONS, firmAgent, sizeof(mass::Agents));
            Market->exchangeAll(&neighbors, FinancialMarketFunctions::EXCHANGEOUTGOINGTRANSACTIONS, NULL, 0);
            Market->callAll(FinancialMarketFunctions::MANAGEINCOMINGTRANSACTIONS);
            bankAgent->callAll(BankFunctions::READROUNDTRANSACTIONS);

            bankAgent->callAll(BankFunctions::REPORTROUNDVALUES);
            // Prepare for next round

            Market->callAll(FinancialMarketFunctions::PREPBANKINFOEXCHANGE);
            Market->exchangeAll(&neighbors, FinancialMarketFunctions::EXCHANGEBANKINFO, NULL, 0);
            Market->callAll(FinancialMarketFunctions::RESETROUNDVALUES);

            // Output viz frame each 'interval' tick.
            if (interval != 0 && (round % interval == 0 || round == opts.rounds - 1))
            {
                outputSimSpace(vizFile, Market, opts.sizeX, opts.sizeY);
            }
        }
        printf("\nTOTAL BANK: %d\n", bankAgent->getNumAgents());
        // implement simulation...
        mass::logger::info("simulation complete, shutting down...");

        mass::Mass::finish();
    }

    ConfigOpts parseSimConfig(po::variables_map vm)
    {
        ConfigOpts opts = ConfigOpts{
            vm["interestRate"].as<int>(),
            vm["sizeX"].as<int>(),
            vm["sizeY"].as<int>(),
            vm["rounds"].as<int>(),
            vm["init_banks"].as<int>(),
            vm["liquidity"].as<double>(),
        };

        // If interestRate is unset, set to current time in milli-seconds since
        // epoch
        if (opts.interestRate == -1)
        {
            std::time_t _time = std::time(NULL);
            opts.interestRate = _time;
        }

        mass::logger::debug("Simulation Config: {interestRate=%d, sizeX=%d, sizeY=%d, rounds=%d, init_banks=%d"
                            "liquidity=%d}",
                            opts.interestRate,
                            opts.sizeX,
                            opts.sizeY,
                            opts.rounds,
                            opts.init_banks,
                            opts.liquidity);

        return opts;
    }

    void outputSimSpace(simviz::RGBFile &vizFile, mass::Places *places, int sizeX, int sizeY)
    {
        unsigned char *spaceColor = new unsigned char[3]{255, 255, 255}; // white
        unsigned char *bank = new unsigned char[3]{0, 0, 255};           // blue
        unsigned char *worker = new unsigned char[3]{128, 0, 128};       // purple
        unsigned char *firm = new unsigned char[3]{0, 255, 0};           // fluorescent color
        unsigned char *owner = new unsigned char[3]{255, 0, 0};          // red

        mass::Place **cells = places->getElements();

        int indices[2];
        for (int row = 0; row < sizeY * 2; row++)
        {
            indices[0] = row / 2;
            for (int col = 0; col < sizeX * 2; col++)
            {
                indices[1] = col / 2;
                int rmi = places->getRowMajorIdx(indices);
                if (rmi != (indices[0] % sizeX) * sizeY + indices[1])
                {
                    mass::logger::error("Row Major Index is incorrect: [%d][%d] != %d",
                                        row, col, rmi);

                    continue;
                }

                FinancialMarket *place = (FinancialMarket *)cells[rmi];

                unsigned char *color = spaceColor;

                if (row % 2 == 0)
                {
                    if (col % 2 == 0)
                    { // top left (Bank)
                        if (place->getBank())
                        {
                            color = bank;
                        }
                    }
                    else
                    { // top right (Firm)
                        if (place->getFirm())
                        {
                            color = firm;
                        }
                    }
                }
                else
                {
                    if (col % 2 == 0)
                    { // bottom left (Worker)
                        if (place->getWorker())
                        {
                            color = worker;
                        }
                    }
                    else
                    {
                        // bottom right (Owner)
                        if (place->getOwner())
                        {
                            color = owner;
                        }
                    }
                }

                vizFile.write((char *)color, simviz::NumRGBBytes);
            }
        }
    }

    void spawnInitialBank(int sizeX, int sizeY, int *bankSpawn, int totalBank)
    {
        int quadrant = sizeX / 4;
        // set spawner locations on centre of each quadrant
        bankSpawn[0] = quadrant - 1 + sizeX * (quadrant - 1);         // Top Left
        bankSpawn[1] = sizeX - quadrant + sizeX * (quadrant - 1);     // Top Right
        bankSpawn[2] = quadrant - 1 + sizeX * (sizeX - quadrant);     // Bottom Left
        bankSpawn[3] = sizeX - quadrant + sizeX * (sizeX - quadrant); // Bottom Right
    }

    int *generateRandVals(int n)
    {
        int *randVals = new int[n];
        for (int i = 0; i < n; i++)
        {
            randVals[i] = rand();
        }

        return randVals;
    }

}
bool foundBankruptcy(mass::Agents *banks, int numBanks)
{
    bool bankruptcyFound = false;
    banks->callAll(BankFunctions::ISBANKRUPT);
    Bank *bnk = (Bank *)banks;

    for (int i = 0; i < numBanks; i++)
    {
        BankState *state = bnk[i].getState();
        if ((state->liquidity + state->totalDebt) < 0)
        {
            bankruptcyFound = true; // Detected a bankruptcy
            logText.str("");
            logText << "Bank " << i << " bankrupt!";
            // MASS_base::log(logText.str());
            /*eval only*/ bankruptcyFound = false; // Keeps running until maxRounds hit
            break;
        }
    }

    return bankruptcyFound;
}