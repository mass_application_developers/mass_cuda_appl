#ifndef ENVIRONMENT_PLACE_H
#define ENVIRONMENT_PLACE_H

#include <mass/Place.h>

#include "Bank.h"
#include "FinancialMarketState.h"
class Bank;

enum FinancialMarketFunctions : int
{
    INIT = 0,
    // INIT_RAND = 15,
    ADDBANK = 1,
    ADDWORKER = 2,
    CALCULATEWORKFORCECOST = 3,
    PREPBANKINFOEXCHANGE = 4,
    EXCHANGEBANKINFO = 5,
    UPDATEBANKREGISTRY = 6,
    UPDATEBANKINFO = 7,
    PREPFORINCOMINGTRANSACTIONS = 8,
    EXCHANGEOUTGOINGTRANSACTIONS = 9,
    MANAGEINCOMINGTRANSACTIONS = 10,
    GETINCOMINGTRANSACTIONAMT = 11,
    RESETROUNDVALUES = 12,
    SELECTFROMBANKS = 13,
    ADDBANKTRANSACTION = 14,
    FIX_NEIGHBORS = 16,
    // Debug functions
    PRINT_STATE,
};

// An EnvironmentPlace is a cell in the Tuberculosis simulation space.
class FinancialMarket : public mass::Place
{
public:
    MASS_FUNCTION FinancialMarket(mass::PlaceState *state, void *arg);
    MASS_FUNCTION ~FinancialMarket();

    // Calls the method with the given arguments specified by the given function ID.
    MASS_FUNCTION virtual void callMethod(int functionId, void *arg = NULL);

    //---------------------------------- init --------------------------------------
    /** Initializes default values.
     *  @param nAgents - a pair<int,int> where the first item is the number of Bank
     *         agents, and the second item is the number of Firm agents.
     *----------------------------------------------------------------------------*/

    MASS_FUNCTION void init(void *nAgents);

    // pass in generated random values from host to Device
    // MASS_FUNCTION void initRand(int randVal);

    MASS_FUNCTION void fixNeighbors();
    //---------------------------------- addBank -----------------------------------
    /** Stores information on the residing Bank agent.
     *  @param bankInfo - a BankInfo* storing the information of the residing bank.
     *----------------------------------------------------------------------------*/

    MASS_FUNCTION void addBank(void *bankInfo);

    //------------------------------ updateBankInfo --------------------------------
    /** Updates the residing Bank's information to reflect any changes to its values
     *  during the last round.
     *  @param updatedInfo - a BankInfo* storing the info of the residing bank.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void updateBankInfo(void *updatedInfo);
    //---------------------------- prepBankInfoExchange ----------------------------
    /** Prepares each Place to receive information on the Banks in the simulation.
     *  Must be called before exchangeBankInfo().
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void prepBankInfoExchange(void *agent);
    MASS_FUNCTION void cleanNeighbors();
    MASS_FUNCTION void addNeighbor(int *index, int dimension);
    //------------------------------ exchangeBankInfo ------------------------------
    /** Returns a BankInfo object containing all information for the residing bank.
     *  prepBankInfoExchange() must be called before this function.
     *  @return - a BankInfo* containing the information of the residing bank.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void *exchangeBankInfo();
    //--------------------------- updateBankRegistry -------------------------------
    /** Updates the local bank registry that stores information on all the Banks in
     *  the simulation. exchangeBankInfo() must be called before this function.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void updateBankRegistry();
    //------------------------------ selectFromKBanks ------------------------------
    /** Chooses from k banks the bank with the lowest interest rate that can afford
     *  to provide the loan/
     *  @param arg - a BankSelectionArgs* where k is the number of banks to choose
     *         from, minAcceptableInterest is the lowest interest rate that is
     *         acceptable, and amountNeeded is the amount needed from the bank.
     *  @return - Returns a BankInfo* storing the information on the bank selected.
     *         if no bank fit the criteria, nullptr is returned.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void selectFromBanks();
    //-------------------------- addBankTransaction --------------------------------
    /** Adds the monetary value of a transaction for a specified bank. A loan will
     *  be a negative value, decreasing the running total, and a deposit will be
     *  a positive value, increasing the running total.
     *  @param transactionInfo - a pair<int,double>* where the first item is the
     *         bankId and the second item is the transaction amount.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void addBankTransaction(void *transaction);
    //------------------------ prepForIncomingTransactions -------------------------
    /** Prepares all Financial Market elements with a residing Bank agent to send
     *  the id of the bank as an integer, and receive the total value of
     *  transactions accumulated from all other Financial Market places during the
     *  round. This function must precede exchangeOutgoingTransactions.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void prepForIncomingTransactions(void *agent);
    //------------------------ exchangeOutgoingTransactions ------------------------
    /** Receives a bankId and returns the total sum of all transactions accumulated
     *  for that bank during the round. Must be preceded by the function
     *  prepForIncomingTransactions().
     *  @param givenBankId - an int representing the bank's id.
     *  @return - a double representing the value of the transactions.
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void exchangeOutgoingTransactions();
    //-------------------------- manageIncomingTransactions ------------------------
    /** Has any Financial Market with a residing Bank sort through its inMessages
     *  and total the transactions received from other place elements. Must be
     *  preceded by exchangeOutgoingTransactions().
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void manageIncomingTransactions();
    //------------------------- getIncomingTransactionAmt --------------------------
    /** Returns the sum of all incoming transactions totalled in the
     *  manageIncomingTransactions() function.
     *  @return - a double storing the value of all incoming transactions
     *----------------------------------------------------------------------------*/
    MASS_FUNCTION void *getIncomingTransactionAmt();
    //---------------------------- resetRoundValues --------------------------------
    // Resets the round values to prepare for next round
    MASS_FUNCTION void resetRoundValues();

    MASS_FUNCTION void addWorker(void *workerInfo);

    MASS_FUNCTION void *calculateWorkforceCost();
    // Bank
    // return whether there is a bank occupying the FinancialMarket
    MASS_FUNCTION bool getBank();

    // sets the occupancy of bank at the financialMarket.
    MASS_FUNCTION void setBank(bool newBank);

    // Firm
    MASS_FUNCTION bool getFirm();

    MASS_FUNCTION void setFirm(bool newFirm);

    // Worker
    MASS_FUNCTION bool getWorker();

    MASS_FUNCTION void setWorker(bool newWorker);

    // Owner
    MASS_FUNCTION bool getOwner();

    MASS_FUNCTION void setOwner(bool newOwner);

private:
    // the state of the FinancialMarket
    FinancialMarketState *myState;
};

#endif
