#ifndef ENVIRONMENT_PLACE_STATE_H
#define ENVIRONMENT_PLACE_STATE_H

#include "../src/PlaceState.h"
#include <iostream>
#include <unordered_map>
using namespace std;

#include "Bank.h"

class FinancialMarket;
class Bank;

struct BankInfo
{
    double interestRate;
    double liquidity;
    int bankId;
    int linearIndex;
};

struct BankSelectionArgs
{
    double minAcceptableInterest;
    double amountNeeded;
    int k;
};

struct Loan
{
    double amount;
    double interestRate;
    int loaningBankId;
};
// The state for an EnvironmentPlace in the Tuberculosis simulation space.
class FinancialMarketState : public mass::PlaceState
{
public:
    BankInfo *bankinfo;

    BankSelectionArgs *bankselectionargs;

    Loan *loan;

    int residingBank;

    int numBanks;

    int numFirms;

    double incomingTransactions;

    unordered_map<int, BankInfo *> bankRegistry;

    unordered_map<int, double> localWorkerWages;

    unordered_map<int, double> outgoingTransactions;

    // Random value (randomized on init)
    int randState;

    // whether the bank exists
    bool bank;
    // The bank state.
    int bankState;

    // Firm
    bool firm;

    // Worker
    bool worker;

    // Owner
    bool owner;
};

#endif
