/*==============================================================================
    Firm Class
================================================================================
    The Firm Class is an Agent in the Bail-in/Bail-out simulation. Each
  round, it calculates its costs and profits, pays its workers and owner,
  pays off loans, and determines if it needs a loan if it goes into debt.
  The loan may come from its owner, or a bank. When the Firm takes out a
  loan, it sends a negative value to the bank. If it makes a payment, it
  sends a positive value to the bank.
==============================================================================*/

#ifndef FIRMSTATE_H
#define FIRMSTATE_H
#include "mass/AgentState.h"
#include "Owner.h"
#include "FinancialMarketState.h"

class FirmState : public mass::AgentState
{
public:
    pair<double, double> resetVals; // Stores default {cost, liq} for reset
    double productionCost;          // Current round's production costs
    double liquidity;               // Ongoing liquidity
    double workforceCost;           // Cost to pay workers
    double profit;                  // Current round's profits
    int firmId;                     // Unique identified for firm
    Owner *owner;                   // Pointer to owner object
    vector<Loan> debt;              // Outstanding Loans
};
#endif