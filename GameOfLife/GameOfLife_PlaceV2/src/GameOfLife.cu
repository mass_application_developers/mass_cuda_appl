#include <iostream>
#include <sstream> // ostringstream
#include <vector>
#include <stdlib.h> // random

#include "GameOfLife.h"
#include "Life.h"
#include "mass/Mass.h"
#include "mass/Logger.h"
#include "mass/CudaEventTimer.h"
#include "Timer.h"

using namespace std;
using namespace mass;
using namespace logger;

#define USE_TIMER 1

GameOfLife::GameOfLife() {}
GameOfLife::~GameOfLife() {}

void GameOfLife::runMassSim(int generations, int size)
{
	logger::debug("Starting MASS CUDA simulation with MAX_NEIGHBORS %d", MAX_NEIGHBORS);

	logger::debug("Running MASS CUDA simulation for " + to_string(generations) + " generations, with size of " + to_string(size));

	int nDims = 2;
	int placesSize[] = {size, size};

	// Atart a timer which records the CPU clock time
	Timer timer;
	timer.start();

#ifdef USE_TIMER
	// Start a CUDA Event timer for the overall simulation
	std::unique_ptr<mass::CudaEventTimer> simulationTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
	simulationTimer->startTimer();
#endif

	// Initialize MASS
	Mass::init();

#ifdef USE_TIMER
	// Start a CUDA Event timer for the initialization
	std::unique_ptr<mass::CudaEventTimer> initTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
	initTimer->startTimer();
#endif

	// Initialize places
	mass::Places *places = mass::Mass::createPlaces<Life>(0, nDims, placesSize, mass::Place::MemoryOrder::ROW_MAJOR);

	// Initialize neighbors
	vector<int *> neighbors;
	int left_top[2] = {-1, -1};
	int top[2] = {-1, 0};
	int right_top[2] = {-1, 1};
	int left[2] = {0, -1};
	int right[2] = {0, 1};
	int left_bottom[2] = {1, -1};
	int bottom[2] = {1, 0};
	int right_bottom[2] = {1, 1};
	neighbors.push_back(left_top);
	neighbors.push_back(top);
	neighbors.push_back(right_top);
	neighbors.push_back(left);
	neighbors.push_back(right);
	neighbors.push_back(left_bottom);
	neighbors.push_back(bottom);
	neighbors.push_back(right_bottom);

	// Set the neighbors for each place
	places->exchangeAll(&neighbors);

	// Add the attribute health to the Place
	places->setAttribute<int>(Life::HEALTH, 1, 0);
	places->finalizeAttributes();

	// Initialize Life cells' health
	places->callAll(Life::INIT_HEALTH);

#ifdef USE_TIMER
	// Stop the CUDA Event timer for the initialization
	initTimer->stopTimer();
#endif
	mass::logger::info("Initialization CPU time: %ld ms", timer.lap() / 1000);

#ifdef USE_TIMER
	// Start a CUDA Event timer for the time after all setup is complete
	std::unique_ptr<mass::CudaEventTimer> afterSettingUpTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
	afterSettingUpTimer->startTimer();
#endif

	// Avarage step time
	float avgStepTime = 0.0;

	for (int i = 0; i < generations; i++)
	{
#ifdef USE_TIMER
		// Start a CUDA Event timer for each step (exchangeAll call)
		std::unique_ptr<mass::CudaEventTimer> stepTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
		stepTimer->startTimer();
#endif
		places->callAll(Life::COMPUTE_NEXT_STATE);

#ifdef USE_TIMER
		// Stop the CUDA Event timer for each step (exchangeAll call)
		stepTimer->stopTimer();
		// logger::info("Step %d time %f ms", i, stepTimer->getElapsedTime());
		avgStepTime += stepTimer->getElapsedTime();
#endif
	}

#ifdef USE_TIMER
	// End of simulation, stop the CUDA Event timer for all timers
	simulationTimer->stopTimer();
	afterSettingUpTimer->stopTimer();
	logger::info("CUDA total time %d ms", simulationTimer->getElapsedTime());
	logger::info("CUDA time for initialization %d ms", initTimer->getElapsedTime());
	logger::info("CUDA time for each step %f ms", avgStepTime / generations);
	logger::info("CUDA time after setting up %d ms", afterSettingUpTimer->getElapsedTime());
#endif

	// displayResults(places, generations, size, result);
	// logger::Logger::print("MASS time %d\n\n",timer.lap());
	logger::info("MASS time %d ms", timer.lap() / 1000);

	// close library and clean up
	Mass::finish();
}
