#include <curand_kernel.h> // rand
#include "Life.h"
#include "mass/settings.h" // MAX_NEIGHBORS

using namespace std;
using namespace mass;

#define SIZE 2000

__device__ void Life::callMethod(int functionId, void *argument)
{
	switch (functionId)
	{
	case INIT_HEALTH:
		initializeHealth();
		break;
	case COMPUTE_NEXT_STATE:
		computeNextState();
		break;
	case PRINT_HEALTH:
		printHealth();
		break;
	default:
		break;
	}
}

__device__ void Life::initializeHealth()
{
	// Get current index
	int index = getIndex();

	// Init cuda random number generator
	curandState state;
	unsigned long seed = 0;
	curand_init(seed, index, 0, &state);
	// Randomly initialize health to 0 or 1
	int health = curand(&state) % 2;

	// Get the attribute of health
	int *d_health = getAttribute<int>(index, ATTRIBUTE::HEALTH, 1);

	// Set the health
	d_health[0] = health;
}

__device__ void Life::computeNextState()
{
	// Get current index
	int index = getIndex();
	// Get attribute NEIGHBORS
	int *neighbors = getAttribute<int>(index, PlacePreDefinedAttr::NEIGHBORS, MAX_NEIGHBORS);
	// Get the attribute of health
	int *health = getAttribute<int>(index, ATTRIBUTE::HEALTH, 1);

	unsigned int aliveNeighbors = 0;

	// Count alive neighbors
	for (int i = 0; i < MAX_NEIGHBORS; i++)
	{
		if (neighbors[i] != -1)
		{
			// Get the health of the neighbor
			int *health = getAttribute<int>(neighbors[i], ATTRIBUTE::HEALTH, 1);
			// If the neighbor is alive, increment the counter
			if (*health == 1)
			{
				aliveNeighbors++;
			}
		}
	}

	// If current cell is alive
	if (*health >= 1)
	{
		// If alive neighbors are less than 2 or more than 3, then die
		if (aliveNeighbors < 2 || aliveNeighbors > 3)
		{
			*health = 0;
		}
	}
	// If current cell is dead
	else
	{
		// If alive neighbors are exactly 3, then live
		if (aliveNeighbors == 3)
		{
			*health = 1;
		}
	}
}

__device__ void Life::printHealth()
{
	int index = getIndex();
	int *health = getAttribute<int>(index, ATTRIBUTE::HEALTH, 1);
	printf("Health of cell %d is %d\n", index, *health);
}
