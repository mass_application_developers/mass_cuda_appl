#ifndef GAMEOFLIFE_H_
#define GAMEOFLIFE_H_

#include "Life.h"
#include "mass/Places.h"
#include <vector>
using namespace std;
// A GameOfLife is the simulation that simulates Conway's Game of Life using
// the MASS CUDA library.
class GameOfLife {
public:
	GameOfLife();
	virtual ~GameOfLife();

	// Executes the game of life simulation using the given grid size and a
	// given number of generations.
	void runMassSim(int generations, int size);
};

#endif
