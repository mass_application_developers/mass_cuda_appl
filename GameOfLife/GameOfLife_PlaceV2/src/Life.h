#ifndef LIFE_H
#define LIFE_H

#include "mass/Place.h"

// A Life is cell in the simulation grid space of Conway's Game of Life.
class Life: public mass::Place {
public:

	enum FUNCTION{
		INIT_HEALTH,
		COMPUTE_NEXT_STATE,
		PRINT_HEALTH
	};

	enum ATTRIBUTE{
		HEALTH
	};

	MASS_FUNCTION Life(int index) : Place(index) {}
  	MASS_FUNCTION ~Life() {}

	// Calls the method specified by the given function ID and parameters
	__device__ virtual void callMethod(int functionId, void *arg = NULL);

private:
	__device__ void initializeHealth();

	// Computes the next generation of life cells
	__device__ void computeNextState();

	__device__ void printHealth();
};
#endif
