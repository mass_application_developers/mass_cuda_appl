#include "GameOfLife.h"
#include <mass/Mass.h>
#include <gtest/gtest.h>

using namespace std;

TEST(GameOfLife, test1){
    size_t size = 2500;
    GameOfLife gol;
    gol.runMassSim(250, size);
}

int main(int argc, char **argv)
{
    mass::logger::setLogLevel(boost::log::trivial::info);
    testing::InitGoogleTest(&argc, argv);
    testing::FLAGS_gtest_filter = "GameOfLife.test1";

    return RUN_ALL_TESTS();
}