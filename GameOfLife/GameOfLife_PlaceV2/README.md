## Steps to run MASS simulation:
1. `make develop`
2. `make build`
3. `./bin/GameOfLife`

## Steps to run test:
1. `make test`