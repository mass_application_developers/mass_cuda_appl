#include <iostream>	// cout
#include <stdlib.h>	// rand
#include "Timer.h"	// timer
using namespace std;

#define H2D cudaMemcpyHostToDevice
#define D2H cudaMemcpyDeviceToHost

// Used to store the current Life health status and the health status
// of the next generation of each cell in the Game of Life grid
struct Life {
	bool mod0, mod1;
};

// Pre: Given a cell's current state and its neighbors
// Post: Returns whether the cell will be dead or alive in the
//	next generation
__global__ void computeDeadOrAlive(Life** grid, int size, int tick) {
	int j = threadIdx.y, i = threadIdx.x, numAlive = 0;
	for (int vert = -1; vert <= 1; vert++) {
		for (int hori = -1; hori <= 1; hori++) {
			if (vert != 0 || hori != 0) {
				if (i + vert != -1 && i + vert != size && j + hori != -1 && j + hori != size) {
					numAlive += tick % 2 == 0 ? grid[i + vert][j + hori].mod0 : grid[i + vert][j + hori].mod1;
				}
			}
		}
	}
	if (tick % 2 == 0) {
		grid[i][j].mod1 = grid[i][j].mod0 ? numAlive == 2 || numAlive == 3 : numAlive == 3;
	} else {
		grid[i][j].mod0 = grid[i][j].mod1 ? numAlive == 2 || numAlive == 3 : numAlive == 3;
	}
}

// Pre: Given three integers, numTurns, sizeX, and sizeY
// Post: Runs a Game of Life simulation for the given amount of generations
//	on the given sizeX by sizeY two-dimensional grid
void runDeviceSim(int generations, int size) {
	int i, j, tick;
	Life **host_grid = new Life*[size];
	for (i = 0; i < size; i++) {
		host_grid[i] = new Life[size];
	}

	// Initialize host Game of Life grid
	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			host_grid[i][j].mod0 = rand() % 2 == 0;
		}
	}

	// Allocate device grid and fill with initialized values
	Life** device_grid;
	cudaMalloc((void**) &device_grid, size*sizeof(Life*));
	Life* device_grid_rows[size];
	for (i = 0; i < size; i++) {
		cudaMalloc((void**) &device_grid_rows[i], size*sizeof(Life));
		cudaMemcpy(device_grid_rows[i], host_grid[i], size*sizeof(Life), H2D);
	}
	cudaMemcpy(device_grid, device_grid_rows, size*sizeof(Life*), H2D);

	// Iterate through Game of Life generations
	dim3 block(size, size);
	for (tick = 0; tick < generations; tick++) {
		computeDeadOrAlive<<<1, block>>>(device_grid, size, tick);
	}

	// Copy Device Memory to Host to Display Health Status
	for (i = 0; i < size; i++) {
		cudaMemcpy(host_grid[i], device_grid_rows[i], size*sizeof(Life), D2H);
	}
	cout << "Health Status at Size " << size << " after " << generations << " Generations" << endl;
	for (i = 0; i < size; i++) {
		for (j = 0; j < size; j++) {
			cout << host_grid[i][j].mod0 << "  ";
		}
		cout << endl;
	}

	// Clear Device Memory and Heap Memory
	for (i = 0; i < size; i++) {
		cudaFree(device_grid_rows[i]);
		delete[] host_grid[i];
	}
	cudaFree(device_grid);
	delete[] host_grid;
}

// Pre:
// Post: Measures the execution time elapsed for the Game of Life
//  simulation for various grid sizes
int main() {
	const int numSimSizes = 5, generations = 10;
	const int simSizes[numSimSizes] = {2, 4, 8, 16, 32};

	Timer timer;
	cout << "Game of Life (GPU implementation)" << endl;

	cout << "Starting Simulation" << endl;
	for (int i = 0; i < numSimSizes; i++) {
		timer.start();
		runDeviceSim(generations, simSizes[i]);
		cout << "Time: " << timer.lap() << "ms" << endl << endl;
	}
	cout << "Ending Simulation" << endl;

	return 0;
}
