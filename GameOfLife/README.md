# Game of Life
Updated: 2024-04-13

## Description
This is a simple implementation of Conway's Game of Life in MASS CUDA. The latest version of this code can be found at `./GameOfLife_PlaceV2`.