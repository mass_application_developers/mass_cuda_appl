#ifndef LIFE_H
#define LIFE_H

#include "mass/Place.h"
#include "LifeState.h"

// A Life is cell in the simulation grid space of Conway's Game of Life.
class Life: public mass::Place {
public:
	// The function ID for initializing the health of Life cells.
	const static int INIT_HEALTH = 0;

	// The function ID for computing the next generation of Life cells.
	const static int COMPUTE_NEXT_GEN = 1;

	MASS_FUNCTION Life(mass::PlaceState *state, void *argument);
  MASS_FUNCTION ~Life();

	// Calls the method specified by the given function ID and parameters
	MASS_FUNCTION virtual void callMethod(int functionId, void *arg = NULL);

	// Returns whether the Life cell is dead or alive.
	MASS_FUNCTION bool getHealth();

private:
	// The current state of the Life.
	LifeState* myState;

	// Initializes the Life's initial health based on the current position in the
	// given boolean health array.
	MASS_FUNCTION void initializeHealth(bool* healthArray);

	// Computes the next generation of life cells based on the given step in the
	// simulation.
	MASS_FUNCTION void computeDeadOrAlive(int tick);
};
#endif
