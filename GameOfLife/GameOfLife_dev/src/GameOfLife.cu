#include <iostream>
#include <sstream> // ostringstream
#include <vector>
#include <stdlib.h> // random

#include "GameOfLife.h"
#include "Life.h"
#include "mass/Mass.h"
#include "mass/Logger.h"
#include "Timer.h"

using namespace std;
using namespace mass;
using namespace logger;

#define USE_TIMER 1

GameOfLife::GameOfLife() {}
GameOfLife::~GameOfLife() {}

void GameOfLife::runMassSim(int generations, int size, vector<int> &result)
{
	logger::debug("Starting MASS CUDA simulation\n");

	logger::debug("Running MASS CUDA simulation for " + to_string(generations) + " generations, with size of " + to_string(size));

	int nDims = 2;
	int placesSize[] = {size, size};
	int totalLifeCells = size * size;

	// start a timer which records the CPU clock time
	Timer timer;
	timer.start();

#ifdef USE_TIMER
	// Start a CUDA Event timer for the overall simulation
	std::unique_ptr<mass::CudaEventTimer> simulationTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
	simulationTimer->startTimer();
#endif

	// start a process at each computing node
	Mass::init();
	logger::debug("Finished Mass::init\n");

#ifdef USE_TIMER
	// Start a CUDA Event timer for the time after MASS init
	std::unique_ptr<mass::CudaEventTimer> afterInitTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
	afterInitTimer->startTimer();
#endif

	// initialization parameters
	bool initHealth[totalLifeCells];
	for (int i = 0; i < totalLifeCells; i++)
	{
		initHealth[i] = rand() % 2 == 0;
	}

#ifdef USE_TIMER
	// Start a CUDA Event timer for the createPlaces call
	std::unique_ptr<mass::CudaEventTimer> createPlacesTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
	createPlacesTimer->startTimer();
#endif

	// initialize places
	mass::Places *places = Mass::createPlaces<Life, LifeState>(0, NULL, 0, nDims, placesSize);
	logger::debug("Finished Mass::createPlaces\n");

#ifdef USE_TIMER
	// Stop the CUDA Event timer for the createPlaces call
	createPlacesTimer->stopTimer();

	// Start a CUDA Event timer for the init callAll call
	std::unique_ptr<mass::CudaEventTimer> initCallAllTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
	initCallAllTimer->startTimer();
#endif

	// initialize life cells
	places->callAll(Life::INIT_HEALTH, initHealth, totalLifeCells * sizeof(bool));

#ifdef USE_TIMER
	// Stop the CUDA Event timer for the init callAll call
	initCallAllTimer->stopTimer();
#endif

	// create neighborhood
	vector<int *> neighbors;
	int northwest[] = {-1, 1};
	neighbors.push_back(northwest);
	int north[] = {0, 1};
	neighbors.push_back(north);
	int northeast[] = {1, 1};
	neighbors.push_back(northeast);
	int east[] = {1, 0};
	neighbors.push_back(east);
	int southeast[] = {1, -1};
	neighbors.push_back(southeast);
	int south[] = {0, -1};
	neighbors.push_back(south);
	int southwest[] = {-1, -1};
	neighbors.push_back(southwest);
	int west[] = {-1, 0};
	neighbors.push_back(west);

	logger::info("Initialization time: %ld ms", timer.lap() / 1000);

#ifdef USE_TIMER
	// Start a CUDA Event timer for the time after all setup is complete
	std::unique_ptr<mass::CudaEventTimer> afterSettingUpTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
	afterSettingUpTimer->startTimer();
#endif

	// Average time for each step
	float avgTime = 0.0;

	int tick = 0;
	places->exchangeAll(&neighbors, Life::COMPUTE_NEXT_GEN, &tick, sizeof(int));
	for (tick = 1; tick < generations; tick++)
	{
#ifdef USE_TIMER
		// Start a CUDA Event timer for each step (exchangeAll call)
		std::unique_ptr<mass::CudaEventTimer> stepTimer = std::unique_ptr<mass::CudaEventTimer>(new mass::CudaEventTimer());
		stepTimer->startTimer();
#endif

		places->callAll(Life::COMPUTE_NEXT_GEN, &tick, sizeof(int));

#ifdef USE_TIMER
		// Stop the CUDA Event timer for each step (exchangeAll call)
		stepTimer->stopTimer();
		// logger::info("Step %d time %d ms", tick, stepTimer->getElapsedTime());
		avgTime += stepTimer->getElapsedTime();
#endif
	}

#ifdef USE_TIMER
	// End of simulation, stop the CUDA Event timer for all timers
	simulationTimer->stopTimer();
	afterInitTimer->stopTimer();
	afterSettingUpTimer->stopTimer();
	logger::info("CUDA total time %d ms", simulationTimer->getElapsedTime());
	logger::info("CUDA time per step %d ms", avgTime / (generations - 1));
	logger::info("CUDA time after init %d ms", afterInitTimer->getElapsedTime());
	logger::info("CUDA time after setting up %d ms", afterSettingUpTimer->getElapsedTime());
	// logger::info("CUDA time for createPlaces %d ms", createPlacesTimer->getElapsedTime());
	// logger::info("CUDA time for init CallAll %d ms", initCallAllTimer->getElapsedTime());
#endif

	// displayResults(places, generations, size, result);
	// logger::Logger::print("MASS time %d\n\n",timer.lap());
	logger::info("MASS time %d ms", timer.lap() / 1000);

	// close library and clean up
	Mass::finish();
}

void GameOfLife::displayResults(mass::Places *places, int generations, int size, vector<int> &result)
{
	logger::debug("Entering GameOfLife::displayResults");
	mass::Place **grid = places->getElements();
	int coordinates[2];
	// vector<int> res;
	logger::debug("Entering loops to display results");
	for (int row = 0; row < size; row++)
	{
		coordinates[0] = row;
		logger::debug("Entering loop to print out cols");
		for (int col = 0; col < size; col++)
		{
			coordinates[1] = col;
			int rmi = places->getRowMajorIdx(coordinates);
			if (rmi != (row % size) * size + col)
			{
				throw("Row Major Index is incorrect: [%d][%d] != %d", row, col, rmi);
			}
			bool currentHealth = ((Life *)grid[rmi])->getHealth();
			int tempStr = currentHealth ? 1 : 0;
			result.push_back(tempStr);
		}
		logger::debug("Finished cols");
	}
	// return res;
	// logger::sprintf(ss.str());
}
