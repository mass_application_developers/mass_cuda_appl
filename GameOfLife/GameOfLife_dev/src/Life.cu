#include <curand_kernel.h>	// rand
#include "Life.h"
// #include "../src/settings.h"	// MAX_NEIGHBORS
#include "mass/settings.h" // MAX_NEIGHBORS

using namespace std;
using namespace mass;

MASS_FUNCTION Life::Life(PlaceState* state, void *argument) : Place(state, argument) {
	myState = (LifeState*) state;
}

MASS_FUNCTION Life::~Life() {}

MASS_FUNCTION void Life::callMethod(int functionId, void *argument) {
	switch (functionId) {
		case INIT_HEALTH:
			initializeHealth((bool*)argument);
			break;
		case COMPUTE_NEXT_GEN:
			computeDeadOrAlive(*(int*)argument);
			break;
	}
}

MASS_FUNCTION bool Life::getHealth() {
	return myState->mod0;
}

MASS_FUNCTION void Life::initializeHealth(bool* healthArray) {
	myState->mod0 = healthArray[getIndex()];
}

// Pre: Given an integer, tick
// Post: Calculates the life state for the next generation
MASS_FUNCTION void Life::computeDeadOrAlive(int tick) {
	int numAlive = 0;
	int tickRemainderIsZero = tick % 2 == 0;

	for (int neighbor = 0; neighbor < MAX_NEIGHBORS; neighbor++) {
		Life* cell = (Life*)(myState->neighbors[neighbor]);
		if (cell != NULL) {
			numAlive += tickRemainderIsZero ? cell->myState->mod0 : cell->myState->mod1;
		}
	}

	if (tickRemainderIsZero) {	// use mod0 state, store in mod1
		myState->mod1 = myState->mod0 ? numAlive == 2 || numAlive == 3 : numAlive == 3;
	} else {
		myState->mod0 = myState->mod1 ? numAlive == 2 || numAlive == 3 : numAlive == 3;
	}
}
