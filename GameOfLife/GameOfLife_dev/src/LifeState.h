#ifndef LIFESTATE_H
#define LIFESTATE_H

#include "mass/PlaceState.h"
#include <stdlib.h>

// A LifeState is the state of health for a Life cell.
class LifeState: public mass::PlaceState {
public:
	// The data used if the current simulation step is even
	bool mod0 = rand() % 2 == 0;

	// The data used if the current simulation step is odd
	bool mod1 = rand() % 2 == 0;
};

#endif
