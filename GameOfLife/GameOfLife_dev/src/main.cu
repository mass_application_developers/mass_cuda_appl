#include "mass/Logger.h"
#include "GameOfLife.h"

using namespace std;
using namespace mass;

// Executes Conway's Game of Life using MASS CUDA using various simulation sizes
// and number of simulation generations.
int main() {
	// TODO: add file logging.
	// logger::setLogFile("GameOfLife.txt");
	// const int simSizes[] = {2, 4, 8, 16, 32};
	// const int numSimSizes = 5, generations = 10;

	// logger::info("Game Of Life (MASS Implementation)");
	// GameOfLife gameoflife;
	// for (int i = 0; i < numSimSizes; i++) {
	// 	gameoflife.runMassSim(generations, simSizes[i], vector<int>());
	// }
	// logger::info("Ending Simulation");

	return 0;
}
