#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <sstream>     // ostringstream
#include "Timer.h"

#include <gtest/gtest.h>
#include <mass/Mass.h>
#include "GameOfLife.h"


using namespace std;

vector<vector<int>> storeResult;
const int generations = 250;
GameOfLife gameoflife;

void readResults(){
    ifstream myfile("test/goloutput.txt");
    string line;
    if(myfile.is_open()){
        vector<int> temp;
        while(getline(myfile, line)){
            if(line[0] != 'S'){
                stringstream ss(line);
                string word;
                while (ss >> word){
                    temp.push_back(stoi(word));
                }
            } else if(temp.size() > 0) {
                storeResult.push_back(temp);
                temp.clear();
            }
        }
        storeResult.push_back(temp);
    }
    cout << "size of storeresult: " << storeResult.size() << endl;
    myfile.close();
}

bool checkResults(int simSize, vector<int> result){
    switch(simSize){
        case 2:
            return (storeResult[0] == result);
        case 4:
            return (storeResult[1] == result);
        case 8:
            return (storeResult[2] == result);
        case 16:
            return (storeResult[3] == result);
        case 32:
            return (storeResult[4] == result);
    }
}
    
TEST(GameOfLife, LifeSize_2) {    
    // cudaDeviceProp prop;
    // cudaGetDeviceProperties(&prop, 0);
    // // Print device thread, grid, and sm sizes
    // printf( "maxThreadsPerBlock: %d\n", prop.maxThreadsPerBlock );
    // printf( "maxThreadsDim: %d %d %d\n", prop.maxThreadsDim[0], prop.maxThreadsDim[1], prop.maxThreadsDim[2] );
    // printf( "maxGridSize: %d %d %d\n", prop.maxGridSize[0], prop.maxGridSize[1], prop.maxGridSize[2] );

    int simSize = 2;
    
	vector<int> result;

    gameoflife.runMassSim(generations, simSize, result);

    // ASSERT_TRUE(checkResults(simSize, result));
}

TEST(GameOfLife, LifeSize_4) {
    int simSize = 4;
    
    vector<int> result;

	gameoflife.runMassSim(generations, simSize, result);
    
    // ASSERT_TRUE(checkResults(simSize, result));
}

TEST(GameOfLife, LifeSize_8) {
    int simSize = 8;
    
    vector<int> result;

	gameoflife.runMassSim(generations, simSize, result);
    
    // ASSERT_TRUE(checkResults(simSize, result));
}

TEST(GameOfLife, LifeSize_16) {
    int simSize = 16;
    
    vector<int> result;

	gameoflife.runMassSim(generations, simSize, result);
    
    // ASSERT_TRUE(checkResults(simSize, result));
}

TEST(GameOfLife, LifeSize_32) {
    int simSize = 32;
    
    vector<int> result;

	gameoflife.runMassSim(generations, simSize, result);
    
    // ASSERT_TRUE(checkResults(simSize, result));
}

TEST(GameOfLife, LifeSize_64) {
    int simSize = 64;
    
    vector<int> result;

	gameoflife.runMassSim(generations, simSize, result);
    
    // ASSERT_TRUE(checkResults(simSize, result));
}

TEST(GameOfLife, LifeSize_128) {
    int simSize = 128;
    
    vector<int> result;

	gameoflife.runMassSim(generations, simSize, result);
    
    // ASSERT_TRUE(checkResults(simSize, result));
}

TEST(GameOfLife, LifeSize_256) {
    int simSize = 256;
    
    vector<int> result;

	gameoflife.runMassSim(generations, simSize, result);
    
    // ASSERT_TRUE(checkResults(simSize, result));
}

TEST(GameOfLife_Benchmark, LifeSize_500) {
    printf("Running benchmark for LifeSize_500\n");

    int simSize = 500;
    
    vector<int> result;

	gameoflife.runMassSim(generations, simSize, result);
    
    // ASSERT_TRUE(checkResults(simSize, result));
}

// TEST(GameOfLife, LifeSize_165){
//     int simSize = 165;
//     vector<int> result;
//     gameoflife.runMassSim(generations, simSize, result);

//     // int i = 0;
//     // while(i < result.size()){
//     //     string tmpStr = "";
//     //     for(int j = i; j < 165 + i; j++){
//     //         tmpStr.append(to_string(result[j]));
//     //     }
//     //     cout << tmpStr << endl;

//     //     i += 165;
//     // }


//     ASSERT_TRUE(true);
// }

TEST(GameOfLife_Benchmark, LifeSize_1000){
    printf("Running benchmark for LifeSize_1000\n");
    
    int simSize = 1000;
    vector<int> result;
    gameoflife.runMassSim(generations, simSize, result);

    // int i = 0;
    // while(i < result.size()){
    //     string tmpStr = "";
    //     for(int j = i; j < 165 + i; j++){
    //         tmpStr.append(to_string(result[j]));
    //     }
    //     cout << tmpStr << endl;

    //     i += 165;
    // }


    ASSERT_TRUE(true);
}

TEST(GameOfLife_Benchmark, 1500) {
    printf("Running benchmark for LifeSize_1500\n");
    
    int simSize = 1500;
    
    vector<int> result;

	gameoflife.runMassSim(generations, simSize, result);
    
    // ASSERT_TRUE(checkResults(simSize, result));
}


TEST(GameOfLife_Benchmark, LifeSize_2000){
    printf("Running benchmark for LifeSize_2000\n");
    
    int simSize = 2000;
    vector<int> result;
    gameoflife.runMassSim(generations, simSize, result);

    // int i = 0;
    // while(i < result.size()){
    //     string tmpStr = "";
    //     for(int j = i; j < simSize + i; j++){
    //         tmpStr.append(to_string(result[j]));
    //     }
    //     cout << tmpStr << endl;

    //     i += simSize;
    // }

    ASSERT_TRUE(true);
}

TEST(GameOfLife_Benchmark, LifeSize_2500) {
    printf("Running benchmark for LifeSize_2500\n");
    
    int simSize = 2500;
    
    vector<int> result;

	gameoflife.runMassSim(generations, simSize, result);
    
    // ASSERT_TRUE(checkResults(simSize, result));
}


int main(int argc, char **argv) {
    // readResults();
    mass::logger::setLogLevel(boost::log::trivial::info);
	testing::InitGoogleTest(&argc, argv);
    testing::FLAGS_gtest_filter = "GameOfLife_Benchmark*";

    return RUN_ALL_TESTS();
}
