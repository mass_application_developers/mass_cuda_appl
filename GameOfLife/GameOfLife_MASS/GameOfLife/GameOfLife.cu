#include <iostream>
#include <sstream>     // ostringstream
#include <vector>
#include <stdlib.h>	// random

#include "GameOfLife.h"
#include "Life.h"
#include "../src/Mass.h"
#include "../src/Logger.h"
#include "Timer.h"

using namespace std;
using namespace mass;

GameOfLife::GameOfLife() {}
GameOfLife::~GameOfLife() {}

void GameOfLife::runMassSim(int generations, int size) {
	Logger::debug("Starting MASS CUDA simulation\n");

	int nDims = 2;
	int placesSize[] = {size, size};
	int totalLifeCells = size * size;

	// start a process at each computing node
	Mass::init();
	Logger::debug("Finished Mass::init\n");

	// initialization parameters
	bool initHealth[totalLifeCells];
	for (int i = 0; i < totalLifeCells; i++) {
		initHealth[i] = rand() % 2 == 0;
	}

	// initialize places
	Places *places = Mass::createPlaces<Life, LifeState>(0, NULL, 0, nDims, placesSize);
	Logger::debug("Finished Mass::createPlaces\n");

	// initialize life cells
	places->callAll(Life::INIT_HEALTH, initHealth, totalLifeCells * sizeof(bool));

	// create neighborhood
	vector<int*> neighbors;
	int northwest[] = {-1, 1};
	neighbors.push_back(northwest);
	int north[] = {0, 1};
	neighbors.push_back(north);
	int northeast[] = {1, 1};
	neighbors.push_back(northeast);
	int east[] = {1, 0};
	neighbors.push_back(east);
	int southeast[] = {1, -1};
	neighbors.push_back(southeast);
	int south[] = {0, -1};
	neighbors.push_back(south);
	int southwest[] = {-1, -1};
	neighbors.push_back(southwest);
	int west[] = {-1, 0};
	neighbors.push_back(west);

	// start a timer
	Timer timer;
	timer.start();

	for (int tick = 0; tick < generations; tick++) {
		places->exchangeAll(&neighbors, Life::COMPUTE_NEXT_GEN, &tick, sizeof(int));
	}
	displayResults(places, generations, size);
	Logger::print("MASS time %d\n\n",timer.lap());

	// close library and clean up
	Mass::finish();
}

void GameOfLife::displayResults(Places *places, int generations, int size) {
	Logger::debug("Entering GameOfLife::displayResults");
	ostringstream ss;
	ss << "Health Status at Size " << size << " after " << generations << " Generations\n";
	Place **grid = places->getElements();
	int coordinates[2];
	for (int row = 0; row < size; row++) {
		coordinates[0] = row;
		for (int col = 0; col < size; col++) {
			coordinates[1] = col;
			int rmi = places->getRowMajorIdx(coordinates);
			if (rmi != (row % size) * size + col) {
				Logger::error("Row Major Index is incorrect: [%d][%d] != %d", row, col, rmi);
			}
			bool currentHealth = ((Life*)grid[rmi])->getHealth();
			ss << currentHealth << "  ";
		}
		ss << "\n";
	}
	Logger::print(ss.str());
}
