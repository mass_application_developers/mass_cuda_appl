#ifndef GAMEOFLIFE_H_
#define GAMEOFLIFE_H_

#include "Life.h"
#include "../src/Places.h"

// A GameOfLife is the simulation that simulates Conway's Game of Life using
// the MASS CUDA library.
class GameOfLife {
public:
	GameOfLife();
	virtual ~GameOfLife();

	// Executes the game of life simulation using the given grid size and a
	// given number of generations.
	void runMassSim(int generations, int size);

	// Displays the results from executing the game of life simulation given
	// a pointer Places, the number of simulated generations, and simulation size.
	void displayResults(mass::Places *places, int generations, int size);
};

#endif
