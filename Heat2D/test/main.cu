#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

#include <gtest/gtest.h>
#include <mass/Mass.h>

#include "Metal.h"
#include "Timer.h"

bool checkResults(mass::Places *places, int time, int *placesSize) {
    bool correctResult = true;
    
    double targetResults[10][10] = {
        {0, 0, 1, 3, 4, 3, 1, 0, 0, 0},
        {0, 0, 1, 2, 3, 2, 1, 0, 0, 0},
        {0, 0, 0, 1, 1, 1, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    };

    mass::Place ** retVals = places->getElements();
    int indices[2];

    for (int row = 0; row < placesSize[0]; row++) {
        indices[0] = row;
        for (int col = 0; col < placesSize[1]; col++) {
            indices[1] = col;
            int rmi = places->getRowMajorIdx(indices);
            double temp = ((Metal*) retVals[rmi])->getTemp();
            double roundedTemp = floor(temp / 2);
            if (roundedTemp != targetResults[row][col]) {
                correctResult = false;
            }
        }
    }

    if (!correctResult) {
        return false;
    } else {
        return true;
    }
}

TEST(Heat2D, SimTest) {
    int size = 10;
    int max_time = 5;
    int heat_time = 4;
    int nDims = 2;
    int placesSize[] = { size, size };

    mass::Mass::init();

    // initialization parameters
    double a = 1.0;  // heat speed
    double dt = 1.0; // time quantum
    double dd = 2.0; // change in system
    double r = a * dt / (dd * dd);

    // initialize places
    mass::Places *places = mass::Mass::createPlaces<Metal, MetalState>(0 /*handle*/, &r,
            sizeof(double), nDims, placesSize);

    // create neighborhood
    std::vector<int*> neighbors;
    int north[2] = { 0, 1 };
    neighbors.push_back(north);
    int east[2] = { 1, 0 };
    neighbors.push_back(east);
    int south[2] = { 0, -1 };
    neighbors.push_back(south);
    int west[2] = { -1, 0 };
    neighbors.push_back(west);

    // start a timer
    Timer timer;
    timer.start();

    int time = 0;
    for (; time < max_time; time++) {
        if (time < heat_time) {
            places->callAll(Metal::APPLY_HEAT);
        }
        places->exchangeAll(&neighbors);
        places->callAll(Metal::EULER_METHOD);
    }

    ASSERT_TRUE(checkResults(places, time, placesSize));

    mass::Mass::finish();
}

TEST(Heat2D, SimTestImproved) {
    int size = 10;
    int max_time = 5;
    int heat_time = 4;
    int nDims = 2;
    int placesSize[] = { size, size };

    mass::Mass::init();

    // initialization parameters
    double a = 1.0;  // heat speed
    double dt = 1.0; // time quantum
    double dd = 2.0; // change in system
    double r = a * dt / (dd * dd);

    // initialize places
    mass::Places *places = mass::Mass::createPlaces<Metal, MetalState>(0 /*handle*/, &r,
            sizeof(double), nDims, placesSize);

    // create neighborhood
    std::vector<int*> neighbors;
    int north[2] = { 0, 1 };
    neighbors.push_back(north);
    int east[2] = { 1, 0 };
    neighbors.push_back(east);
    int south[2] = { 0, -1 };
    neighbors.push_back(south);
    int west[2] = { -1, 0 };
    neighbors.push_back(west);

    // start a timer
    Timer timer;
    timer.start();

    int time = 0;
    for (; time < max_time; time++) {
        if (time < heat_time) {
            places->callAll(Metal::APPLY_HEAT);
        }
        places->exchangeAll(&neighbors, Metal::EULER_METHOD, NULL /*argument*/, 0 /*argSize*/);
    }

    ASSERT_TRUE(checkResults(places, time, placesSize));

    mass::Mass::finish();
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
