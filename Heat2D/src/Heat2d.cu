/*
 *  @file Heat2d.cpp
 	
 *	@section LICENSE
 *  This is a file for use in Nate Hart's Thesis for the UW Bothell MSCSSE. All rights reserved.
 */

#include "Heat2d.h"
#include <ctime> // clock_t
#include <iostream>
#include <math.h>     // floor
#include <sstream>     // ostringstream
#include <vector>
#include <math.h>

#include <mass/Mass.h>
#include <mass/Logger.h>
#include "Metal.h"
#include "Timer.h"

static const int WORK_SIZE = 32;
static const double a = 1.0;  // heat speed
static const double dt = 1.0; // time quantum
static const double dd = 2.0; // change in system

Heat2d::Heat2d() {

}
Heat2d::~Heat2d() {

}

// getRGB retrieves the RGB color associated with the provided temp.
// It uses 19.0 as the point temp as that is what appears to be used
// in the rest of the implementation, and is not configurable. The color
// code is mapped to a unit circle with the +x representing the red value,
// -x representing the blue value, and +y representing green. x is known
// and we use the pythagorean theorem to find y.
unsigned char* getRGB(double temp) {
	double pointTemp = 19.0;
	
	// lshift used to center temp on unit circle
	double lshift = pointTemp / 2;
	double x = temp -lshift;
	double x_sqrd = x*x;

	// calculate y
	double y = std::sqrt(1 - x_sqrd);

	// calculate and return RGB values
	unsigned char g = y * 255;
	unsigned char r = 0;
	unsigned char b = 0;
	if (x < 0) {
		b = std::abs(255 * x);
	} else if (x > 0) {
		r = 255 * x;
	}

	return new unsigned char[3]{r, g, b};
}

void Heat2d::displayResults(simviz::RGBFile& vizFile, mass::Places *places, int time, int *placesSize) {
	mass::logger::debug("Entering Heat2d::displayResults");
	std::ostringstream ss;

	ss << "time = " << time << "\n";
	mass::Place ** retVals = places->getElements(); //refreshes places here
	int indices[2];
	for (int row = 0; row < placesSize[0]; row++) {
		indices[0] = row;
		for (int col = 0; col < placesSize[1]; col++) {
			indices[1] = col;
			int rmi = places->getRowMajorIdx(indices);
			if (rmi != (row % placesSize[0]) * placesSize[0] + col) {
				mass::logger::error("Row Major Index is incorrect: [%d][%d] != %d",
						row, col, rmi);
			}

			double temp = ((Metal*) retVals[rmi])->getTemp();
			ss << floor(temp / 2) << " ";

			vizFile.write((char*) getRGB(temp), simviz::NumRGBBytes);

		}

		ss << "\n";
	}
	ss << "\n";
	mass::logger::debug(ss.str());
}

void Heat2d::runHostSim(int size, int max_time, int heat_time, int interval) {
	mass::logger::debug("Starting CPU simulation\n");
	double r = a * dt / (dd * dd);

	// create a space
	double ***z = new double**[2];
	for (int p = 0; p < 2; p++) {
		z[p] = new double*[size];
		for (int x = 0; x < size; x++) {
			z[p][x] = new double[size];
			for (int y = 0; y < size; y++) {
				z[p][x][y] = 0.0; // no heat or cold
			}
		}
	}

	// start a timer
	Timer timer;
	timer.start();

	// simulate heat diffusion
	int t = 0;
	int p;
	for (; t < max_time; t++) {
		p = t % 2; // p = 0 or 1: indicates the phase

		// two left-most and two right-most columns are identical
		for (int y = 0; y < size; y++) {
			z[p][0][y] = z[p][1][y];
			z[p][size - 1][y] = z[p][size - 2][y];
		}

		// two upper and lower rows are identical
		for (int x = 0; x < size; x++) {
			z[p][x][0] = z[p][x][1];
			z[p][x][size - 1] = z[p][x][size - 2];
		}

		// keep heating the bottom until t < heat_time
		if (t < heat_time) {
			for (int x = size / 3; x < size / 3 * 2; x++)
				z[p][x][0] = 19.0; // heat
		}

		// display intermediate results
		if (interval != 0 && (t % interval == 0 || t == max_time - 1)) {
			std::ostringstream ss;
			ss << "time = " << t << "\n";
			for (int y = 0; y < size; y++) {
				for (int x = 0; x < size; x++)
					ss << floor(z[p][x][y] / 2) << " ";
				ss << "\n";
			}
			ss << "\n";
			mass::logger::debug(ss.str());
		}

		// perform forward Euler method
		int p2 = (p + 1) % 2;
		for (int x = 1; x < size - 1; x++)
			for (int y = 1; y < size - 1; y++)
				z[p2][x][y] = z[p][x][y]
						+ r * (z[p][x + 1][y] - 2 * z[p][x][y] + z[p][x - 1][y])
						+ r * (z[p][x][y + 1] - 2 * z[p][x][y] + z[p][x][y - 1]);

	} // end of simulation

	// finish the timer
	mass::logger::info("CPU time: %d\n",timer.lap());


	if (size < 80) {
		std::ostringstream ss;
		ss << "time = " << t << "\n";
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++)
				ss << floor(z[p][x][y] / 2) << " ";
			ss << "\n";
		}
		ss << "\n";
		mass::logger::debug(ss.str());
	}

	// clean up memory
	for (int p = 0; p < 2; p++) {
		for (int x = 0; x < size; x++) {
			delete[] z[p][x];
		}
		delete[] z[p];
	}
	delete[] z;
}

void Heat2d::runMassSim(int size, int max_time, int heat_time, int interval, simviz::RGBFile& vizFile) {
	mass::logger::debug("Starting MASS CUDA simulation\n");

	int nDims = 2;
	int placesSize[] = { size, size };

	// start a process at each computing node
	mass::Mass::init();
	mass::logger::debug("Finished Mass::init\n");

	// initialization parameters
	double r = a * dt / (dd * dd);

	// initialize places
	mass::Places *places = mass::Mass::createPlaces<Metal, MetalState>(0 /*handle*/, &r,
			sizeof(double), nDims, placesSize);
	mass::logger::debug("Finished Mass::createPlaces\n");

	// create neighborhood
	std::vector<int*> neighbors;
	int north[2] = { 0, 1 };
	neighbors.push_back(north);
	int east[2] = { 1, 0 };
	neighbors.push_back(east);
	int south[2] = { 0, -1 };
	neighbors.push_back(south);
	int west[2] = { -1, 0 };
	neighbors.push_back(west);

	// start a timer
	Timer timer;
	timer.start();

	int time = 0;
	for (; time < max_time; time++) {

		if (time < heat_time) {
			places->callAll(Metal::APPLY_HEAT);
		}

		// display intermediate results
		if (interval != 0 && (time % interval == 0 || time == max_time - 1)) {
			displayResults(vizFile, places, time, placesSize);
		}

		places->exchangeAll(&neighbors, Metal::EULER_METHOD, NULL /*argument*/, 0 /*argSize*/);
	}

	mass::logger::info("MASS time %d\n",timer.lap());

	if (placesSize[0] < 80) {
		displayResults(vizFile, places, time, placesSize);
	}

	// terminate the processes
	mass::Mass::finish();
}


/*
 * Begin implementation of custom CUDA implementation of HEAT2D
 */
__global__ void setCold(double *dest, double *src, int size) {
	int idx = mass::getGlobalIdx_1D_1D();

	if (idx < size * size) {
		dest[idx] = 0.0;
		src[idx] = 0.0;
	}
}

__device__ bool isLeftEdge(int idx, int size) {
	return idx % size == 0;
}

__device__ bool isRightEdge(int idx, int size) {
	return idx % size == size - 1;
}

__device__ bool isTopEdge(int idx, int size) {
	return idx < size;
}

__device__ bool isBottomEdge(int idx, int size) {
	return idx >= size * size - size;
}

__global__ void setEdges(double *dest, double *src, int size, int t,
		int heat_time, double r) {
	int idx = mass::getGlobalIdx_1D_1D();

	if (idx < size * size) {

		// apply heat to top row
		if (idx >= size / 3 && idx < size / 3 * 2 && t < heat_time) {
			src[idx] = 19.0; // heat
			return;
		}
		// two left-most and two right-most columns are identical
		if (isLeftEdge(idx, size)) {
			src[idx] = src[idx + 1];
			return;
		}
		if (isRightEdge(idx, size)) {
			src[idx] = src[idx - 1];
			return;
		}

		// two upper and lower rows are identical
		if (isTopEdge(idx, size)) {
			src[idx] = src[idx + size];
			return;
		}
		if (isBottomEdge(idx, size)) {
			src[idx] = src[idx - size];
			return;
		}
	}
}

__global__ void euler(double *dest, double *src, int size, int t, int heat_time,
		double r) {
	int blockId = blockIdx.x + blockIdx.y * gridDim.x;
	int threadId = blockId * (blockDim.x * blockDim.y)
			+ (threadIdx.y * blockDim.x) + threadIdx.x;
	int idx = threadId;

	// perform forward Euler method
	if (idx > size && idx < size * size - size) {
		double tmp = src[idx];
		dest[idx] = tmp + r * (src[idx + 1] - 2 * tmp + src[idx - 1])
				+ r * (src[idx + size] - 2 * tmp + src[idx - size]);
	}

}

void Heat2d::runDeviceSim(int size, int max_time, int heat_time, int interval) {
	mass::logger::debug("Starting GPU simulation\n");
	double r = a * dt / (dd * dd);

	// create a space
	double *z = new double[size * size];
	double *dest, *src;
	int nBytes = sizeof(double) * size * size;
	mass::CATCH(cudaMalloc((void** ) &dest, nBytes));
	mass::CATCH(cudaMalloc((void** ) &src, nBytes));

	int gridWidth = (size * size - 1) / WORK_SIZE + 1;
	int threadWidth = (size * size - 1) / gridWidth + 1;

	dim3 gridDim(gridWidth);
	dim3 threadDim(threadWidth);

	setCold<<<gridDim, threadDim>>>(dest, src, size);
	mass::CHECK();

	// start a timer
	Timer time;
	time.start();

	// simulate heat diffusion
	int t = 0;
	for (; t < max_time; t++) {
		setEdges<<<gridDim, threadDim>>>(dest, src, size, t, heat_time, r);
		mass::CHECK();

		// display intermediate results
		if (interval != 0 && (t % interval == 0 || t == max_time - 1)) {
			mass::CATCH(cudaMemcpy(z, src, nBytes, D2H));
			std::ostringstream ss;
			ss << "time = " << t << "\n";
			for (int y = 0; y < size; y++) {
				for (int x = 0; x < size; x++)
					ss << floor(z[(y % size) * size + x] / 2) << " ";
				ss << "\n";
			}
			ss << "\n";
			mass::logger::debug(ss.str());
		}

		euler<<<gridDim, threadDim>>>(dest, src, size, t, heat_time, r);
		mass::CHECK();

		double *swap = dest;
		dest = src;
		src = swap;
	} // end of simulation

	// finish the timer
	mass::logger::info("GPU time: %d\n",time.lap());

	if (size < 80) {
		mass::CATCH(cudaMemcpy(z, dest, nBytes, D2H));
		std::ostringstream ss;
		ss << "time = " << t << "\n";
		for (int y = 0; y < size; y++) {
			for (int x = 0; x < size; x++)
				ss << floor(z[(y % size) * size + x] / 2) << " ";
			ss << "\n";
		}
		ss << "\n";
		mass::logger::debug(ss.str());
	}
	delete[] z;
}

