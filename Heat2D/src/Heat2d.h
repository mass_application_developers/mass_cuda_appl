/*
 *  @file Heat2d.h
 	
 *	@section LICENSE
 *  This is a file for use in Nate Hart's Thesis for the UW Bothell MSCSSE. All rights reserved.
 */
#pragma once

#include "Metal.h"
#include <mass/Places.h>
#include "simviz.h"

class Heat2d {

public:
	Heat2d();
	virtual ~Heat2d();

	void runMassSim(int size, int max_time, int heat_time, int interval, simviz::RGBFile& vizFile);
	void runDeviceSim(int size, int max_time, int heat_time, int interval);
	void runHostSim(int size, int max_time, int heat_time, int interval);
	void displayResults(simviz::RGBFile& vizFile, mass::Places *places, int time, int *placesSize);

};
