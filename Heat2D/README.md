# Heat2D

To build and run the Heat2D application, first run `make develop`. Once the development environment is setup run `make build` and then `make test` to ensure all tests are passing properly. Once built, a `Heat2D` executable will be placed within the `./bin` directory, and can be used to run the simulation.

Running the application with the `--help` flag will provide a description of the application and options that can be used to define simulation parameters.

Specifying an output interval, by using the `--interval` flag will tell the program to output the simulation to a `.viz` file every `<interval>` steps. This simulation file can be played using the MASS SimViz application for a graphical visualization of the simulation space.

In the example below, the hotter cells within the simulation space are colored red, with a gradual shift to blue to indicate cooling temperatures.

_Example output with a simulation space of 100x100_


![Braingrid (100x100)](./img/heat2d_100x100.gif)


```
## Command to reproduce the above simulation
./bin/Heat2D --interval=30
```