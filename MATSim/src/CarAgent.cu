#include "CarAgent.h"

MASS_FUNCTION CarAgent::CarAgent(mass::AgentState *state, void *argument) :
        Agent(state, argument) {
    this->state = (CarState*) state;
}

MASS_FUNCTION CarAgent::~CarAgent() {}

MASS_FUNCTION void CarAgent::callMethod(int functionId, void *argument) {
    switch (functionId) {
        case CarFunctions::INIT_CAR:
            initCar(((matsim::Car*)argument)[getIndex()]); 
            break;
        case CarFunctions::INIT_CAR_ROUTE:
            initCarRoute(((int*)argument)); 
            break;
        case CarFunctions::MOVE_ON_LANE:
            moveOnLane();
            break;
            case CarFunctions::MOVE_TO_LANE:
            moveToLane(*(int*)argument);
            break;
        case CarFunctions::MOVE_TO_INTERSECTION:
            moveToIntersection(*(int*)argument);
            break;
        case CarFunctions::SETTLE_ON_NEW_PLACE:
            settleOnNewPlace();
            break;
        case CarFunctions::PRINT_CAR_STATE:
            printState();
            break;
    }
}

/**** MASS functions ****/

MASS_FUNCTION void CarAgent::initCar(matsim::Car car) {
    state->id = car.id;
    state->start = car.start;
    state->end = car.end;
    state->route = new int[car.routeSize];
    state->routeSize = car.routeSize;
    state->routeOffset = car.routeOffset;
    state->routePos = state->routeSize - 1;
    state->distToGo = 0;
    state->onLane = state->migrating = false;
    state->movingDirection = -1;
}

MASS_FUNCTION void CarAgent::initCarRoute(int* route) {
    for (int i = 0; i < state->routeSize; i++) {
        state->route[i] = route[state->routeOffset + i];
    }
    IntersectionPlace* place = (IntersectionPlace*)getPlace();
}

MASS_FUNCTION void CarAgent::moveOnLane() {
    if (state->onLane) {
        if (state->distToGo > 0) {
            state->distToGo--; // Move forward on lane
        }
    }
    else {
        ((IntersectionPlace*)getPlace())->requestLane(getIndex(), state->route[state->routePos]);
        state->movingDirection = ((IntersectionPlace*)getPlace())->getDirectionByID(state->route[state->routePos]);
    }
}

MASS_FUNCTION void CarAgent::moveToLane(int direction) {
    if (!state->onLane && state->movingDirection == direction) {
        int roadDist = ((IntersectionPlace*)getPlace())->putCarOnLane(getIndex(), state->route[state->routePos]);
        if (roadDist != -1) {
            state->distToGo = roadDist;
            state->onLane = true;
        }
    }
}

MASS_FUNCTION void CarAgent::moveToIntersection(int direction) {
    if (state->onLane && state->distToGo == 0 && state->movingDirection == direction) {
        bool canMove = ((IntersectionPlace*)getPlace())->depart(getIndex(), direction);
        if (canMove) {
            migrate(direction);
        }
    }
}

MASS_FUNCTION void CarAgent::migrate(int direction) {
    IntersectionPlace* place = (IntersectionPlace*)getPlace();
    state->migrating = true;
    ((IntersectionPlace*)(place->state->neighbors[direction]))->addAgent(getIndex());
    migrateAgent(place->state->neighbors[direction], direction);
}

MASS_FUNCTION void CarAgent::settleOnNewPlace() {
    if (state->migrating) {
        state->distToGo = 0;
        state->onLane = state->migrating = false;
        state->movingDirection = -1;
        if (((IntersectionPlace*)getPlace())->getID() == state->end) {
            ((IntersectionPlace*)getPlace())->terminateAgent(getIndex());
            terminateAgent();
        }
        else {
            state->routePos--; // Continue morning route
        }
    }
}

/**** MASS DEBUG functions ****/

MASS_FUNCTION void CarAgent::printState() {
    printf("id=%d, start:%d, end:%d\n",
        state->id,
        state->start,
        state->end
    );
}

/**** Getters ****/

MASS_FUNCTION CarState* CarAgent::getState() {
    return state;
}
