#pragma once

#include <boost/program_options.hpp>

#include <boost/unordered_map.hpp>

#include <mass/Places.h>
#include <mass/Agents.h>

#include "Road.h"

#include <simviz.h>

#include <vector>

namespace po = boost::program_options;

enum Directions : int {
    NORTH = 0,
    EAST,
    SOUTH,
    WEST
};

namespace matsim {
    // Simulation configuration options.
    struct ConfigOpts {
        int sizeX;
        int sizeY;
        int numCars;
        int interval;
        std::string roadFile;
        std::string routeFile;
    };

    struct Intersection {
        int id;
        int x;
        int y;
        bool isRoad;
        int destination[4];
        int roadId[4];
    };

    struct Car {
        int id;
        int start;
        int end;
        int routeSize;
        int routeOffset;
    };

    // runSimulation runs the MATSim simulation, configured with the
    // provided options.
    void runSimulation(ConfigOpts opts, int interval, simviz::RGBFile& vizFile);

    // outputSimSpace outputs a graphical visualiztion of the simulation space
    // to the provided simviz file.
    void outputSimSpace(simviz::RGBFile& vizFile, mass::Places *places, int sizeX, int sizeY);

    // parseSimConfig parses the provided variables_map and
    // returns configs options for which to run the simulation
    // with.
    ConfigOpts parseSimConfig(po::variables_map vm);

    // generateRandVals generates 'n' random values, using cstdlib
    // and the provided seed.
    void readRoadFile(std::string filename, Intersection* intersections);

    std::vector<std::vector<int>> readRouteFile(std::string filename, Car* cars, int numCars);
    
    void routesToRoutesArr(std::vector<std::vector<int>> routes, int* routesArr, Car* cars);

    void fillPlaceIdxs(int* placeIdxs, Car* cars, int numCars);
}