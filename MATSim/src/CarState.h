
#pragma once

#include <vector>

#include "mass/AgentState.h"

class CarState: public mass::AgentState {
public:
    int id, start, end;  // ID, starting point, ending point of route
    int* route;          // The route of roads to be traversed
    int routeSize;       // Size of route array
    int routeOffset;     // Offset for combined route array
    int routePos;        // Position in route vector
    int distToGo;        // Distance left to be travelled on road
    int movingDirection; // Stores the direction the car is moving
    bool onLane;		 // True when on a lane
    bool migrating;  // Indicates if the car has finished its route
};
