#pragma once

#include "mass/Agent.h"
#include "mass/AgentState.h"
#include "mass/Logger.h"

#include "CarState.h"
#include "IntersectionPlace.h"
#include "MATSim.h"

enum CarFunctions : int {
    INIT_CAR = 0,
    INIT_CAR_ROUTE,
    MOVE_ON_LANE,
    MOVE_TO_LANE,
    MOVE_TO_INTERSECTION,
    SETTLE_ON_NEW_PLACE,
    
    // Debug functions
    PRINT_CAR_STATE,
};

class CarAgent: public mass::Agent {

public:

    MASS_FUNCTION CarAgent(mass::AgentState *state, void *argument);
    MASS_FUNCTION ~CarAgent();
    MASS_FUNCTION virtual void callMethod(int functionId, void *arg = NULL);

    // Getters
    MASS_FUNCTION virtual CarState* getState();

private:

    CarState* state;

    // MASS functions
    MASS_FUNCTION void initCar(matsim::Car car);
    MASS_FUNCTION void initCarRoute(int* route);
    MASS_FUNCTION void moveOnLane();
    MASS_FUNCTION void moveToLane(int direction);
    MASS_FUNCTION void moveToIntersection(int direction);
    MASS_FUNCTION void migrate(int direction);
    MASS_FUNCTION void settleOnNewPlace();

    // DEBUG functions
    MASS_FUNCTION void printState();
};
