#pragma once

#include <boost/unordered_map.hpp>
#include <mutex>

#include <mass/PlaceState.h>
#include "Road.h"

// forward declarations
class IntersectionPlace;

struct IntersectionConfig {};

class IntersectionState : public mass::PlaceState {
    public:
        int id, x, y, capacity, numCars;
        bool isRoad;
        int roadId[4], length[4], numLanes[4], destination[4], capacityRoad[4], numCarsRoad[4], direction[4], visualizeCar[4]; // Road values
        int roadAgents[4], requestedRoad[4];
        bool terminateAgents[4];
};
