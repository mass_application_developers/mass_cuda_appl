#include "IntersectionPlace.h"
#include <iostream>

#include <mass/Mass.h>

MASS_FUNCTION IntersectionPlace::IntersectionPlace(mass::PlaceState *placeState, void *argument) 
    : mass::Place(placeState, argument) {

    state = (IntersectionState*)placeState;
}

MASS_FUNCTION IntersectionPlace::~IntersectionPlace() {}

MASS_FUNCTION void IntersectionPlace::callMethod(int functionId, void *argument) {
    switch (functionId) {
        case IntersectionFunctions::INIT_INTERSECTION:
            initIntersection(((matsim::Intersection*)argument)[getIndex()]);
            break;
        case IntersectionFunctions::INIT_AGENT:
            initAgent();
            break;
        case IntersectionFunctions::RESOLVE_LANE:
            resolveLane();
            break;
        case IntersectionFunctions::RESOLVE_TERMINATE:
            resolveTerminate();
            break;
        case IntersectionFunctions::PRINT_STATE:
            printState();
            break;
    }

    return;
}

/**** MASS functions ****/

MASS_FUNCTION void IntersectionPlace::initIntersection(matsim::Intersection intersection) {
    state->numCars = 0;

    state->id = intersection.id;
    state->x = intersection.x;
    state->y = intersection.y;
    state->isRoad = intersection.isRoad;

    int numRoads = 0;
    
    for (int i = 0; i < 4; i++) {
        state->roadId[i] = intersection.roadId[i];
        state->destination[i] = intersection.destination[i];

        if (state->roadId[i] != -1) {
            state->length[i] = 10;
            state->numLanes[i] = 1;
            state->capacityRoad[i] = 1; //std::max((int)(state->numLanesN * state->lengthN) / 15, 1);
            state->numCarsRoad[i] = 0;
            if (state->destination[i] == state->id + 1) state->direction[i] = 1;       // East
            else if (state->destination[i] == state->id - 1) state->direction[i] = 3;  // West
            else if (state->destination[i] > state->id + 1) state->direction[i] = 2;   // South
            else if (state->destination[i] < state->id - 1) state->direction[i] = 0;   // North
            numRoads++;
        } else {
            state->length[i] = -1;
            state->numLanes[i] = -1;
            state->capacityRoad[i] = -1;
            state->numCarsRoad[i] = -1;
            state->direction[i] = -1;
        }

        state->roadAgents[i] = -1;
        state->requestedRoad[i] = -1;
        state->terminateAgents[i] = false;
    }

    state->isRoad = numRoads > 0;
    state->capacity = numRoads * 1; // Number of lanes hardcoded to 1
}

MASS_FUNCTION void IntersectionPlace::initAgent() {
    for (int i = 0; i < 4; i++) {
        if (state->agents[i]) {
            state->roadAgents[i] = state->agents[i]->getIndex();
            addCar();
        }
    }
}

MASS_FUNCTION int IntersectionPlace::putCarOnLane(int index, int roadId) {
    int roadLength = -1;
    for (int i = 0; i < 4; i++) {
        if (roadId == state->requestedRoad[i]) {
            if (index == state->roadAgents[i]) break;
            return roadLength;
        }
    }
    int direction = getDirectionByID(roadId);
    if (addCar(direction)) {
        roadLength = getRoadLengthByDirection(direction);
        state->numCars--;
    }
    return roadLength;
}

MASS_FUNCTION void IntersectionPlace::requestLane(int index, int roadId) {
    for (int i = 0; i < 4; i++) {
        if (state->roadAgents[i] == index) {
            state->requestedRoad[i] = roadId;
        }
    }
}

MASS_FUNCTION void IntersectionPlace::resolveLane() {
    for (int i = 0; i < 3; i++) {
        for (int j = i + 1; j < 4; j++) {
            if (state->requestedRoad[j] == state->requestedRoad[i]) { 
                state->requestedRoad[j] = -1;        // Only 1 car can go to a direction at a time
            }
        }
    }
}

MASS_FUNCTION bool IntersectionPlace::depart(int index, int direction) {
    bool canMove = false;
    IntersectionPlace* dest = (IntersectionPlace*)((state->neighbors)[direction]);
    if (dest->state->numCars < dest->state->capacity) {
        removeCar(index, direction); // Remove from road
        canMove = true;
    }
    return canMove;
}

MASS_FUNCTION void IntersectionPlace::addCar() {
    state->numCars++;
}

MASS_FUNCTION void IntersectionPlace::removeCar() {
    if (state->numCars > 0) {
        state->numCars--;
    }
}

MASS_FUNCTION void IntersectionPlace::addAgent(int index) {
    for (int i = 0; i < 4; i++) {
        if (state->roadAgents[i] == -1) {
            state->roadAgents[i] = index;
            addCar();
            break;
        }
    }
}

MASS_FUNCTION void IntersectionPlace::terminateAgent(int index) {
    for (int i = 0; i < 4; i++) {
        if (state->roadAgents[i] == index) {
            state->terminateAgents[i] = true;
            break;
        }
    }
}

MASS_FUNCTION void IntersectionPlace::resolveTerminate() {
    for (int i = 0; i < 4; i++) {
        if (state->terminateAgents[i]) {
            removeCar();
            state->terminateAgents[i] = false;
            state->roadAgents[i] = -1;
            state->requestedRoad[i] = -1;
        }
    }
}

/**** MASS DEBUG functions ****/

MASS_FUNCTION void IntersectionPlace::printState() {
    printf("id=%d, x:%d, y:%d, roads=[%d, %d, %d, %d]\n",
        state->id,
        state->x,
        state->y,
        state->roadId[0],
        state->roadId[1],
        state->roadId[2],
        state->roadId[3]
    );
}

/**** Road Functions ****/

MASS_FUNCTION bool IntersectionPlace::addCar(int direction) {
    if (state->capacityRoad[direction] > state->numCarsRoad[direction]) { 
        state->numCarsRoad[direction]++;
        return true;
    }
    return false;
}

MASS_FUNCTION void IntersectionPlace::removeCar(int index, int direction) {
    state->numCarsRoad[direction]--;
    for (int i = 0; i < 4; i++) {
        if (state->roadAgents[i] == index) {
            state->roadAgents[i] = -1;
            state->requestedRoad[i] = -1;
            break;
        }
    }
}

/**** Getters ****/

MASS_FUNCTION int IntersectionPlace::getID() {
    return state->id;
}

MASS_FUNCTION int IntersectionPlace::getCapacity() {
    return state->capacity;
}

MASS_FUNCTION int IntersectionPlace::getNumCars() {
    return state->numCars;
}

MASS_FUNCTION bool IntersectionPlace::isRoad() {
    return state->isRoad;
}

MASS_FUNCTION int IntersectionPlace::getDirectionByDestID(int destId) {
    for (int dir = 0; dir < 4; dir++) {
        if (destId == state->destination[dir]) return dir;
    }
    return -1;
}

MASS_FUNCTION int IntersectionPlace::getDirectionByID(int roadID) {
    for (int dir = 0; dir < 4; dir++) {
        if (roadID == state->roadId[dir]) return dir;
    }
    return -1;
}

MASS_FUNCTION int IntersectionPlace::getRoadIDByDirection(int direction) {
    return state->roadId[direction];
}

MASS_FUNCTION int IntersectionPlace::getRoadLengthByDirection(int direction) {
    return state->length[direction];
}

MASS_FUNCTION int IntersectionPlace::getDestByDirection(int direction) {
    return state->destination[direction];
}

MASS_FUNCTION int IntersectionPlace::getNumCars(int direction) {
    return state->numCarsRoad[direction];
}