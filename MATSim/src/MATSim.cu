#include "MATSim.h"

#include <fstream>        // fstream

#include <mass/Mass.h>

#include "CarAgent.h"
#include "CarState.h"
#include "IntersectionPlace.h"
#include "IntersectionState.h"

namespace matsim {
    void runSimulation(ConfigOpts opts, int interval, simviz::RGBFile& vizFile) {
        mass::logger::info("Starting MASS CUDA MATSim simulation");
        
        mass::logger::info("MASS Config: MAX_AGENTS=%d, MAX_NEIGHBORS=%d, MAX_DIMS=%d, N_DESTINATIONS=%d",
            MAX_AGENTS,
            MAX_NEIGHBORS,
            MAX_DIMS,
            N_DESTINATIONS
        );

        // initialize MASS
        mass::Mass::init();

        int dims = 2;
        int simSpace[] = {opts.sizeX, opts.sizeY};
        int numIntersections = opts.sizeX * opts.sizeY;

        // create Intersection places to represent our simulation space.
        mass::Places* intersectionPlaces = mass::Mass::createPlaces<IntersectionPlace, IntersectionState>(
            0,                  // handle
            NULL,               // args
            0,                  // arg size
            dims,               // dimensions
            simSpace            // size of sim space
        );

        Car* cars = new Car[opts.numCars];

        // Generate values and route from Car_Agents.txt for each Car.
        std::vector<std::vector<int>> routes = readRouteFile(opts.routeFile, cars, opts.numCars);
        int routesArr[cars[opts.numCars - 1].routeOffset + cars[opts.numCars - 1].routeSize];
        routesToRoutesArr(routes, routesArr, cars);

        int* placeIdxs = new int[opts.numCars];
        fillPlaceIdxs(placeIdxs, cars, opts.numCars);

        // create Car Agents
        mass::Agents* carAgents = mass::Mass::createAgents<CarAgent, CarState>(
            1,                  // handle
            NULL,               // args
            0,                  // arg size
            opts.numCars,       // number of agents
            0,                  // places handle
            opts.numCars,       // max agents
            placeIdxs           // initial starting places
        );

        Intersection* intersections = new Intersection[numIntersections];

        // Generate values from Node_Links.txt for each Intersection.
        readRoadFile(opts.roadFile, intersections);

        intersectionPlaces->callAll(IntersectionFunctions::INIT_INTERSECTION, intersections, sizeof(Intersection) * numIntersections);

        carAgents->callAll(CarFunctions::INIT_CAR, cars, sizeof(Car) * opts.numCars);
        carAgents->callAll(CarFunctions::INIT_CAR_ROUTE, routesArr, sizeof(int) * (cars[opts.numCars - 1].routeOffset + cars[opts.numCars - 1].routeSize));

        intersectionPlaces->callAll(IntersectionFunctions::INIT_AGENT);

        // DEBUG Calls
        // intersectionPlaces->callAll(IntersectionFunctions::PRINT_STATE);
        // carAgents->callAll(CarFunctions::PRINT_CAR_STATE);

        // Output initial state
        if (opts.interval != 0) {
            outputSimSpace(vizFile, intersectionPlaces, opts.sizeX, opts.sizeY);
        }

        // create neighborhood for average pollution calculations
        std::vector<int*> neighbors;
        int top[2] = { 0, 1 };
        neighbors.push_back(top);
        int right[2] = { 1, 0 };
        neighbors.push_back(right);
        int bottom[2] = { 0, -1 };
        neighbors.push_back(bottom);
        int left[2] = { -1, 0 };
        neighbors.push_back(left);
        intersectionPlaces->exchangeAll(&neighbors);

        // Start simulation loop
        for (int i = 0; carAgents->getNumAgents() > 0; i++) {
            carAgents->callAll(CarFunctions::MOVE_ON_LANE);
            intersectionPlaces->callAll(IntersectionFunctions::RESOLVE_LANE);
            carAgents->callAll(CarFunctions::MOVE_TO_LANE, new int(Directions::NORTH), sizeof(int));
            carAgents->callAll(CarFunctions::MOVE_TO_LANE, new int(Directions::EAST), sizeof(int));
            carAgents->callAll(CarFunctions::MOVE_TO_LANE, new int(Directions::SOUTH), sizeof(int));
            carAgents->callAll(CarFunctions::MOVE_TO_LANE, new int(Directions::WEST), sizeof(int));

            carAgents->callAll(CarFunctions::MOVE_TO_INTERSECTION, new int(Directions::NORTH), sizeof(int));
            carAgents->callAll(CarFunctions::MOVE_TO_INTERSECTION, new int(Directions::EAST), sizeof(int));
            carAgents->callAll(CarFunctions::MOVE_TO_INTERSECTION, new int(Directions::SOUTH), sizeof(int));
            carAgents->callAll(CarFunctions::MOVE_TO_INTERSECTION, new int(Directions::WEST), sizeof(int));

            carAgents->manageAll();

            carAgents->callAll(CarFunctions::SETTLE_ON_NEW_PLACE);
            intersectionPlaces->callAll(IntersectionFunctions::RESOLVE_TERMINATE);

            // Output viz frame each 'interval' tick.
            if (interval != 0 && i % interval == 0) {
                outputSimSpace(vizFile, intersectionPlaces, opts.sizeX, opts.sizeY);
            }
        }
    
        // implement simulation...
        mass::logger::info("simulation complete, shutting down...");

        mass::Mass::finish();
    }

    ConfigOpts parseSimConfig(po::variables_map vm) {
        ConfigOpts opts = ConfigOpts{
            vm["sizeX"].as<int>(),
            vm["sizeY"].as<int>(),
            vm["numCars"].as<int>(),
            vm["interval"].as<int>(),
            vm["roadFile"].as<std::string>(),
            vm["routeFile"].as<std::string>()
        };
        
        mass::logger::debug("Simulation Config: {sizeX=%d, sizeY=%d, numCars=%d, "
            "roadFile=%s, routeFile=%s}",
            opts.sizeX,
            opts.sizeY,
            opts.numCars,
            opts.roadFile,
            opts.routeFile
        );

        return opts;
    }

    void outputSimSpace(simviz::RGBFile& vizFile, mass::Places *places, int sizeX, int sizeY) {
        unsigned char* backgroundColor = new unsigned char[3]{255, 255, 255};         // white
        unsigned char* roadColor = new unsigned char[3]{90, 90, 90};                  // dark gray
        unsigned char* oneCarsColor = new unsigned char[3]{0, 255, 0};                // green
        unsigned char* twoCarsColor = new unsigned char[3]{255, 255, 0};              // yellow
        unsigned char* threeCarsColor = new unsigned char[3]{255, 165, 0};              // orange
        unsigned char* fourCarsColor = new unsigned char[3]{255, 0, 0};               // red
        
        int indices[2];
        mass::Place** cells = places->getElements();
        for (int row = 0; row < sizeY * 3; row++) {
            indices[0] = row / 3;
            for (int col = 0; col < sizeX * 3; col++) {
                indices[1] = col / 3;
                int rmi = places->getRowMajorIdx(indices);
                if (rmi != (indices[0] % sizeX) * sizeY + indices[1]) {
                    mass::logger::error("Row Major Index is incorrect: [%d][%d] != %d",
                    row, col, rmi
                    );

                    continue;
                }

                IntersectionPlace* intersection = (IntersectionPlace*)cells[rmi];
                int direction;

                if (row % 3 == 1) {
                    if (col % 3 == 1) {             // middle of intersection 
                        if (intersection->isRoad()) {
                            int numCars = intersection->getNumCars();
                            if (numCars == 1) {
                                vizFile.write((char*)oneCarsColor, simviz::NumRGBBytes);
                            } else if (numCars == 2) {
                                vizFile.write((char*)twoCarsColor, simviz::NumRGBBytes);
                            } else if (numCars == 3) {
                                vizFile.write((char*)threeCarsColor, simviz::NumRGBBytes);
                            } else if (numCars == 4) {
                                vizFile.write((char*)fourCarsColor, simviz::NumRGBBytes);
                            } else {
                                vizFile.write((char*)roadColor, simviz::NumRGBBytes);
                            }

                            continue;
                        }

                        vizFile.write((char*)backgroundColor, simviz::NumRGBBytes);
                        continue;
                    } else if (col % 3 == 0) {      // left of intersection
                        if (intersection->getID() % sizeX == 0) {
                            vizFile.write((char*)backgroundColor, simviz::NumRGBBytes);
                            continue;
                        }
                        direction = Directions::WEST;
                    } else {    // right of intersection
                        if (intersection->getID() % sizeX == sizeX - 1) {
                            vizFile.write((char*)backgroundColor, simviz::NumRGBBytes);
                            continue;
                        }
                        direction = Directions::EAST;
                    }
                } else if (col % 3 == 1) {
                    if (row % 3 == 0) {      // north of intersection
                        if (intersection->getID() < sizeX) {
                            vizFile.write((char*)backgroundColor, simviz::NumRGBBytes);
                            continue;
                        }
                        direction = Directions::NORTH;
                    } else {      // south of intersection
                        if (intersection->getID() >= sizeY * sizeX - sizeX) {
                            vizFile.write((char*)backgroundColor, simviz::NumRGBBytes);
                            continue;
                        }
                        direction = Directions::SOUTH;
                    }
                } else { // corner
                    vizFile.write((char*)backgroundColor, simviz::NumRGBBytes);
                    continue;
                }
                
                if (intersection->getNumCars(direction) > 0) {
                    vizFile.write((char*)oneCarsColor, simviz::NumRGBBytes);
                } else if (intersection->getRoadIDByDirection(direction) != -1) {
                    vizFile.write((char*)roadColor, simviz::NumRGBBytes);
                } else {
                    vizFile.write((char*)backgroundColor, simviz::NumRGBBytes);
                }
            }
        }
    }

    void readRoadFile(std::string filename, Intersection* intersections) {
        std::ifstream file(filename);
        if (!file) {
            mass::logger::error("error opening road file: %s", filename);
        }
        std::string word = "";
        while (word != "Links") {
            if (word == "id:") {
                int id, x, y;
                file >> id;
                file >> word;
                file >> x;
                file >> word;
                file >> y;
                intersections[id] = {
                    id,                 // id
                    x,                  // x
                    y,                  // y
                    false,              // isRoad
                    {-1 , -1, -1, -1},  // destinations
                    {-1 , -1, -1, -1}   // roadIds
                };
            }
            file >> word;
        }
        while (file) {
            file >> word; // "id:"
            if (word == "") continue;
            int id, src, des;
            file >> id;
            file >> word; // "PointID1:"
            file >> src;
            file >> word; // "PointID2:"
            file >> des;

            if (src < des) {
                if (src + 1 == des) {
                    intersections[src].destination[1] = des;
                    intersections[src].roadId[1] = id;
                    intersections[des].destination[3] = src;
                    intersections[des].roadId[3] = id;
                } else {
                    intersections[src].destination[2] = des;
                    intersections[src].roadId[2] = id;
                    intersections[des].destination[0] = src;
                    intersections[des].roadId[0] = id;
                }
            } else if (src > des) {
                if (src - 1 == des) {
                    intersections[src].destination[3] = des;
                    intersections[src].roadId[3] = id;
                    intersections[des].destination[1] = src;
                    intersections[des].roadId[1] = id;
                } else {
                    intersections[src].destination[0] = des;
                    intersections[src].roadId[0] = id;
                    intersections[des].destination[2] = src;
                    intersections[des].roadId[2] = id;
                }
            } else {
                continue;
            }
        }
    }

    std::vector<std::vector<int>> readRouteFile(std::string filename, Car* cars, int numCars) {
        std::vector<std::vector<int>> routes;
        std::ifstream file(filename);
        if (!file) {
            mass::logger::error("error opening route file: %s", filename);
        }
        std::string word = "";
        while (file) {
            file >> word; // Agent
            if (word != "Agent") continue;
            if (word == "") continue;
            int id, start, end;
            file >> word; // ID:
            file >> id;
            file >> word; file >> word; // starting Point:
            file >> start;
            file >> word; file >> word; // ending Point:
            file >> end;
            file >> word; // Route:
            file.get(); // " "
            std::string line;
            std::getline(file, line);
            std::istringstream f(line);
            routes.push_back(std::vector<int>{});
            while (getline(f, word, ' ')) {
                if (atoi(word.c_str()) != 0)
                    routes[id].push_back(atoi(word.c_str()));
            }
            cars[id].id = id;
            cars[id].start = start;
            cars[id].end = end;
            cars[id].routeSize = routes[id].size();
            if (id == 0) {
                cars[id].routeOffset = 0;
                continue;
            }
            cars[id].routeOffset = cars[id - 1].routeOffset + cars[id - 1].routeSize;
        }
        return routes;
    }

    void routesToRoutesArr(std::vector<std::vector<int>> routes, int* routesArr, Car* cars) {
        for (int i = 0; i < routes.size(); i++) {
            for (int j = 0; j < cars[i].routeSize; j++) {
                routesArr[cars[i].routeOffset + j] = routes[i][j];
            }
        }
    }
    
    void fillPlaceIdxs(int* placeIdxs, Car* cars, int numCars) {
        for (int i = 0; i < numCars; i++) {
            placeIdxs[i] = cars[i].start;
        }
    }
}