#pragma once

#include <mass/Place.h>
#include <fstream>

#include "MATSim.h"
#include "IntersectionState.h"
#include "Road.h"

enum IntersectionFunctions : int {
    INIT_INTERSECTION = 0,
    INIT_AGENT,
    RESOLVE_LANE,
    RESOLVE_TERMINATE,
    
    // Debug functions
    PRINT_STATE,
};

class IntersectionPlace : public mass::Place {
    public:
        IntersectionState* state;
        
        MASS_FUNCTION IntersectionPlace(mass::PlaceState *placeState, void *argument);
        MASS_FUNCTION ~IntersectionPlace();
        MASS_FUNCTION void callMethod(int functionId, void *argument);

        // MASS functions
        MASS_FUNCTION void initIntersection(matsim::Intersection intersection);
        MASS_FUNCTION void initAgent();
        MASS_FUNCTION int putCarOnLane(int index, int roadId);
        MASS_FUNCTION void requestLane(int index, int roadId);
        MASS_FUNCTION void resolveLane();
        MASS_FUNCTION bool depart(int index, int direction);
        MASS_FUNCTION void addCar();
        MASS_FUNCTION void removeCar();
        MASS_FUNCTION void addAgent(int index);
        MASS_FUNCTION void terminateAgent(int index);
        MASS_FUNCTION void resolveTerminate();

        // DEBUG functions
        MASS_FUNCTION void printState();

        // Road functions
        MASS_FUNCTION bool addCar(int direction);
        MASS_FUNCTION void removeCar(int index, int direction);
        
        // Getters
        MASS_FUNCTION int getID();
        MASS_FUNCTION int getCapacity();
        MASS_FUNCTION int getNumCars();
        MASS_FUNCTION bool isRoad();
        MASS_FUNCTION int getDirectionByDestID(int destId);
        MASS_FUNCTION int getDirectionByID(int roadId);
        MASS_FUNCTION int getRoadIDByDirection(int direction);
        MASS_FUNCTION int getRoadLengthByDirection(int direction);
        MASS_FUNCTION int getDestByDirection(int direction);
        MASS_FUNCTION int getNumCars(int direction);
};
