#include "Road.h"
#include <algorithm>

Road::Road() {
  roadID = -1;
  length = -1;
  numLanes = -1;
  source = -1;
  destination = -1;
  capacity = -1;
  numCars = -1;
  direction = -1;
}

void Road::makeRoad(int roadId, int source, int destination, int length, int numLanes) {
  this->roadID = roadId;
  this->length = length;
  this->numLanes = numLanes;
  this->source = source;
  this->destination = destination;
  capacity = std::max((int)(numLanes * length) / 15, 1);
  numCars = 0;
  if (destination == source + 1) direction = 1;       // North
  else if (destination == source - 1) direction = 3;  // East
  else if (destination > source + 1) direction = 2;   // South
  else if (destination < source - 1) direction = 0;   // West
}

//================================== Getters ==================================
int Road::getID() const { return roadID; }

int Road::getLength() const { return length; }

int Road::getNumLanes() const { return numLanes; }

int Road::getSource() const { return source; }

int Road::getDest() const { return destination; }

int Road::getCapacity() const { return capacity; }

int Road::getVacancy() const { return capacity - numCars; }

int Road::getDirection() const { return direction; }

//================================= Helpers =================================
bool Road::addCar() {
  bool addedCar = (getVacancy() > 0);
  if (addedCar) {
    numCars++;
  }
  return addedCar;
}

void Road::removeCar() {
  if (numCars > 0) {
    numCars--;
  }
}