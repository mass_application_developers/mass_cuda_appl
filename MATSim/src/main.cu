#include <iostream>

// Boost APIs
#include <boost/program_options.hpp>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include <simviz.h>

#include <mass/Mass.h>
#include <mass/Logger.h>

#include "MATSim.h"
#include "Timer.h"

namespace po = boost::program_options;
namespace logging = boost::log;

int main(int argc, char* argv[]) {
	// Setup and parse program options.
	po::options_description desc("general options");
	desc.add_options()
		("help", "print help message")
		("verbose", po::bool_switch()->default_value(false), "set verbose output")
		("interval", po::value<int>()->default_value(0), "output interval")
		("out_file", po::value<std::string>()->default_value("./matsim.viz"), "SimViz file output")
	;

	po::options_description sim_opts("simulation options");
	sim_opts.add_options()
		("sizeX", po::value<int>()->default_value(30), "number of columns in Place")
		("sizeY", po::value<int>()->default_value(30), "number of rows in Place")
		("numCars", po::value<int>()->default_value(90), "number of car Agents")
		("roadFile", po::value<std::string>()->default_value("src/Node_Links.txt"), "name of Edge text file")
		("routeFile", po::value<std::string>()->default_value("src/Car_Agents.txt"), "name of Car text file")
	;
	desc.add(sim_opts);

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
		std::cout << desc << std::endl;

		return 0;
	}

	// Setup logger.
	logging::trivial::severity_level log_level = logging::trivial::info;
	if (vm["verbose"].as<bool>()) {
		log_level = logging::trivial::debug;
	}

	mass::logger::setLogLevel(log_level);

	// Parse simulation options and get a config struct.
	matsim::ConfigOpts opts = matsim::parseSimConfig(vm);

	int interval = vm["interval"].as<int>();
	mass::logger::info(
		"Running MATSim with params: sizeX=%d, sizeY=%d, numCars=%d, interval=%d",
		opts.sizeX, opts.sizeY, opts.numCars, interval
	);

	// Create viz file if interval is non-zero.
	simviz::RGBFile vizFile(opts.sizeX*3, opts.sizeY*3);
	if (interval > 0) {
		vizFile.open(vm["out_file"].as<std::string>().c_str());
	}

	Timer timer;
	timer.start();

    // Run application
	matsim::runSimulation(opts, interval, vizFile);

	mass::logger::info("Total execution time %dus\n", timer.lap());
    
	vizFile.close();

	return 0;
}