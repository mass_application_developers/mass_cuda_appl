# Multi-Agent Transport Simulation (MATSim)

Multi-Agent Transport Simulation (MATSim) simulates vehicles commuting during peak hours of traffic. 

The purpose of this simulation is to model the flow of traffic and formed during the morning commute when the majority of single household cars have a similar origin and destination points. 

This simulation has cars do a morning route where they travel to a destination near the center of the city grid.

## Providing Inputs

This benchmark requires two input files to run the program: Car_Agents.txt and Node_Links.txt.

Car_Agents.txt lists the cars’ start and endpoints in the roadway graph and provides the route the car will take to get from the start to endpoint by listing the road IDs.

Node_Links.txt is the input file to generate the roadway graph, assigning IDs to intersections and roadways and indicating which intersections are connected.

To ensure these files are accurate, please use Nathan’s program to automatically generate these text files located on Bitbucket at: 
https://bitbucket.org/mass_application_developers/mass_cpp_appl/src/Nathan_Repast_HPC/Repast_MATSIM_Final/InputFileMATSIM/.

## Running the Program

To build and run the MATSim application, first run `make develop`. Once the development environment is setup, run `make build` and then `make test` to ensure all tests are passing properly. Once built, a `MATSim` executable will be placed within the `./bin` directory, and can be used to run the simulation.

Running the application with the `--help` flag will provide a description of the application and options that can be used to define simulation parameters.

Specifying an output interval, by using the `--interval` flag will tell the program to output the simulation to a `.viz` file every `<interval>` steps. This simulation file can be played using the MASS SimViz application for a graphical visualization of the simulation space. To build the simviz application, run `make build-simviz`.

In the examples below, roads and intersctions are colored dark gray. A single car is green, two cars are yellow, three cars are orange, and four cars are red.

_Example output with a simulation space of 30x30_


![MATSim (30x30)](./img/matsim_30x30.gif)


```
## Command to reproduce the above simulation
./bin/MATSim --sizeX=30 --sizeY=30 --numCars=90 --roadFile=test_files/Node_Links.txt --routeFile=test_files/Car_Agents.txt --interval=1
```

_Example output with a simulation space of 110x110_

![MATSim (110x110)](./img/matsim_110x110.gif)

```
## Command to reproduce the above simulation
./bin/MATSim --sizeX=110 --sizeY=110 --numCars=1210 --roadFile=test_files/Node_Links110.txt --routeFile=test_files/Car_Agents110.txt --interval=5
```