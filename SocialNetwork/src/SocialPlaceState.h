#pragma once

#include "mass/PlaceState.h"
#include "mass/settings.h"
#include "Queue.h"
#include "Set.h"

using namespace std;

class SocialPlace;

class SocialPlaceState : public mass::PlaceState {    
public:
    MASS_FUNCTION ~SocialPlaceState(){
        delete q;
        delete visited;
        delete[] firstDegree;
        delete[] currentLevel;
    }
    int userId;
    int nFriends;
    Queue* q;
    int* firstDegree;
    int* currentLevel;
    int currentLevelSize;
    Set* visited;
};