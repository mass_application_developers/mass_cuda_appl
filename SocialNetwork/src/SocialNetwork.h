#pragma once

#include "mass/Places.h"
#include <vector>
using namespace std;


class SocialNetwork {
public:
    SocialNetwork();
    virtual ~SocialNetwork();
    void runMassSim(int nPeople, int nFriends, int xthDegree);

    // Randomize the selection for the network friends.
    void createKRegularGraph(vector<int>& graph, int numVertices, int numEdges);

    void addEdgesXStepsAway(int numStepsAway, vector<int>& graph, int numVertices, int numEdges);

    bool addEdge(int source, int dest, vector<int>& graph, int numEdges);
};
