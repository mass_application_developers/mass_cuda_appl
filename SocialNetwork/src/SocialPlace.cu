#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "mass/Mass.h"
#include "mass/Logger.h"
#include "SocialPlace.h"

using namespace std;
using namespace mass;
using namespace logger;

MASS_FUNCTION SocialPlace::SocialPlace(mass::PlaceState* state, void *argument) : mass::Place(state, argument) {
    myState = (SocialPlaceState*)state;

    // setting userId using the state index of the place.
    myState->userId = 0;
    myState->nFriends = 0;
    myState->firstDegree = new int[MAX_NEIGHBORS];
    myState->currentLevel = new int[1];
    myState->q = new Queue(MAX_NEIGHBORS);
    myState->visited = new Set;
}

MASS_FUNCTION SocialPlace::~SocialPlace() {
    delete myState;
}

MASS_FUNCTION void SocialPlace::callMethod(int functionId, void *arg){
    switch (functionId) {
        case SET_FRIENDS:
            setFriends((int*)arg);
            break;
        case CALCULATE_XTH_FRIENDS:
            calculateXthFriends();
            break;
        case SET_USER_ID:
            setUserId((int*)arg);
            break;
        case OUTPUT_FRIENDS:
            outputlevel((int*)arg);
            break;
        default:
            break;
    }
}

MASS_FUNCTION void SocialPlace::setUserId(int* nFriends) {
    myState->userId = this->getIndex();
    myState->nFriends = *nFriends;
}

MASS_FUNCTION void SocialPlace:: setFriends(int* friends) {
    int friendIndex = (myState->userId * myState->nFriends);
    for(int friendNum = 0; friendNum < myState->nFriends; friendNum++, friendIndex++){
        myState->firstDegree[friendNum] = friends[friendIndex];
        myState->q->enqueue(friends[friendIndex]);
        myState->neighborOffsets[friendNum] = friends[friendIndex] - myState->userId;
        // printf("\n my index: %d, friend: %d, queue top: %d, offset: %d \n", myState->userId, myState->firstDegree[friendNum], myState->q->getFront(), myState->neighborOffsets[friendNum]);
    }
}

MASS_FUNCTION void SocialPlace::calculateXthFriends(){
    int size = myState->q->getSize();
    if(size == 0){
        return;
    }
    Set newOffsets;
    Set level;

    for(int i = 0; i < size; i++){
        if(myState->q->isEmpty()){
            break;
        }
        int cur = myState->q->getFront();
        myState->q->dequeue();
        level.add(cur);
        
        myState->visited->add(cur);
        
        int* neighborFriends = ((SocialPlace*)myState->neighbors[i])->myState->firstDegree;
        for(int j = 0; j < myState->nFriends; j++){
            int nNeighbor = neighborFriends[j];
            if((nNeighbor != myState->userId) && !(myState->visited->contains(nNeighbor))){
                myState->q->enqueue(nNeighbor);
                newOffsets.add(nNeighbor - myState->userId);
            }
        }
    }
    int* arrNewOffsets = newOffsets.toArray();
    // set new offsets
    for(int i = 0; i < newOffsets.getSize(); i++){
        myState->neighborOffsets[i] = arrNewOffsets[i];
    }
    myState->currentLevel = level.toArray();
    myState->currentLevelSize = level.getSize();
    delete[] arrNewOffsets;
}

MASS_FUNCTION void SocialPlace::outputlevel(int* args){
    if(myState->currentLevelSize == 0){
        return;
    }
    int degree = args[0];
    int id = args[1];
    if(id == myState->userId){
        printf("\nDegree: %d\n User %d has friends:", degree, myState->userId);
        for(int i = 0; i < myState->currentLevelSize; i++){
            printf(" %d", myState->currentLevel[i]);
        }
        printf("\n");
    }
}