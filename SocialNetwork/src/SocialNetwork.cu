#include "SocialNetwork.h"

#include <iostream>
#include <cstdlib>
#include <math.h>	// floor
#include <sstream> 	// ostringstream
#include <vector>

#include "mass/Mass.h"
#include "Timer.h"
#include "mass/Logger.h" // unused

#include "SocialPlace.h"

// User input is taken on host outside of the runMassSim function, edge case checked, and then passed in
using namespace std;
using namespace mass;
using namespace logger;

SocialNetwork::SocialNetwork() {};
SocialNetwork::~SocialNetwork() {};

 
void SocialNetwork::runMassSim(int nPeople, int nFriends, int xthDegree){
  // TODO: initialize loggers go below vvvv
  logger::debug("Starting initialization and running.");

  int nDims = 1;
  int placesSize[] = {nPeople};

  // start the MASS CUDA library processes
  mass::Mass::init();

  mass::Places *places = mass::Mass::createPlaces<SocialPlace, SocialPlaceState>(
          0,		// handle
          NULL,	// args
          0,
          nDims,
          placesSize
  );

  places->callAll(SocialPlace::SET_USER_ID, &nFriends, sizeof(nFriends));
  vector<int> friendShipList((nPeople * nFriends), -1);

  createKRegularGraph(friendShipList, nPeople, nFriends);

  int myArray[friendShipList.size()];
  for(int i = 0; i < friendShipList.size(); i++){
    myArray[i] = friendShipList[i];
  }

  Timer timer;
  timer.start();

  places->callAll(SocialPlace::SET_FRIENDS, &myArray, sizeof(myArray));
  places->exchangeAll(MAX_NEIGHBORS);

  // Call all places to calculateXth neighbor
  for(int degree = 0; degree < xthDegree; degree++){
    places->callAll(SocialPlace::CALCULATE_XTH_FRIENDS);
    places->exchangeAll(MAX_NEIGHBORS);

    for(int i = 0; i < nPeople; i++){
      int argArr[] = {degree, i};
      places->callAll(SocialPlace::OUTPUT_FRIENDS, &argArr, sizeof(argArr));
    }
  }
  logger::info("MASS time %d\n\n",timer.lap());

  // clean up logger and mass and delete friendShipList
  Mass::finish();
}

void SocialNetwork::createKRegularGraph(vector<int>& graph, int numVertices, int numEdges) {
  if (numVertices % 2 == 0) /*Case 1: numVertices (n) is even*/ {
    if (numEdges % 2 == 0) /*Case 1a: numEdges (k) is also even*/ {
      int numEvens = numEdges / 2; // Number of even integers in the interval [1,numEdges]
      for (int i = 0; i < numEvens; i++) {
        addEdgesXStepsAway(i + 1, graph, numVertices, numEdges);
      }
    }
    else /*Case 1b: k is odd*/ {
      int numOdds = ((numEdges - 1) / 2) + 1; // Number of odd integers in the interval [1,numEdges]
      for (int i = 0; i < numOdds; i++) {
        addEdgesXStepsAway((numVertices / 2) - i, graph, numVertices, numEdges);
      }
    }
  }
  else /*Case 2: n is odd*/ {
    for (int i = 1; i < numEdges; i++) {
      if (i % 2 == 1) {
        addEdgesXStepsAway(i, graph, numVertices, numEdges);
      }
    }
  }
  logger::info("completed the creation of the graph.");
}


//--------------------------- addEdgesXStepsAway -------------------------------
void SocialNetwork::addEdgesXStepsAway(int numStepsAway, vector<int>& graph, int numVertices, int numEdges) {
  for (int sourceVtx = 0; sourceVtx < numVertices; sourceVtx++) {
    int destVtx = (sourceVtx + numStepsAway) % numVertices;
    if (addEdge(sourceVtx, destVtx, graph, numEdges)) /*Source adds Dest*/ {
      addEdge(destVtx, sourceVtx, graph, numEdges); // Dest adds source back
    }
  }
}

//------------------------------- addEdge --------------------------------------
bool SocialNetwork::addEdge(int source, int dest, vector<int>& graph, int numEdges) {
  int edgeIndex = -1;
  int srcStart = source * numEdges;
  for (int curEdge = srcStart; curEdge < srcStart + numEdges; curEdge++) {
    if (graph[curEdge] == dest) /*Already Added*/ {
      break;
    }
    else if (graph[curEdge] == -1) /*Found open slot*/ {
      edgeIndex = curEdge;
      break;
    }
  }
  bool addingEdge = (edgeIndex != -1);
  if (addingEdge) {
    graph[edgeIndex] = dest;
  }
  return addingEdge;
}