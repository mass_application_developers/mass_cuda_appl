// CPP program for array
// implementation of queue
#include "mass/Place.h"
using namespace std;
 
// A structure to represent a queue
class Queue {
private:
    int front, rear, size;
    unsigned capacity;
    int* array;

public:
    // Constructor to create a queue of given capacity
    MASS_FUNCTION Queue(unsigned capacity) {
        this->capacity = capacity;
        front = size = 0;
        rear = capacity - 1;  // This is important, see the enqueue
        array = new int[capacity];
    }

    // Destructor to clean up allocated memory
    MASS_FUNCTION ~Queue() {
        delete[] array;
    }

    MASS_FUNCTION int getSize() {
        return size;
    }

    // Check if the queue is full
    MASS_FUNCTION bool isFull() const {
        return (size == capacity);
    }

    // Check if the queue is empty
    MASS_FUNCTION bool isEmpty() const {
        return (size == 0);
    }

    // Add an item to the queue
    MASS_FUNCTION void enqueue(int item) {
        if (isFull()) {
            return;
        }
        rear = (rear + 1) % capacity;
        array[rear] = item;
        size = size + 1;
    }

    // Remove an item from the queue
    MASS_FUNCTION int dequeue() {
        if (isEmpty()) {
            return INT_MIN;
        }
        int item = array[front];
        front = (front + 1) % capacity;
        size = size - 1;
        return item;
    }

    // Get the front item of the queue
    MASS_FUNCTION int getFront() const {
        if (isEmpty())
            return INT_MIN;
        return array[front];
    }

    // Get the rear item of the queue
    MASS_FUNCTION int getRear() const {
        if (isEmpty())
            return INT_MIN;
        return array[rear];
    }
};
