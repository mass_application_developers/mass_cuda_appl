#include "mass/Place.h"

using namespace std;


struct Node {
 
    // The data content of the node
    int data;
 
    // Link to the left child
    Node* left;
 
    // Link to the right child
    Node* right;
 
public:
    MASS_FUNCTION ~Node(){
        delete left;
        delete right;
    }
    /*
        Function to check if BST contains a node 
        with the given data
         
        @param r pointer to the root node
        @param d the data to search
        @return 1 if the node is present else 0
    */
    MASS_FUNCTION int containsNode(Node* r, int d)
    {
        while (r != NULL) {
            if (r->data == d) {
                return 1;
            } else if (d < r->data) {
                r = r->left;
            } else {
                r = r->right;
            }
        }
        return 0;
        // if (r == NULL) {
        //     return 0;
        // }
        // int x = r->data == d ? 1 : 0;
        // return x | containsNode(r->left, d) | containsNode(r->right, d);
    }
 
    /*
        Function to insert a node with 
        given data into the BST
         
        @param r pointer to the root node
        @param d the data to insert
        @return pointer to the root of the resultant BST
    */
    MASS_FUNCTION Node* insert(Node* r, int d)
    {
        Node* newNode = new Node;
        newNode->data = d;
        newNode->left = newNode->right = NULL;

        if (r == NULL) {
            r = newNode;
        } else {
            Node* current = r;
            Node* parent = NULL;

            while (true) {
                parent = current;
                if (d < parent->data) {
                    current = current->left;
                    if (current == NULL) {
                        parent->left = newNode;
                        break;
                    }
                } else {
                    current = current->right;
                    if (current == NULL) {
                        parent->right = newNode;
                        break;
                    }
                }
            }
        }

        return r;
        // Add the node when NULL node is encountered
        // if (r == NULL) {
        //     Node* tmp = new Node;
        //     tmp->data = d;
        //     tmp->left = tmp->right = NULL;
        //     return tmp;
        // }
 
        // // Traverse the left subtree if data
        // // is less than the current node
        // if (d < r->data) {
        //     r->left = insert(r->left, d);
        //     return r;
        // }
 
        // // Traverse the right subtree if data
        // // is greater than the current node
        // else if (d > r->data) {
        //     r->right = insert(r->right, d);
        //     return r;
        // }
        // else
        //     return r;
    }
};
 

class Set {
 
    // Pointer to the root of the
    // BST storing the set data
    Node* root;
 
    // The number of elements in the set
    int size;
 
public:
    // Default constructor
    MASS_FUNCTION Set()
    {
        root = NULL;
        size = 0;
    }
 
    // Copy constructor
    MASS_FUNCTION Set(const Set& s)
    {
        root = s.root;
        size = s.size;
    }

    MASS_FUNCTION ~Set(){
        delete root;
    }
 
    /*
        Function to Add an element to the set
 
        @param data the element to add to the set
    */
    MASS_FUNCTION void add(const int data)
    {
        if (!root->containsNode(root, data)) {
            root = root->insert(root, data);
            size++;
        }
    }
 
    /*
        Function to convert the set into an array
         
        @return array of set elements
    */
    MASS_FUNCTION int* toArray() {
        if (root == nullptr) return nullptr;

        int* A = new int[size];
        Node** tempQueue = new Node*[size];

        int readIndex = 0;
        int writeIndex = 0;

        // Start with the root
        tempQueue[writeIndex++] = root;

        for (int i = 0; i < size; ++i) {
            Node* currentNode = tempQueue[readIndex++];
            A[i] = currentNode->data;

            if (currentNode->left != nullptr)
                tempQueue[writeIndex++] = currentNode->left;
            if (currentNode->right != nullptr)
                tempQueue[writeIndex++] = currentNode->right;
        }

        delete[] tempQueue;
        return A;
    }
 
    /*
        Function to check whether the set contains an element
         
        @param data the element to search
        @return relut of check
    */
    MASS_FUNCTION bool contains(int data)
    {
        return root->containsNode(root, data) ? true : false;
    }
 
    /*
        Function to return the current size of the Set
         
        @return size of set
    */
    MASS_FUNCTION int getSize()
    {
        return size;
    }
};