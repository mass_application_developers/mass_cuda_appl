#pragma once

#include "mass/Place.h"
#include "mass/Logger.h"
#include "SocialPlaceState.h"

class SocialPlace: public mass::Place {

// Here we declare all the functions for setting new neighbors, keeping a count of the neighbors, getting neighbors, etc.

private:    

public:
    // Setting all functionId's
    const static int ADD_FRIEND = 0;
    const static  int SET_FRIENDS = 1;
    const static int IS_FRIEND = 2;
    const static int GET_FRIENDS = 3;
    const static int CALCULATE_XTH_FRIENDS = 4;
    const static int SET_USER_ID = 5;
    const static int OUTPUT_FRIENDS = 6;
    const static int INIT_NEIGHBORS = 7;
    const static int NUM_NEIGHBORS = 8;
    
    // TODO: probably define all the total number of neighbors for the simulation...
    MASS_FUNCTION SocialPlace(mass::PlaceState *state, void *argument);
    MASS_FUNCTION ~SocialPlace();

    MASS_FUNCTION virtual void callMethod(int functionId, void *arg = NULL);

    MASS_FUNCTION void setUserId(int* nFriends);

    MASS_FUNCTION void setFriends(int* friendIds);

    MASS_FUNCTION void calculateXthFriends();

    MASS_FUNCTION void outputlevel(int* args);


    SocialPlaceState* myState;
};