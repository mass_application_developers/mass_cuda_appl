#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "mass/settings.h"
#include <gtest/gtest.h>
#include <mass/Mass.h>
#include "SocialNetwork.h"

SocialNetwork socialNetwork;

// TEST(SocialNetwork, Test10) {
//     socialNetwork.runMassSim(10, 2, 5);
//     ASSERT_TRUE(true);
// }

// TEST(SocialNetwork, Test20){
//     socialNetwork.runMassSim(20, 5, 5);
//     ASSERT_TRUE(true);
// }

TEST(SocialNetwork, Test50){
    socialNetwork.runMassSim(50, 10, 5);
    ASSERT_TRUE(true);
}

// TEST(SocialNetwork, Test100){
//     printf("This is the settings of neighbors: %d", MAX_NEIGHBORS);
//     socialNetwork.runMassSim(100, 10,5);
//     ASSERT_TRUE(true);
// }

// TEST(SocialNetwork, Test200){
//     socialNetwork.runMassSim(200, 10, 5);
//     ASSERT_TRUE(true);
// }

// TEST(SocialNetwork, Test300){
//     socialNetwork.runMassSim(6000, 30, 60);
//     ASSERT_TRUE(true);
// }

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
